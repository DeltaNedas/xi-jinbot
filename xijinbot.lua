#!/usr/bin/env lua5.3
XJB = {
	Aliases = {}, -- Built-in command aliases, pointers to Commands
	Commands = {}, -- Command tables, don't manually modify
	Constants = {}, -- Stuff that does not change
	Data = {}, -- REST requests for discord objects
	Events = {}, -- onGuildCreate() and stuff
	Misc = {}, -- Random stuff like EbinFace()
	News = {},
	User = {}, -- Users' persisting data stored in JSON files
	Util = {} -- General utilities
}
math.randomseed(os.time())

print("Loading Xi JinBot.")
local STDLua = require("stdlua")
local JSON = require("lunajson")
require("xjb.constants")
require("xjb.util")
require("xjb.misc")
require("xjb.data")
require("xjb.news")
require("xjb.commands")
require("xjb.events")
require("xjb.parser")
print("Xi JinBot loaded, starting up.")

local PID = STDLua.Execute("pidof xijinbot"):match("^(.-) ") -- First PID only
if PID then
	print("XiJinBot already running on PID "..PID.."!")
	os.exit(1)
end

local PID = STDLua.Read("/proc/self/stat"):match("^.- ")

local old_write = io.write

function io.write(text) -- Highlights FunctionNames(): in green
	if type(text) == "string" and text:match("^.-: ") then
		text = text:gsub(
			"^(.-:) ", "\27[32m%1\27[0m "):gsub( -- Function name
			"@(.-) ", "\27[31m@%1\27[0m "):gsub( -- User names
			" #(.-) ", " \27[33m#%1\27[0m ") -- Channel names
	end
	return old_write(text)
end

function print(...)
	local Input = {...}
	for i, val in ipairs(Input) do
		if i > 1 then
			io.write("\t")
		end
		io.write(tostring((val == nil and "nil") or val))
	end
	io.write("\n")
end

function printf(text, ...)
	return io.write(text:format(...))
end

XJB.User.Tokens = {}
if not STDLua.Exists("tokens.json") then -- chmod 400 tokens.json
	print("Please fill in tokens in `tokens'.")
	local Success, Error = STDLua.Write("tokens.json", JSON.encode{translate = "TOKEN", discord = "TOKEN"})
	if not Success then print("(Tokens): "..Error) end
	os.exit(1)
end
local Data, Error = STDLua.Read("tokens.json")
if not Data then
	print(Error)
	os.exit(1)
end
XJB.User.Tokens = JSON.decode(Data)

XJB.User.Prefixes = XJB.Util.ReadData("prefixes")
XJB.User.Credit = XJB.Util.ReadData("credit")
XJB.User.ShadowBans = XJB.Util.ReadData("shadowbans", {Issue = {}})
XJB.User.Issues = XJB.Util.ReadData("issues")
XJB.User.Rings = XJB.Util.ReadData("rings", { -- Execution levels, < 1 = full power
	Global = {["542742543478292480"] = -1, ["592448068792746000"] = -1} -- Me, replace with your accs
})
XJB.User.Roles = XJB.Util.ReadData("roles")
XJB.User.RSBM = XJB.Util.ReadData("rsbm")
XJB.User.Gerald = XJB.Util.ReadData("gerald", {"i haven't even read it"})
XJB.User.Archives = XJB.Util.ReadData("archives") -- Stuff is put here when XJB is kicked, remove archives.json to clear
XJB.User.Invites = XJB.Util.ReadData("invites")
XJB.User.Requirements = XJB.Util.ReadData("requirements")
XJB.User.Newbies = XJB.Util.ReadData("newbies")
XJB.User.Users = XJB.Util.ReadData("users")
XJB.User.Times = {} -- Used for per user timeout
XJB.User.Rupees = XJB.Util.ReadData("rupees")
XJB.User.Achievements = XJB.Util.ReadData("achievements")
XJB.User.Inventories = XJB.Util.ReadData("inventories", {Shop = {
	["iFone 6s"] = 200,
	["window 10 Professional key"] = 140,
	["Debian Buster Installer CD"] = 20,
	["toilet"] = 1200,
	["ipHone 12"] = 1739,
	["personal poo street"] = 24000
}})
XJB.User.Blind = XJB.Util.ReadData("blind") -- {Channel.id -> true}
XJB.User.Mirrors = XJB.Util.ReadData("mirrors") -- {Channel.id (source) -> {Channel.id (mirror), ...}}
XJB.User.Aliases = XJB.Util.ReadData("aliases") -- User.id = {alias = "aliased", ...}
XJB.Util.LoadFeeds()
XJB.Util.LoadCreators()

local litcord = require("litcord")
local SUser = require("litcord.structures.User")
local SChannel = require("litcord.structures.Channel")
local SMessage = require("litcord.structures.Message")
local Lanes = require("lanes")
XJB.Client = litcord(XJB.User.Tokens.discord)

for Name, Callback in pairs(XJB.Events) do
	XJB.Client:on(Name, Callback)
end

local succ, mess = xpcall(function()
	XJB.News.Start()
	XJB.Client:run()
end, debug.traceback)

if not succ then
	if not mess:match("interrupted!\n") then
		XJB.Util.Quit(1, mess)
	end
end
XJB.Util.Quit()
