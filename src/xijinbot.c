#include <lua5.3/lua.h>
#include <lua5.3/lualib.h>
#include <lua5.3/lauxlib.h>

#include <pthread.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

const int FEED_CHECK_DELAY = 5 * 60; // Check news feeds every minute

/* Lua code
	News.Loop = function()
		print("News.Loop(): News Daemon started.")
		News_Daemon_Alive = true
		while true do
			for i = 1, 5 do -- Every 5 mins check feeds
				Lanes.sleep(1)
				if not News.Alive then
					print("News.Loop(): News Daemon stopped.")
					return
				end
				print("News.Loop(): Checked for news.")
				News.CheckFeeds()
			end
		end
	end
*/

lua_State* L;

void dump_stack() {
	int i;
	int top = lua_gettop(L);
	for (i = 1; i <= top; i++) {  /* repeat for each level */
		int t = lua_type(L, i);
		switch (t) {

			case LUA_TSTRING:  /* strings */
				printf("`%s'", lua_tostring(L, i));
				break;

			case LUA_TBOOLEAN:  /* booleans */
				printf(lua_toboolean(L, i) ? "true" : "false");
				break;

			case LUA_TNUMBER:  /* numbers */
				printf("%g", lua_tonumber(L, i));
				break;

			default:  /* other values */
				printf("%s", lua_typename(L, t));
				break;
		}
		printf("  ");  /* put a separator */
	}
	printf("\n");  /* end the listing */
}

pthread_t newsLoopThread;
void *newsLoop(void* state) {
	/*lua_pushboolean(state, 1);
	lua_setglobal(state, "News_Daemon_Alive");
	while (1) {
		for (int i = 0; i < FEED_CHECK_DELAY; i++) { // Constantly loop and check if it should stop
			sleep(1);
			lua_getglobal(state, "News_Daemon_Alive");
			if (lua_toboolean(state, -1) == 0) {
				printf("Loop() [C]: News Daemon stopped.\n");
				return NULL;
			}
			lua_pop(state, -1);
		}
		printf("Loop() [C]: Checking feeds...\n");
		lua_getglobal(state, "XJB.News.CheckFeeds");
		if (lua_pcall(state, 0, 0, 0)) {
			fprintf(stderr, "Loop() [C]: Failed to check feeds: %s\n", lua_tostring(state, -1));
		} else {
			printf("Loop() [C]: Checked feeds!\n");
		}
	}*/
	return NULL;
}

int news_loop(lua_State* state) {
	printf("News.Loop() [C]: News Daemon started.\n");
	if (pthread_create(&newsLoopThread, NULL, newsLoop, state)) {
		fprintf(stderr, "C_NewsLoop() [C]: failed to create news loop thread!\n");
		dump_stack();
		exit(1);
	}
	return 0;
}

void push_key_value(char* key, char* value) {
	lua_pushstring(L, key);
	lua_pushstring(L, value);
	lua_settable(L, -3);
}


int quit(int i) {
	lua_close(L);
	exit(i);
}

int handled = 0;

void handle_signal(int signo) {
	handled++;
	if (handled == 1) {
		printf("[C] Shutting down...\n");
		lua_getglobal(L, "XJB");
		lua_getfield(L, -1, "Util");
		lua_getfield(L, -1, "Quit");
		lua_pushnumber(L, 0);
		lua_pcall(L, 1, 0, 0);
	} else if (handled == 2) {
		printf("[C] Attempting to save creators and feeds...\n");
		lua_getglobal(L, "XJB");
		lua_getfield(L, -1, "Util");
		lua_getfield(L, -1, "SaveCreators");

		printf("[C] Creators saved!\n");
		lua_pcall(L, 1, 0, 0);
		lua_getglobal(L, "XJB");
		lua_getfield(L, -1, "Util");
		lua_getfield(L, -1, "Feeds");

		printf("[C] Feeds saved, shutting down...\n");
		quit(0);
	} else {
		printf("\033[31m[C] Force quitting!\033[0m\n");
		remove("pid");
		quit(1);
	}
}

pthread_t luaThread;
void* start_xijinbot(void* void_ptr) {
	printf("[C]: Loaded Xi JinBot, starting!\n");
	if (lua_pcall(L, 0, LUA_MULTRET, 0)) {
		fprintf(stderr, "[C]: Failed to run Xi JinBot!\n");
		dump_stack();
		quit(-1);
	}
	return NULL;
}

void run_command(char* command) { // Run Lua ParseCommand()
	lua_getglobal(L, "ParseCommand");
	lua_pushstring(L, command);
	lua_pushstring(L, command);
	lua_pushboolean(L, 1); // Indicate that its fake
	lua_pcall(L, 3, 0, 0);
}

char* read_line() { // "Repurposed" from stack overflow
	char* line = malloc(50), *linep = line;
	size_t lenmax = 50, len = lenmax;
	int c;

	if (line == NULL) {
		return NULL;
	}

	while (1) {
		c = fgetc(stdin);
		if (c == EOF) {
			break;
		}

		if (--len == 0) {
			len = lenmax;
			char* linen = realloc(linep, lenmax *= 2);

			if(linen == NULL) {
				free(linep);
				return NULL;
			}
			line = linen + (line - linep);
			linep = linen;
		}

		if ((*line++ = c) == '\n') {
			break;
		}
	}
	*line = '\0';
	return linep;
}

int start_console() { // Broken pipe shit
	/*while (1) {
		//printf("Xi JinBot$ ");
		char* command = read_line();
		if (command == NULL) {
			break;
		}
		if (strcmp(command, "quit") == 0) {
			break;
		}
		run_command(command);
	}*/
	return 0;
}

int main(int argc, char* argv[]) {
	if (signal(SIGINT, handle_signal) == SIG_ERR) {
		fprintf(stderr, "[C]: Can't handle interrupts!\n");
		quit(1);
	}

	L = luaL_newstate();
	luaL_openlibs(L);
	if (luaL_loadfile(L, "xijinbot.lua")) {
		fprintf(stderr, "[C]: Failed to load xijinbot.lua!\n");
		dump_stack();
		quit(1);
	}
	printf("[C]: Loading Xi JinBot News Loop...\n");
	lua_pushcfunction(L, news_loop);
	lua_setglobal(L, "XJB_News_Loop");

	if (argc > 2) {
		lua_newtable(L);
		//int pargs = lua_gettop(L);
		push_key_value("Channel", argv[1]);
		push_key_value("Reply", argv[2]);
		push_key_value("Message", argv[3]);
		push_key_value("Timestamp", argv[4]);
		//lua_settop(L, pargs);
		lua_setglobal(L, "PArgs"); // Xi JinBot is back online! message
	}

	start_xijinbot(NULL);
	return 0;
	/*if (pthread_create(&luaThread, NULL, start_xijinbot, NULL)) {
		fprintf(stderr, "C_NewsLoop() [C]: failed to create Xi JinBot thread!\n");
		exit(1);
	}
	return start_console();*/
}
