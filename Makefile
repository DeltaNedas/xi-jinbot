BUILDDIR = build
OBJECTDIR = objects
SOURCEDIR = src

CC = gcc
CCFLAGS = -O3 -Wall -I$(SOURCEDIR) -c -g
LDFLAGS = -lpthread -llua5.3

EXEC_FILE = xijinbot
OBJ = xijinbot
OBJECTS = $(patsubst %, $(OBJECTDIR)/%.o, $(OBJ))

all: $(EXEC_FILE)

$(OBJECTDIR)/%.o: $(SOURCEDIR)/%.c
	mkdir -p `dirname $@`
	$(CC) $(CCFLAGS) -o $@ $^

$(BUILDDIR)/$(EXEC_FILE): $(OBJECTS)
	mkdir -p $(BUILDDIR)
	$(CC) -o $@ $^ $(LDFLAGS)

$(EXEC_FILE): $(BUILDDIR)/$(EXEC_FILE)

clean:
	rm -rf $(OBJECTDIR)
	rm -f $(BUILDDIR)/$(EXEC_FILE)

remake: clean $(EXEC_FILE)
