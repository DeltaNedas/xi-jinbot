local STDLua = require("stdlua")
local JSON = require("lunajson")
local litcord = require("litcord")
local SUser = require("litcord.structures.User")
local SChannel = require("litcord.structures.Channel")
local SMessage = require("litcord.structures.Message")

XJB.Events.ready = function()
	if PArgs then
		local ReplyChannel = PArgs.Channel
		local ReplyMessage = PArgs.Reply
		local Timestamp = PArgs.Timestamp
		if ReplyChannel and ReplyMessage then
			local message = XJB.Data.GetMessage(ReplyChannel, ReplyMessage)
			if message then
				local secs, mils = STDLua.Timestamp(3)
				print(secs,mils,Timestamp)
				local Text = XJB.Util.CommandOutput(nil, PArgs.Message.."\n*Restarting took **"..((tonumber(secs..mils) - tonumber(Timestamp)) / 1000).."** seconds.*", 65450)
				XJB.Util.SendMessage(message, Text, XJB.Data.GetChannel(ReplyChannel))
			end
		end
	end
	PArgs = nil

	XJB.News.CheckFeeds()
	XJB.Data.UpdateStatus()

	XJB.Ready = true
	print("on.ready: Xi JinBot is now ready, use $help in PMs or a server.")
end

XJB.Events.guildCreate = function(Server)
	if XJB.Ready then
		XJB.Util.Welcome(Server)
		XJB.Util.LoadArchive(Server)
		XJB.Util.UpdateInvites(Server)
	end
end

XJB.Events.guildDelete = function(Server)
	if XJB.Ready then
		local ID = Server.id
		printf("on.guildDelete(): Removed server %s (%s).\n", Server.name, ID)
		for i, Guild in pairs(XJB.Client.servers) do
			if Guild.id == Server.id then
				XJB.Util.SaveArchive(Server)
				XJB.Client.servers[i] = nil
				break
			end
		end
		XJB.Data.UpdateStatus()
	end
end

XJB.Events.guildMemberAdd = function(member, Temporary)
	if XJB.Ready then
		local User = member.user
		if User.id ~= XJB.Client.user.id and not XJB.User.Users[User.id] then
			print("on.guildMemberAdd(): "..User.username.." joined "..member.parent.name.."!")
			XJB.User.Users[User.id] = STDLua.Timestamp() -- actual botnet dilate
			local Flags = User.flags
			if Flags and Flags % 2 == 1 then -- JANNY!!!!
				print("on.guildMemberAdd: WARNING: janny %s (%s) is in %s (%s)\nSocial credit set to 600.", User.id, User.username, member.parent.name, member.parent.id)
				XJB.Util.SetCredit(User.id, 600)
			end
			XJB.Util.CheckRole(member.parent, member.user)
			local Channel, Newbie = XJB.Util.GetNewbie(member.parent.id, User)
			if Channel then
				local Days = XJB.Util.ParseTime(tonumber(Newbie))
				XJB.Util.SendMessage(nil, (XJB.Util.CommandOutput("🚨 Warning!", "**"..User.username.."**#**"..User.discriminator.."** *(**"..User.id.."**)* has joined while being on discord for less than **"..Days.."** "..XJB.Misc.s("day", Days).."!", XJB.Constants.Colours.Red)), Channel)
			end
			if Temporary then
				XJB.User.Users[User.id] = nil
			else
				XJB.Util.WriteData("users")
			end
		end
	end
end

XJB.Events.messageCreate = function(message)
	if message.author.id ~= XJB.Client.user.id then
		printf("on.messageCreate: %s #%s | %s: %s\n", (message.parent or {}).parent.name or "", message.parent.name or "PMs", message.author.username, message.clean)
		for _, Attachment in pairs(message.attachments or {}) do
			printf("on.messageCreate(): %s was attached: (%s)\n", Attachment.filename, Attachment.url)
		end
		XJB.Util.ParseMessage(message)
	end
end

XJB.Events.messageDelete = function(message)
	if type(message) ~= "string" then
		if message.content then
			printf("on.messageDelete: Message from %s by %s was deleted: %s\n", message.channel_id, message.author.username, message.content)
		else
			printf("on.messageDelete: Message %s from %s deleted.\n", message.id, message.channel_id)
		end
		local Channel = XJB.User.Creators[message.channel_id]
		if Channel then
			local Creator = Channel[message.id]
			if Creator then
				if Creator.Cached then -- Fast startup time
					print("on.messageDelete: Loading cached message "..message.id..".")
					Creator = XJB.Util.LoadCreator(Creator, message)
				end

				if Creator then -- If command users msg was deleted
					message = Creator.Message
					if message.content == nil or not message.content:match("^say") then
						local Message = require("litcord.structures.Message")
						for _, Response in pairs(Creator.Responses) do
							if not Response.parent then
								Response.parent = XJB.Data.GetChannel(message.channel_id)
							end
							if Response.parent then
								if not Response.parent.parent then
									Response.parent.parent = XJB.Data.GetServer(Response.parent.guild_id)
								end
								if Response.parent.parent then
									print("Command deleted.")
									Response.parent.parent.parent = XJB.Client
									SMessage.delete(Response)
								end
							end
						end
					end
				end
			end
		end
	end
end