local Lanes = require("lanes")
local STDLua = require("stdlua")
local JSON = require("lunajson")
local News = {}
News_Daemon_Alive = false
News.Loop = XJB_News_Loop
XJB_News_Loop = nil

News.CheckFeeds = function()
	printf("CheckFeeds(): Took %.3fms to check feeds.\n", (XJB.Util.TimeFunction(function()
		for ServerID, ServerFeeds in pairs(XJB.User.Feeds) do
			printf("CheckFeeds(): Checking feeds for %s.\n", ServerID)
			for Short, _ in pairs(ServerFeeds) do
				local Link = table.merge(ServerFeeds[Short] or {}, XJB.Constants.FeedLinks[Short:lower()] or {})
				local Data, Error
				if Link.URL then
					Data, Error = STDLua.Download(Link.URL)
				else
					Data = ""
				end
				if Data then
					local Latest = (Link.GetLatest or XJB.Constants.RSS.GetLatest)(Data) -- Parse RSS to retrieve latest link
					if Latest and Latest ~= Link.Latest then
						ServerFeeds[Short].Latest = Latest
						local Success, Error = XJB.Util.WriteData("feeds")
						if not Success then
							print("CheckFeeds(): "..Error)
						end
						local Channel = XJB.Util.GetFeed(Short, ServerID)
						if Channel then
							XJB.Util.SendMessage(nil, (Link.GetEmbed or XJB.Constants.RSS.GetEmbed)(Link.Name, Latest, Data), Channel)
						end
					end
				else
					print("CheckFeeds(): Failed to get latest article from "..Short..": "..Error)
				end
			end
		end
	end) / 1000))
end

News.Start = function()
	print("News.Start(): Starting News Daemon...")
	News_Daemon_Alive = true
	News.Loop()
end

News.Stop = function()
	print("News.Stop(): Stopping News Daemon...")
	News_Daemon_Alive = false
end

XJB.News = News
