local STDLua = require("stdlua")
local JSON = require("lunajson")
local Lanes = require("lanes")

XJB.Util.ParseIntegrations = function(message)
	printf("ParseIntegrations(): Took %.6f seconds to run.\n", (XJB.Util.TimeFunction(function()
		for Identifier, List in pairs(XJB.Constants.Integrations) do
			local ID, Username
			if Identifier:match("^ID:") then ID = Identifier:match("^ID:(.*)") end
			if Identifier:match("^Username:") then Username = Identifier:match("^Username:(.*)") end
			if Identifier == "*" or message.author.id == ID or message.author.username == Username then
				for Pattern, Integration in pairs(List) do
					if message.content:match(Pattern) then
						if Integration.Delete then
							message:delete()
						end
						local Username
						local Discriminator
						local ID
						if Integration.Username then Username = message.content:match(Integration.Username) end
						if Integration.Discriminator then Discriminator = message.content:match(Integration.Discriminator) end
						if Integration.ID then ID = message.content:match(Integration.ID) end
						local User
						if ID then
							User = XJB.Data.GetUser(ID)
						elseif Username and Discriminator then
							local Server, Error = XJB.Data.GetServer(message.guild_id)
							if Server then
								User = XJB.Data.GetUserByName(Server, Username, Discriminator)
								ID = User.id
							else
								print("ParseIntegrations(): Failed to get bot server: "..tostring(Error))
							end
						end
						if User and not (Username and Discriminator) then
							Username = User.username
							Discriminator = User.discriminator
						end

						local Score = 1000
						if User then
							Score, Error = XJB.Util.GetCredit(User)
							if not Score then print("ParseIntegrations(): "..Error) end
						end

						Integration.Title = Integration.Title or "**🚔 Social Credit Police**"
						if Integration.Title == "" then Integration.Title = nil end
						local Text = Integration.Text or Integration.Title
						if Text then
							Text = Text:gsub(
								"%$ID", ID or "$ID"):gsub(
								"%$USERNAME", Username or "$USERNAME"):gsub(
								"%$DISCRIMINATOR", Discriminator or "$DISCRIMINATOR" ):gsub(
								"%$PING", "<@"..(ID or "ID")..">"):gsub(
								"%$SCORE", Score or "1000"):gsub(
								"%$NEWSCORE", Score - (Integration.Penalty or 0)):gsub(
								"%$PENALTY", Integration.Penalty or 0)
							if Integration.Text then
								Text = XJB.Util.CommandOutput(Integration.Title, Text, Integration.Colour or XJB.Constants.Colours.DarkBlue)
								if Integration.Image then
									Text.embed.image = {
										url = Integration.Image
									}
								end
							end
							XJB.Util.SendMessage(message, Text)
						end
						if ID then
							XJB.UtilSetCredit(ID, Score - (Integration.Penalty or 0))
						end
						break
					end
				end
			end
		end
	end)))
end

XJB.Util.RebuildMessage = function(Text, Username, Discriminator, UserID, ChannelID, ServerID)
	return {
		content = Text,
		clean = Text,
		author = {
			username = Username,
			discriminator = Discriminator,
			id = UserID
		},
		parent = XJB.Data.GetChannel(ChannelID, ServerID),
		parent_id = ChannelID,
		channel_id = ChannelID,
		guild_id = ServerID
	}
end

XJB.Util.ParseCommand = function(CommandName, Text, Fake, User, Ring, Score, Rupees, Message, Prefix, Silent)
	local Username, Discriminator, UserID, ChannelID, ServerID
	if Fake then
		Username = "ThioJones"
		Discriminator = "0282"
		UserID = "542742543478292480"
		Ring = -1
		Score = 1300
		Rupees = 10000
		CommandName = Text:match("^(.-)%s") or Text
		Prefix = XJB.Constants.DefaultPrefix
		ChannelID = 607705335553327117
		ServerID = 607703734339895315
	end


	local Aliases = XJB.User.Aliases[UserID or User.id] or {}
	local Aliased = Aliases[CommandName]
	if Aliased then
		local AliasedCommand = Aliased:match("^(.-) ") or Aliased
		Message.content = Text:gsub("^"..CommandName:escape("Patterns"), AliasedCommand)
		return XJB.Util.ParseCommand(AliasedCommand, Aliased.." "..Text, Fake, User, Ring, Score, Rupees, Message)
	end

	local Command = XJB.Commands[CommandName]
	if Command then
		local message = Message or XJB.Util.RebuildMessage(Text, Username, Discriminator, UserID, ChannelID, ServerID)
		local Args = message.content:split(" ") or {""}
		local CommandName = table.remove(Args, 1)
		if message.content:match(Prefix:escape("Patterns").."%(.-%)") then
			local Start, End
			local Brackets = XJB.Misc.FindBrackets(message.content, Prefix)
			for i, Set in pairs(Brackets or {}) do
				if type(i) == "number" then
					Start = Set.start
					End = Set.stop
					if Start and End then
						local SubCommandName = message.content:sub(Start + #Prefix + 1, End - 1):gsub(" .+$", "")
						local Before = ""
						if Start > 1 then
							Before = message.content:sub(1, Start - 1)
						end
						local After = ""
						if End < #message.content then
							After = message.content:sub(End + 1)
						end
						local LoopTimes = 1
						local Count = After:match("^%[(.-)%]")
						if Count then -- Loops
							if tonumber(Count) then
								LoopTimes = tonumber(Count)
							else
								local LoopMessage = table.copy(message)
								LoopMessage.content = Prefix..Count
								LoopMessage.clean = LoopMessage.content
								local Reply, Value, Error = XJB.Util.ParseMessage(LoopMessage, true, false)
								if Error then print(Error) end
								if Value then
									LoopTimes = tonumber(Value)
								end
							end
							if LoopTimes == nil then
								XJB.Util.SendMessage(message, (XJB.Util.CommandOutput("🚨 Error while parsing subcommand 🚨", "Loop times must be a number.", XJB.Constants.Colours.Red)), nil, Fake)
								break
							elseif not (Ring and Ring <= 1) and LoopTimes > (5 + (Score - 1000) / 100) then
								XJB.Util.SendMessage(message, (XJB.Util.CommandOutput("🚨 Error while parsing subcommand 🚨", "Loop must be ran "..toString(5 + (Score - 1000) / 100).." times or less.", XJB.Constants.Colours.Red)), nil, Fake)
								break
							elseif LoopTimes < 1 then
								XJB.Util.SendMessage(message, (XJB.Util.CommandOutput("🚨 Error while parsing subcommand 🚨", "Loop must run once or more.", XJB.Constants.Colours.Red)), nil, Fake)
								break
							end
							LoopTimes = math.floor(LoopTimes)
							After = After:match("^%[.-%](.*)")
						end
						local SubMessageContent = Prefix..message.content:sub(Start + #Prefix + 1, End - 1)
						local SubCommand = XJB.Commands[SubCommandName]
						local Aliased = Aliases[SubCommandName]
						if Aliased then
							local AliasedName = Aliased:match("^(.-) ") or Aliased
							SubMessageContent = SubMessageContent:gsub("^("..Prefix:escape("Patterns")..")"..SubCommandName, "%1"..Aliased)
							SubCommand = XJB.Commands[AliasedName]
						end

						local SubMessage = table.copy(Message)
						SubMessage.content = SubMessageContent
						SubMessage.clean = SubMessageContent -- Lazy but ok

						SubMessage.LoopTimes = LoopTimes
						if SubCommand then
							local Res
							for i = 1, LoopTimes do
								local Success, Error = xpcall(function()
									if not SubMessage.content:match("^"..Prefix:escape("Patterns")) then
										SubMessage.content = Prefix..SubMessage.content
									end
									if Score > 750 or (SubCommandName == "e" or SubCommandName == "english" or SubCommandName == "c" or SubCommandName == "chinese") or (Ring and Ring <= 3) then -- Always allow translating
										local Ret, Value, Error = XJB.Util.ParseMessage(SubMessage, true, false)
										if Error then print(Error) end
										if Value then
											Res = (Res or "")..XJB.Util.ToString(Value).." "
										end
									end -- Do not tell him if his scs is very bad
								end, debug.traceback)
								if not Success then
									XJB.Util.SendMessage(message, (XJB.Util.CommandOutput(nil, "🚨 **Xi JinBot enountered an error in subcommand!** 🚨", XJB.Constants.Colours.Red)))
									print("ParseCommand(): Subcommand Error: ", Error)
								end
								if LoopTimes > 1 then
									Lanes.sleep(0.05)
								end
							end
							Res = tostring((Res == nil and "-") or Res)
							message.content = Before..Res..After
							Args = message.content:split(" ") or {""}
							table.remove(Args, 1)
							Count = (Count and "["..Count.."]") or ""
							local Diff = (End - Start - #Res) + #(Count or "") + 1
							for i, Set in pairs(Brackets) do
								if type(i) == "number" then
									Set.start = Set.start - Diff
									Set.stop = Set.stop - Diff
								end
							end
						else
							break
						end
					end
				end
			end
		end
		local Reply, Value, Error
		local Success, MError = xpcall(function()
		if Score > 625 or (CommandName == "e" or CommandName == "english" or CommandName == "c" or CommandName == "chinese") or (Ring and Ring <= 4) then -- Always allow translating
				Reply, Value, Error = Command.func(message, Ring, Score, Rupees, table.unpack(Args))
				if not Silent then
					if Reply then
						if type(Reply) == "table" then
							if Reply.embed then
								if not Reply.embed.author then
									Reply.embed.author = XJB.Util.GetAuthor(message.author)
								elseif type(Reply.embed.author) ~= "table" then
									Reply.embed.author = nil
								end
							end
						end
						XJB.Util.SendMessage(message, Reply)
					end
					if Error then print(Error) end
				end
			end -- Do not tell him if his scs is very bad
		end, debug.traceback)
		if not (Success or Silent) then
			XJB.Util.SendMessage(message, (XJB.Util.CommandOutput(nil, "🚨 **Xi JinBot enountered an error!** 🚨", XJB.Constants.Colours.Red)))
			print("ParseCommand(): Error: ", MError)
		end
		return Reply, Value, Error
	else
		return false, "Command not found."
	end
end

XJB.Util.ParseMessage = function(message, Silent, UseTimestamp, IsAliased)
	UseTimestamp = (UseTimestamp ~= nil and UseTimestamp) or true
	local startTime
	local Ret, Value, Error
	if not message.author.bot then
		if not Silent then
			local Mirrors = XJB.Util.GetMirrors(message.channel_id)
			if Mirrors then
				local Author = XJB.Util.GetAuthor(message.author)
				Author.name = Author.name.." - Mirrorred from #"..(message.parent.name or "PMs")
				for i, Mirror in ipairs(table.copy(Mirrors)) do
					if not XJB.Util.SendMessage(nil, {
							embed = {
								author = Author,
								description = message.content,
								color = XJB.Constants.Colours.Delta
							}
						}, Mirror) then
						print("ParseMessage(): Removing old mirror "..Mirror)
						table.remove(Mirrors, i)
						XJB.Util.WriteData("mirrors")
					end
				end
			end
			startTime = STDLua.Timestamp(6)
		end
		local Server = message.parent.parent
		local Prefix = XJB.Util.GetPrefix(message.guild_id or message.parent.guild_id or Server.id)
		if message.content:match("^"..Prefix:escape("Patterns")..".+") then -- Command attempt
			if not XJB.Util.IsBlind(message.channel_id) then
				local OldContent = message.content
				message.content = message.content:match("^"..Prefix:escape("Patterns").."(.+)")
				local Args = message.content:split(" ") or {""}
				local CommandName = table.remove(Args, 1)

				local Aliases = XJB.User.Aliases[message.author.id] or {}
				local Aliased = Aliases[CommandName]
				if Aliased then
					if not IsAliased then -- No recursion :)
						message.content = OldContent:gsub("^"..Prefix:escape("Patterns")..CommandName:escape("Patterns"), Prefix..Aliased)
						return XJB.Util.ParseMessage(message, Silent, false, true)
					else
						return
					end
				end

				if XJB.Commands[CommandName] then
					if UseTimestamp then
						local now = STDLua.Timestamp()
						local last = XJB.User.Times[message.author.id]
						if last and not Silent then
							if (now - last) < XJB.Constants.Timeout then
								XJB.Util.SendMessage(message, (XJB.Util.CommandOutput("⌛ Timeout", "Hey, <@"..message.author.id..">! Slow down!")))
								return false, "Timeout"
							end
						end
						XJB.User.Times[message.author.id] = now
					end
					if message.parent.type ~= 0 then Server = nil end
					local ring = XJB.Util.GetRing(message.author, Server)
					local score = XJB.Util.GetCredit(message.author)
					Ret, Value, Error = XJB.Util.ParseCommand(CommandName, message.content,
						false, message.author,
						XJB.Util.GetRing(message.author, Server), XJB.Util.GetCredit(message.author), XJB.Util.GetRupees(message.author),
						message, Prefix, Silent)
				end
			end
		else
			printf("ParseMessage(): Filters took %.3fms to process.\n", (XJB.Util.TimeFunction(function()
				local Score = XJB.Util.GetCredit(message.author)
				for Pattern, Response in pairs(XJB.Constants.Filters) do
					if message.content:lower():match("^"..Pattern) or message.content:lower():match("%s+"..Pattern) then
						if XJB.Util.CreditRange(Response.Range or "850,1100", Score) then
							if Response.Safe or not message.parent.nsfw then
								Text = Response.Text or Response.Title
								if Text then
									Response.Title = Response.Title or "**🚔 Social Credit Police**"
									if Response.Title == "" then Response.Title = nil end
									Text = Text:gsub(
										"%$ID", message.author.id):gsub(
										"%$USERNAME", message.author.username):gsub(
										"%$DISCRIMINATOR", message.author.discriminator):gsub(
										"%$PING", "<@"..message.author.id..">"):gsub(
										"%$SCORE", Score):gsub(
										"%$NEWSCORE", Score - (Response.Penalty or 0)):gsub(
										"%$PENALTY", Response.Penalty or 0)
									if Response.Text then
										Text = XJB.Util.CommandOutput(Response.Title, Text, Response.Colour or XJB.Constants.Colours.DarkBlue)
										if Response.Image then
											Text.embed.image = {
												url = Response.Image
											}
										end
									end
									XJB.Util.SendMessage(message, Text)
								end
								XJB.Util.SetCredit(message.author, Score - (Response.Penalty or 0))
								break -- Only punish once, please dont exploit to say you fuck kids and only take an owo penalty
							end
						end
					end
				end
			end) * 1000))
		end
	else
		XJB.Util.ParseIntegrations(message)
	end
	if startTime then
		printf("ParseMessage(): It took %.3fms to parse message.\n", (STDLua.Timestamp(6) - startTime) / 1000)
	end

	return Ret, Value, Error
end
