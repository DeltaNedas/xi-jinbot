function XJB.Data.GetUser(ID, UseCached)
	if ID then
		if UseCached ~= false then
			for _, Server in pairs(XJB.Client.servers) do
				if Server.members then
					local User = Server.members:get(ID)
					if User then return User.user, User end
				end
			end
		end

		ID = ID:match("^<@(%d+)>$") or ID:match("^%d+$")
		if not ID then return false, 400 end

		local Data, Error = XJB.Client.rest:request(Route("users/"..ID))
		if not Data then print("GetUser(): Failed to get user "..ID..": "..Error) end
		if Error == 200 then Error = nil end
		return Data, Error
	end
end

function XJB.Data.GetUserByName(Server, Username, Discriminator)
	for _, Member in pairs(Server.members) do
		if type(Member) == "table" then
			local User = Member.user
			if User.username == Username and User.discriminator == Discriminator then
				return User, Member
			end
		end
	end
end

function XJB.Data.GetServer(ID, UseCached)
	if ID then
		if UseCached ~= false then
			for _, Server in pairs(XJB.Client.servers) do
				if Server.id == ID then
					return Server
				end
			end
		end

		ID = ID:match("^%d+$")
		if not ID then return false, 400 end

		local Data, Error = XJB.Client.rest:request(Route("guilds/"..ID))
		if not Data then print("GetServer(): Failed to get server "..ID..": "..Error)  end
		return Data, Error
	end
end

function XJB.Data.GetChannel(ID, ServerID)
	if ID then
		if ServerID then
			local Server = XJB.Data.GetServer(ServerID)
			if not Server then return false, "Invalid Server ID." end
			for _, Channel in pairs(Server.channels or {}) do
				if Channel.id == ID then
					return Channel
				end
			end
		end

		ID = ID:match("^<#(%d+)>$") or ID:match("^%d+$")
		if not ID then return false, 400 end

		local Data, Error = XJB.Client.rest:request(Route("channels/"..ID))
		if not Data then print("GetChannel(): Failed to get channel "..ID..": "..Error) end
		return Data, Error
	end
end

function XJB.Data.GetMessage(ChannelID, ID)
	ID = ID:match("^<@(%d+)>$") or ID:match("^%d+$")
	if not ID then return false, 400 end
	ChannelID = ChannelID:match("^<#(%d+)>$") or ChannelID:match("^%d+$")
	if not ChannelID then return false, 400 end

	local Data, Error = XJB.Client.rest:request(Route("channels/"..ChannelID.."/messages/"..ID))
	if not Data then print("GetMessage(): Failed to get message "..ID.." of "..ChannelID..": "..Error); return nil end
	if not Data.parent then Data.parent = XJB.Data.GetChannel(ChannelID) end
	return Data, Error
end

function XJB.Data.GetRoleBy(Server, PropertyName, Property, Lossy)
	for _, Role in pairs(Server.roles) do
		if type(Role) == "table" then
			if Lossy then
				if Role[PropertyName]:lower():match(Property:lower()) then
					return Role
				end
			end
			if Role[PropertyName] == Property then
				return Role
			end
		end
	end
end

function XJB.Data.GetRoleByName(Server, Name, Lossy)
	return XJB.Data.GetRoleBy(Server, "name", Name, Lossy)
end

function XJB.Data.GetRoleByID(Server, ID)
	return XJB.Data.GetRoleBy(Server, "id", ID)
end

function XJB.Data.SetRole(Server, User, Role, Add)
	local Method = "DELETE"
	if Add then Method = "PUT" end
	local Data, Error = XJB.Client.rest:request(Route("guilds/"..Server.id.."/members/"..User.id.."/roles/"..Role.id),
		{},
		Method
	)
	if Data then
		local RSBM = XJB.User.RSBM
		RSBM[Server.id] = RSBM[Server.id] or {}
		RSBM[Server.id][User.id] = RSBM[Server.id][User.id] or {}
		RSBM[Server.id][User.id][Role.id] = RSBM[Server.id][User.id][Role.id] or {}
		RSBM[Server.id][User.id][Role.id] = Add
		XJB.Util.WriteData("rsbm", RSBM)
		print("SetRole(): Set role "..Role.name.." for "..User.username.."#"..User.discriminator.." on "..Server.name.." to "..tostring(Add or false)..".")
	elseif Add then
		print("SetRole(): Failed to add Role "..Role.id.." to "..User.id..": "..Error)
	else
		print("SetRole(): Failed to remove Role "..Role.id.." from "..User.id..": "..Error)
	end
	return Data, Error
end

function XJB.Data.SetNickname(Server, Name)
	print("set")
	local Data, Error = XJB.Client.rest:request(Route("guilds/"..Server.id.."/members/@me/nick"),
		{
			nick = Name
		},
		"PATCH"
	)
	print("nick")
	if not Data then print("SetNickname(): Failed to set nickname to "..Name..": "..Error)  end
	return Data, Error
end


XJB.Data.UpdateStatus = function(Status)
	return XJB.Client.socket:send(3, {
		game = {
			type = 0,
			name = Status or "with "..tostring(#(XJB.Client.servers)).." servers | "..XJB.Constants.DefaultPrefix.."help"
		},
		afk = false,
		since = 696969,
		status = "online"
	})
end