XJB.Constants.Timeout = 2 * 1000 -- Min time between commands in miliseconds
XJB.Constants.DefaultCredit = 1000
XJB.Constants.DefaultPrefix  = "$"
XJB.Constants.SendTimeout = 1000 -- Min time between sending message in milliseconds
XJB.Constants.DefaultRupees = 0

XJB.Constants.Seconds = {
	Century = 3155760000,
	Decade = 315576000,
	Year = 31557600,
	Month = 2629800,
	Week = 604800,
	Day = 86400,
	Hour = 3600,
	Minute = 60,
	Second = 1 -- DURGASoftWare best CORE JAVA
}

XJB.Constants.Permissions = {
	["CREATE_INSTANT_INVITE"] = 0x00000001,
	["KICK_MEMBERS"] = 0x00000002,
	["BAN_MEMBERS"] = 0x00000004,
	["ADMINISTRATOR"] = 0x00000008,
	["MANAGE_CHANNELS"] = 0x00000010,
	["MANAGE_GUILD"] = 0x00000020,
	["ADD_REACTIONS"] = 0x00000040,
	["VIEW_AUDIT_LOG"] = 0x00000080,
	["VIEW_CHANNEL"] = 0x00000400,
	["SEND_MESSAGES"] = 0x00000800,
	["SEND_TTS_MESSAGES"] = 0x00001000,
	["MANAGE_MESSAGES"] = 0x00002000,
	["EMBED_LINKS"] = 0x00004000,
	["ATTACH_FILES"] = 0x00008000,
	["READ_MESSAGE_HISTORY"] = 0x00010000,
	["MENTION_EVERYONE"] = 0x00020000,
	["USE_EXTERNAL_EMOJIS"] = 0x00040000,
	["CONNECT"] = 0x00100000,
	["SPEAK"] = 0x00200000,
	["MUTE_MEMBERS"] = 0x00400000,
	["DEAFEN_MEMBERS"] = 0x00800000,
	["MOVE_MEMBERS"] = 0x01000000,
	["USE_VAD"] = 0x02000000,
	["PRIORITY_SPEAKER"] = 0x00000100,
	["STREAM"] = 0x00000200,
	["CHANGE_NICKNAME"] = 0x04000000,
	["MANAGE_NICKNAMES"] = 0x08000000,
	["MANAGE_ROLES"] = 0x10000000,
	["MANAGE_WEBHOOKS"] = 0x20000000,
	["MANAGE_EMOJIS"] = 0x40000000
}

XJB.Constants.Changelog = {
	{
		{
			{"Unknown"}
		},
		{
			{"Unknown"}
		},
		{
			{
				[[
Added $PREFIXchanges.
All initial goals are implemented in some way.
1.0 will come out when I:
- Make a dedicated server.
- Port most/all commands to C.
- Implement sharding?]],
				"Fixed `$PREFIXuser` or anything that uses GetAuthor not returning correct avatar for people without avatars.",
				"`$PREFIXchanges` works now.",
				"`$PREFIXchanges` puts color in embed.color now."
			},
			{
				"Made `$PREFIXfeed` work and it now shows output message.",
				"`$PREFIXcheckfeeds` has a new low credit score message"
			},
			{
				[[UTF-8 support, meaning `$PREFIXoutback` `$PREFIX(outback text)` returns "text".
$PREFIXlua now doesnt crash when you call Output(non number/string)]],
				"Fixed long messages causing Connection timed out and not making a creator.",
				"`$PREFIXinvites` *[user id]* now does something.",
				"`$PREFIXcommit` added so I can easily edit out in the field",
				"`$PREFIXcommit` now pushes correctly."
			},
			{
				[[
__Xi JinBot update feed added to `$PREFIXfeed`.__
XKCD switched from atom to rss.
Feeds default is now dynamically set.]],
				[[Fixed update feed and made it replace $ PREFIX with `$PREFIX`.
Feeds are now checked after bot sends restart message.]]
			},
			{
				"***FIXED TRANSLATION COMMANDS YEEEEESSSSSSSSS***"
			},
			{
				[[__Final build before **0.3.0.0**__
You can now add an RSS feed URL to `$PREFIXfeed` with an id.
Commands are now timed.]],
				[[SendMessage() timer has an accurate message and returns values now.
Latest server feed is a server to server thing.]],
				"Performance improved and entire bot was reworked.",
				[[`$PREFIXtest` was added to make debugging easier.
Fixed $PREFIX not being needed for commands]],
				[[
`$PREFIXfeed` now works with RSS.
Changelog indentation fixed.
Feeds work now.]],
				"Improved `$PREFIXfeed` feed list output.",
				"Made commands in changelog bold, exmaple: `$PREFIXhelp`",
				"Fixed a typo in `$PREFIXmath` causing exponents to crash.",
				"`$PREFIXshutdown` now smoothly quits without JSON errors.",
				"Modified hash function for `$PREFIXmeasure`."
			}
		},
		{
			{
				[[**Final major update for a while!**
Improved `$PREFIXmeasure`'s hash function.
I'll work on upgrading the API now.]]
			},
			{
				"Fixed PMs, you can now use them for private commands.",
				"Fixed on-join pm.",
				"Fixed output of help."
			},
			{
				[[Added `$PREFIXrequirements`,
Added `$PREFIXdelete` and
Added `$PREFIXretry`.
`$PREFIXnick` now accepts **MANAGE_NICKNAMES** permission as payment.
*Please note, channel permissions are currently ignored for $PREFIXdelete.*]],
				"Fixed `$PREFIXrequirements` and `$PREFIXhelp`.",
				"Added error message for `$PREFIXinvites`.",
				"Added `delete` switch for `$PREFIXfeed`.",
				"Added version crawler so version and latest changelog are in sync.",
				"`$PREFIXnick` now errors if it has no permissions.",
				"Hacked in a quick fix making `$PREFIXnick` not freeze the bot.",
				[[In changelog **$PREFIXcommand** is now `$PREFIXcommand`
`$PREFIXinvites` works now!]],
				"Fixed `$PREFIXdelete`"
			},
			{
				"Fixed `$PREFIXrequirements`!",
				"Better weeb protection."
			},
			{
				"`$PREFIXuser` now shows account creation time!",
				"`$PREFIXuser` now shows server join time."
			},
			{
				"`$PREFIXnewbies` added. Read up on it.",
				"Made callbacks more modular."
			},
			{
				[[Fixed actual ddos vulnerability.
`$PREFIXnewbies` works now!]],
				[[Fixed feed checker crashing if it fails to get a feed.
Sped up `$PREFIXtest` by using less social credit tests.]],
				"You can now use `$PREFIXemoji` directly on an emoji without copying its id.",
				"Improved `$PREFIXhelp` output.",
				"Fixed very rare 17 char long user ids breaking some commands."
			},
			{
				"Added `$PREFIXreddit`.",
				"Added PewDiePie feed to `$PREFIXfeed`.",
				[[Fixed `$PREFIXnewbies <time> <nothing>` crashing.
Users now save after modification so crashes do not affect it.]],
				"Added `$PREFIXfreeze`.",
				"Improved `$PREFIXfeed` output.",
				"Merged some categories in `$PREFIXreddit`.",
				"Resized reddit images."
			},
			{
				"Added `$PREFIXrupees` and `$PREFIXachievements` systems.",
				"`$PREFIXfeed` list is now ordered alphabetically.",
				"Fixed `GetUser()` and others not checking for numerical ids only."
			},
			{
				[[Added `$PREFIXrupees send`.
Made `$PREFIXinterject` take Rupees.
Fixed `$PREFIXroll` crashing for non integers.]],
				"Made `$PREFIXrupees` more needful.",
				"Fixed some stuff and added a new social credit filter.",
				"`$PREFIXemoji` can now be piped properly.",
				[[
Fixed crash in `$PREFIXroulette`.
`$PREFIXreddit` now supports restricted subreddits and user awards.]],
				"Added reddit premium support for `$PREFIXreddit`."
			},
			{
				[[Added `$PREFIX8ball`.
XJB now supports version numbers >9. Thought it already did but here we are.]],
				"Added `$PREFIXblind`.",
				"Added `$PREFIXmirror`."
			}
		}
	}
}

-- Get version from last version numbers

local function fixVersions(t)
	if type(t) == "table" then
		for i, v in ipairs(t) do
			t[i - 1] = fixVersions(v)
			t[i] = nil
		end
		return t
	end
	return t
end
fixVersions(XJB.Constants.Changelog) -- 0 index a 1-indexed table
fixVersions = nil

local Major = #XJB.Constants.Changelog
local Minor = #XJB.Constants.Changelog[Major]
local Build = #XJB.Constants.Changelog[Major][Minor]
local Patch = #XJB.Constants.Changelog[Major][Minor][Build]

XJB.Constants.Version = string.format("%d.%d.%d.%d", Major, Minor, Build, Patch)
print("Xi JinBot Version "..XJB.Constants.Version)
XJB.Constants.LatestChanges = XJB.Constants.Changelog[Major][Minor][Build][Patch]:gsub("%$PREFIX", XJB.Constants.DefaultPrefix)

XJB.Constants.Colours = {
	Black = 0,
	Blue = 255,
	Brown = 10181180, -- EBIN
	DarkBlue = 170,
	Delta = 43775, -- #00AAFF
	Green = 65450, -- #00FFAA
	Magenta = 16711850,
	Orange = 13412864,
	Pepe = 5868605,
	Pink = 16755370,
	Red = 16666666,
	White = 16777215,
	Yellow = 16776960
}

local Filters = {
	["bad robot$"] = {
		Text = "i know you are but what am i?!",
		Range = "600,1300",
		Colour = XJB.Constants.Colours.Red,
		Safe = true
	},
	["%(%(%(.+%)%)%)"] = { -- (((they)))
		Text = "Hey, $PING! Please don't be anti-semetic, as a result your social credit score is now $NEWSCORE.",
		Penalty = 5
	},
	["[o0口]%s*[o0口%s]+[f]"] = { -- oof
		Text = "Bro u just posted cringe, -$PENALTY SCS",
		Penalty = 5
	},
	["y[e3][e3]+t"] = {
		Text = "Zoom zoom! -$PENALTY SCS",
		Penalty = 5
	},
	["i%s*love%s*the%s*ccp$"] = {
		Text = "I love you too, $USERNAME! <3333",
		Penalty = -2,
		Range = "875,1150",
		Colour = XJB.Constants.Colours.Pink,
		Title = "**❤ CCP**",
		Safe = true
	},
	["based%s+durga%s+sir$"] = {
		Text = "15 rupee are deposit into account",
		Penalty = -15,
		Range = "830,1150",
		Colour = XJB.Constants.Colours.Brown,
		Title = "**☕ DURGA Software Solutions ☕**",
		Safe = true,
		Image = "https://media.discordapp.net/attachments/622862027328585748/622862076360261632/ICON_OF_JAVA.jpeg"
	},
	["hail%s*jinping$"] = {
		Text = "hail hydra",
		Penalty = -1,
		Range = "1000,1100"
	},
	["drumpf"] = {
		Title = "title",
		Text = "orang man bad",
		Range = "600,1300",
		Colour = XJB.Constants.Colours.Orange
	},
	["[o0口uq]%s*[wm]%s*[o0口uqt]"] = { -- OwO
		Text = "Hey, $PING! Please don't be a furry. You have lost $PENALTY social credit.",
		Range = "601,1200",
		Penalty = 15
	},
	yaoi = { -- gay
		Text = "Hey, $PING! Please don't be a weeb. You have lost $PENALTY social credit.",
		Range = "650,1100",
		Penalty = 10
	},
	["lolita"] = {
		Text = "Hey, $PING! Please don't be a pedophile. You have lost $PENALTY social credit.",
		Range = "601,1250",
		Penalty = 10,
		Title = "**🚔 Police**"
	},
	["^>>>/.+/"] = { -- /r/chan
		Text = "Anon, you need to go back.",
		Range = "601,1000",
		Penalty = 5,
		Title = "**🚔 Paid CIA Glowers**"
	},
	["install%s+gentoo$"] = {
		Title = "**💻 Do it.**\nhttps://youtu.be/VjGSMUep6_4",
		Range = "600,1300",
		Safe = true
	},
	["le%s+"] = {
		Text = ">implying\n>muh",
		Range = "601,1100",
		Penalty = 1
	},
	["天安门六四大屠杀?事?件?"] = {
		Title = "**🚔 Police**",
		Penalty = 15,
		Text = "**Nothing** happened in 1980-never.",
		Delete = true,
		601, 1200
	}
}
Filters.yuri = Filters.yaoi
Filters.bukkake = Filters.yaoi
Filters.kawaii = Filters.yaoi
Filters["desu%s*desu"] = Filters.yaoi
Filters.lolis = Filters.lolita
Filters["i%s*%s*fuck%s*kids"] = Filters.lolita
Filters["i%s*%s*like%s*kids"] = Filters.lolita
Filters["christian%s+photos"] = Filters.lolita
Filters["christian%s+pictures"] = Filters.lolita
Filters["p%-pwease"] = Filters["[o0口uq]%s*[wm]%s*[o0口uqt]"]
Filters["我喜欢中国共产党"] = Filters["i%s*love%s*the%s*ccp$"]
XJB.Constants.Filters = Filters

local Integrations = { -- Filters but for bots
	["ID:155149108183695360"] = { -- Dyno Mutes
		["***.-#%d%d%d%d was muted***$"] = {
			Penalty = 50,
			Username = "***(.-)#",
			Discriminator = "#(.-) ",
			Text = "$PING lost 50 Social Credit for being muted."
		}
	},
	["Username:UB3R-B0T"] = {
		["^HECK YES!$"] = {
			Title = "⬆ is a bad bot"
		},
		["^:%(%(%(%(%($"] = {
			Title = "I agree.",
		}
	},
	["ID:398601531525562369"] = {
		[""] = {
			Delete = true -- Delete all partnerbot messages kek
		}
	}
}
Integrations["Username:UB3R-B0T"]["^:D!!!!$"] = Integrations["Username:UB3R-B0T"]["^HECK YES!$"] -- "good bot"
Integrations["Username:UB3R-B0T"]["^T_T$"] = Integrations["Username:UB3R-B0T"]["^:%(%(%(%(%($"] -- "bad bot"
XJB.Constants.Integrations = Integrations

XJB.Constants.RSS = {
	URL = "",
	Name = "",
	Short = "",
	Prefix = "🔧 ",
	GetLatest = function(Data)
		return Data:match("^.-</title>.-</title>%s*<link>%s*(http.-)%s*</link>")
	end,
	GetEmbed = function(Name, Link)
		return "New article from **"..Name.."**!\n"..Link
	end
}

XJB.Constants.Atom = {
	URL = "",
	Name = "",
	Short = "",
	Prefix = "🔧 ",
	GetLatest = function(Data)
		return Data:match('^.-<entry>.-<link.-href="(.-)"%s*/>')
	end,
	GetEmbed = function(Name, Link)
		return "New article from **"..Name.."**!\n"..Link
	end
}

XJB.Constants.FeedLinks = { -- Global, default feeds
	hkfp = {
		URL = "https://www.hongkongfp.com/feed/", -- Required
		Name = "Hong Kong Free Press", -- Required
		Short = "HKFP", -- Required
		Prefix = "🇭🇰 ", -- Optional, For XJB.Util.CommandOutput(Prefix..Short, "text")
		GetLatest = function(Data) -- Required, defaults to RSS
			return Data:match("^.-<item>.-<link>(.-)</link>")
		end,
		GetEmbed = function(Name, Link) -- Required, defaults to RSS
			return "Extra, extra! New article from **"..Name.."**!\n"..Link
		end
	},
	xkcd = {
		URL = "https://xkcd.com/rss.xml",
		Name = "XKCD",
		Short = "XKCD",
		Emoji = "📑 ",
		GetEmbed = function(_, Link, Data)
			local Image = Data:match('^.-(https://imgs%.xkcd%.com/comics/.-%.png)') or "https://cdn.discordapp.com/embed/avatars/"..math.random(0, 4)..".png" -- Fallback to a random default pfp
			local Quote = Data:match('%.-%.png" title="(.-)" alt'):gsub("&amp;quot;", "\"") or "Quote"
			local Title = Data:match('^.-<item>%s*<title>(.-)</title>') or "Title"
			return {
				content = "**New comic from XKCD!**",
				embed = {
					image = {
						url = Image
					},
					fields = {{
						name = "**"..Title:escape("Markdown").."**", -- If he puts `` in it im fucked
						value = "*"..Quote:escape("Markdown").."*"
					}},
					color = XJB.Constants.Colours.White
				}
			} -- Built in viewer for it
		end
	}, xjb = {
		Name = "Xi JinBot Updates",
		Short = "XJB",
		Prefix = "🤖 ",
		GetLatest = function()
			return XJB.Constants.LatestChanges
		end,
		GetEmbed = function(_, Link)
			return {
				content = "**Xi JinBot has updated to __v"..XJB.Constants.Version.."__!**",
				embed = {
					description = Link,
					colour = XJB.Constants.Colours.Blue
				}
			}
		end
	}, pewdiepie = {
		URL = "https://www.youtube.com/feeds/videos.xml?channel_id=UC-lHJZR3Gqxm24_Vd_AJ5Yw",
		Name = "PewDiePie - 100M Club",
		Short = "PewDiePie",
		Prefix = "👊 ",
		GetLatest = XJB.Constants.Atom.GetLatest,
		GetEmbed = function(_, Link)
			return "👊 **PewDiePie uploaded a new video!**\n"..Link
		end
	}
}
