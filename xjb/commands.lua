local STDLua = require("stdlua")
local JSON = require("lunajson")
local MCAPI = require("mcapi")
local Lanes = require("lanes")

XJB.Util.AddCommand("help", {
	types = {
		"info",
		"utility"
	},
	help = function(a)
		return XJB.Util.CommandHelp("help [command or (category)]", [[
Returns help for a command or lists all available command categories for __Xi JinBot__.
Replace an optional field with **-** for its default value.
If the command is surrounded by brackets it will be treated as a category.
A category will list all commands in it. Commands can have multiple categories.
You can also use **(all)** to list all commands.
You can run a command inside **$()** to use its output as an argument to another command.
Adding **[]** to the **$()** will turn it into a loop, put a number inside to make the command loop over.
A \n will be replaced with a new line.
What usage means:
- __<value>__: a value is required
- *[value]*: a value is optional]])
	end,
	func = function(Message, Ring, Score, Rupees, Command)
		if Score < 700 and not (Ring and Ring <= 4) then -- Ring0-4 or >700 Social Credit required
			return XJB.Util.CommandCreditScore("help", "You deserve no help.")
		end
		if Command and not Command:match("^%(.+%)$") then
			local Found = XJB.Commands[Command]
			if Found then
				local Help = Found.help(Message, XJB.Util.GetPrefix(Message.guild_id))
				local Requirements = (XJB.Util.GetRequirements(Command, Message.guild_id) or {}).ring
				if Requirements and Requirements ~= "null" then
					Help.embed.description = Help.embed.description.."\nRequires an execution level of __Ring-**"..Requirements.."**__."
				end
				return Help
			else
				return XJB.Util.CommandError("help", "Command '"..Command.."' not found.")
			end
		end
		return XJB.Util.PrintHelp(Message, Command)
	end,
	aliases = {
		"man",
		"manual",
		"guide"
	}
})

XJB.Util.AddCommand("roll", {
	types = {
		"fun",
		"script",
		"utility"
	},
	help = function()
		return XJB.Util.CommandHelp("roll [min = 1] [max = 6]", "Returns a random number between min and max.")
	end,
	func = function(Message, Ring, Score, Rupees, Min, Max)
		if Score < 700 and not (Ring and Ring <= 4) then -- Ring0-4 or >700 Social Credit required
			return XJB.Util.CommandCreditScore("roll", "Lay off of the dice.")
		end
		if Min == "-" then Min = 1 end
		if Max == "-" then Max = 6 end

		Min = math.round(XJB.Util.ToNumber(Min or "1"))
		Max = math.round(XJB.Util.ToNumber(Max or "6"))
		if not XJB.Util.IsNumberValid(Min) then return XJB.Util.CommandError("roll", "Invalid minimum.") end
		if not XJB.Util.IsNumberValid(Max) then return XJB.Util.CommandError("roll", "Invalid maximum.") end
		if Min > Max then return XJB.Util.XJB.Util.CommandError("roll", "Minimum must be lower than maximum.") end

		local Result = math.random(Min, Max)
		return XJB.Util.CommandOutput(
			("🎲 Roll %d, %d"):format(Min, Max),
			("Rolled a ***%d***!"):format(Result),
			14483456, XJB.Util.ToString(Result))
	end,
	aliases = {
		"random",
		"rand",
		"dice"
	}
})

XJB.Util.AddCommand("chinese", {
	types = {
		"language",
		"utility"
	},
	help = function()
		return XJB.Util.CommandHelp("c/hinese [text]",[[
			With text, it will translate text into **Traditional Chinese**.
			Uses Yandex Translate to translate your text.
			You can use **c** or **chinese**.]])
	end,
	func = function(Message, _, _, Rupees, ...) -- Not even social credit should be a language barrier
		if Message.Looptimes or 1 > 1 then
			return XJB.Util.CommandError("chinese", "You cannot translate in a loop!")
		end
		local Text = table.concat({...}, " ")
		return XJB.Util.TranslateText(Text, "Chinese")
	end,
	aliases = {
		"c",
		"tochinese"
	}
})

XJB.Util.AddCommand("english", {
	types = {
		"language",
		"utility"
	},
	help = function()
		return XJB.Util.CommandHelp("e/nglish [text]", [[
			With text, it will translate text into **English (US)**.
			Uses Yandex Translate to translate your text.
			You can use **e** or **english**.]])
	end,
	func = function(Message, _, _, Rupees, ...)
		if Message.Looptimes or 1 > 1 then
			return XJB.Util.CommandError("english", "You cannot translate in a loop!")
		end
		local Text = table.concat({...}, " ")
		return XJB.Util.TranslateText(Text, "English")
	end,
	aliases = {
		"e",
		"toenglish"
	}
})

XJB.Util.AddCommand("credit", {
	requirements = {
		ring = 0
	},
	types = {
		"admin",
		"info",
		"utility"
	},
	help = function()
		return XJB.Util.CommandHelp("credit [ID = you] [score]", [[
Returns you (or a user)'s social credit.
Score can be prepended with **+** or **-** to add or substract from original score.
If no score is specified then it will not require any execution level.
__**Not affiliated with real life social credit score in China.**__]])
	end,
	func = function(Message, Ring, Score, Rupees, ID, SetScore)
		if (ID == "-" or not ID) and not SetScore then
			return XJB.Util.CommandOutput("🏦 Social Credit", "Your social credit score is **"..tostring(XJB.Util.GetCredit(Message.author)).."**.", XJB.Constants.Colours.Yellow, XJB.Util.GetCredit(Message.author))
		end
		if Score < 700 and not (Ring and Ring <= 4) then -- Ring 0-4 or >700 Social Credit required
			return XJB.Util.CommandCreditScore("credit", "Your own social credit is bad enough!")
		end
		if ID == "-" then ID = Message.author.id end
		ID = ID:match("^<@!?(.+)>$") or ID
		if not ID then
			return XJB.Commands.credit.help()
		end
		if not XJB.Data.GetUser(ID) then
			return XJB.Util.CommandError("credit", "Invalid ID.")
		end
		if not SetScore then
			if ID == XJB.Client.user.id then
				return XJB.Util.CommandOutput("🏦 Social Credit", "**"..Message.author.username.."**: Xi JinBot! What does the scouter say about your social credit!?\n**Me**: It's over **9000**!\n**"..Message.author.username.."**: What? Nine *thousand*!? That can't be right!", XJB.Constants.Colours.Yellow)
			end
			return XJB.Util.CommandOutput("🏦 Social Credit", "**"..XJB.Data.GetUser(ID).username.."**'s social credit score is **"..tostring(XJB.Util.GetCredit(ID)).."**.", XJB.Constants.Colours.Yellow)
		end
		if not XJB.Util.CanRunCommand("credit", Message.guild_id, Ring) then
			return XJB.Util.CommandError("credit", "Setting credit score requires a __Ring-**"..XJB.Util.CanRunCommand("credit", Message.guild_id).ring.."**__ execution level.")
		end
		SetScore = SetScore:gsub("%.%d+", "")
		if SetScore == "-" then
			SetScore = 1000
		elseif SetScore:match("^[%-%+]%d+$") then
			SetScore = XJB.Util.GetCredit(ID) + tonumber(SetScore)
		end
		if not tonumber(SetScore) then
			return XJB.Util.CommandError("credit", "Invalid Social Credit Score.")
		end
		return XJB.Util.SetCredit(ID, math.floor(tonumber(SetScore)))
	end
})

XJB.Util.AddCommand("say", {
	requirements = {
		ring = 1
	},
	types = {
		"admin",
		"fun",
		"script",
		"utility"
	},
	help = function()
		return XJB.Util.CommandHelp("say <text>", [[
Makes me say something.
It can be a JSON embed or plaintext.]])
	end,
	func = function(Message, Ring, Score, Rupees, ...)
		local Text = table.concat({...}, " ")
		if Text == "" then return XJB.Commands.say.help() end
		if not Message.LoopTimes then -- Dont delete or make message if running in loop or subcommand
			if not XJB.Util.CanRunCommand("say", Message.guild_id, Ring) then
				return XJB.Util.CommandError("say", "Ring-__0-1__ execution level is required.")
			end
			Message:delete()
		end
		pcall(function()
			Text, Error = JSON.decode(Text)
		end)
		return Text, Text
	end
})

XJB.Util.AddCommand("shutdown", {
	requirements = {
		ring = 0
	},
	types = {
		"admin",
		"utility"
	},
	help = function()
		return XJB.Util.CommandHelp("shutdown [seconds = 0]", "Shuts Xi JinBot down after x seconds.")
	end,
	func = function(Message, Ring, _, Rupees, Seconds)
		if not XJB.Util.CanRunCommand("shutdown", Message.guild_id, Ring) then
			return XJB.Util.CommandError("shutdown", "Shutdown requires a Ring-__0__ execution level.")
		end
		if Seconds == "-" then Seconds = 0 end
		Seconds = math.floor(tonumber(Seconds or 0))
		if Seconds < 0 then Seconds = 0 end
		if Seconds > 0 then
			local s = "s"
			if Seconds == 1 then s = "" end
			XJB.Util.SendMessage(Message, (XJB.Util.CommandOutput(nil, "💀 Shutting down in **"..Seconds.."** second"..s..". 💀", XJB.Constants.Colours.Red)))
			Lanes.sleep(Seconds)
		end

		XJB.Util.SendMessage(Message, (XJB.Util.CommandOutput(nil, "💀 The sun has set on the CCP. 💀", XJB.Constants.Colours.Red)))
		XJB.Util.Quit()
	end,
	destructive = true
})

XJB.Util.AddCommand("restart", {
	requirements = {
		ring = 0
	},
	types = {
		"admin",
		"utility"
	},
	help = function()
		return XJB.Util.CommandHelp("restart [seconds = 0]", [[
Restarts Xi JinBot after x seconds.
If there is no Xi JinBot is back online message then it is dead.]])
	end,
	func = function(Message, Ring, _, Rupees, Seconds)
		if not XJB.Util.CanRunCommand("restart", Message.guild_id, Ring) then
			return XJB.Util.CommandError("restart", "Restart requires a Ring-__0__ execution level.")
		end
		if Seconds == "-" then Seconds = 0 end
		Seconds = math.floor(tonumber(Seconds) or 0)
		if Seconds < 0 then Seconds = 0 end
		if Seconds > 0 then
			local s = "s"
			if Seconds == 1 then s = "" end
			XJB.Util.SendMessage(Message, (XJB.Util.CommandOutput(nil, "**♻ Restarting in **"..Seconds.."** second"..s..". ♻**", XJB.Constants.Colours.Green)))
			Lanes.sleep(Seconds)
		end

		XJB.Util.SaveCreators()
		XJB.Util.SaveFeeds()
		XJB.News.Stop()
		os.remove("pid")

		XJB.Util.SendMessage(Message, (XJB.Util.CommandOutput(nil, "**♻ Xi JinBot restarting... ♻**", XJB.Constants.Colours.Green)))
		os.execute(("build/xijinbot %s %s '✅ __**Xi JinBot is back online.**__ ✅' %s &"):format(Message.channel_id, Message.id, STDLua.Timestamp()))
		os.exit(0)
	end,
	destructive = true
})

XJB.Commands.ring = {}
XJB.Commands.ring.types = {
	"admin", "info", "utility"
}
XJB.Commands.ring.help = function()
	return XJB.Util.CommandHelp("ring [id] [level = null] [global = false]", [[
Gets or sets a user's execution level.
Setting requires __Ring-***(level - 1)***__ execution level.
If you set a level, you can also specify global.
Global requires a __Ring-**0-1**__ execution level.]])
end
XJB.Commands.ring.func = function(Message, Ring, Score, Rupees, ID, Level, Global)
	if Score < 950 and not (Ring and Ring <= 3) then -- Ring 0-3 or >950 Social Credit required
		return XJB.Util.CommandCreditScore("ring", "You do not need to know that.")
	end
	Global = Global or false
	if type(Global) == "string" then
		Global = toboolean(Global)
		if not Global then
			return XJB.Util.CommandError("ring", "Global must be a boolean.")
		end
	end
	if ID then
		if ID == "-" then ID = Message.author.id end
		ID = ID:match("^<@!?(.+)>$") or ID
		if ID and not XJB.Data.GetUser(ID) then
			return XJB.Util.CommandError("ring", "Invalid ID.")
		end
	else
		ID = Message.author.id
	end
	if not Level then
		local Name = XJB.Data.GetUser(ID).username
		Ring = XJB.Util.GetRing(ID, Message.guild_id)
		if Ring then
			if Ring < 0 then
				Ring = "**Xi Jinping**."
			else
				Ring = "**Ring-__"..tostring(Ring).."__**."
			end
		else
			Ring = "None."
		end
		return XJB.Util.CommandOutput("🤖 "..Name.."'s Execution Level", Ring, XJB.Constants.Colours.Magenta)
	else
		if ID == Message.author.id and not (Ring and Ring <= 0) then
			return XJB.Util.CommandError("ring", "You cannot set your own execution level!")
		end
		if Level:compare("^null$", "^%-$") then
			Level = nil
		else
			Level = tonumber((Level:gsub("%.%d+", "")))
			if not (Level and XJB.Util.IsNumberValid(Level)) then
				return XJB.Util.CommandError("ring", "Invalid execution level.")
			end
		end
		local TargetRing = XJB.Util.GetRing(ID, Message.parent.parent.id)
		if Ring then
			if TargetRing and Ring >= TargetRing and not (Ring and Ring <= 0) then
				return XJB.Util.CommandError("ring", "Setting execution level to "..tostring(Level).." requires Ring-__"..tostring(tonumber(TargetRing) - 1).."__ execution level.")
			end
		elseif not (Ring and Ring <= 0) then
			return XJB.Util.CommandError("ring", "You cannot set someones execution level!")
		end
		if Global and not (Ring and Ring <= 0) then
			return XJB.Util.CommandError("ring", "Setting a global execution level requires a __Ring-**0**__ execution level.")
		end
	end
	local Server = Message.parent.parent.id
	if Global then Server = nil end
	if Message.parent.type == 1 and not Global then
		return XJB.Util.CommandError("ring", "You cannot set a DM-specific execution level.")
	end
	return XJB.Util.SetRing(ID, Level, Server)
end

local function FormatLoadedError(Error, Code) -- Replaces [script "..."] with code
	if Code:match("\n") then
		Code = "(.*)%.%.%."
	else
		Code = Code:escape("Patterns")
	end
	return Error:gsub("^%[string \""..Code.."\"%]", "code")
end

XJB.Util.AddCommand("lua", {
	requirements = {
		ring = -1
	},
	types = {
		"admin",
		"utility"
	},
	help = function()
		return XJB.Util.CommandHelp("lua <code>", "Runs lua5.3 code.")
	end,
	func = function(Message, Ring, _, Rupees, ...)
		local Code = table.concat({...}, " ")
		if Code == "" then return XJB.Commands.lua.help() end
		if not XJB.Util.CanRunCommand("lua", Message.guild_id, Ring) then
			return XJB.Util.CommandError("lua", "😒 Nice RCE, buddy.")
		end
		if Code:byte(1) == 27 then
			return XJB.Util.CommandError("lua", "🚫 No precompiled chunks.")
		end
		local Env = table.copy(_ENV)
		local OutputMessage
		Env.Output = function(...)
			local Out = ""
			for _, v in ipairs{...} do
				Out = Out..tostring(v)..", "
			end
			OutputMessage = (OutputMessage or "").."\n"..Out:gsub(", $", "")
		end
		Env.Message = Message
		Env.STDLua = STDLua
		Env.JSON = JSON
		Env.Commands = XJB.Commands
		Env.Persist = Persist or {}

		local Chunk, Error = load(Code, nil, "t", Env)
		if not Chunk then
			return XJB.Util.CommandError("lua", "Failed to compile: "..FormatLoadedError(Error, Code))
		end

		local Success, Error = xpcall(function()
			Chunk()
		end, debug.traceback)
		print("XJB.Commands.lua: Ran "..Code)
		if not Success then
			return XJB.Util.CommandError("lua", "Failed to run: "..FormatLoadedError(Error, Code):sub(1, 256)), FormatLoadedError(Error, Code)
		end
		Persist = Env.Persist
		OutputMessage = OutputMessage or "Code executed successfully."
		return XJB.Util.CommandOutput("🤖 Lua", OutputMessage)
	end
})

XJB.Commands.reee = {}
XJB.Commands.reee.types = {
	"fun"
}
XJB.Commands.reee.help = function()
	return XJB.Util.CommandHelp("reee [length = 5] [anger = false]", [[
Let out a reee!
Length must be between 3 and 64.
If **anger** is true, it will be all caps.]])
end
XJB.Commands.reee.func = function(Message, Ring, Score, Rupees, Length, Anger)
	if Score < 950 and not (Ring and Ring <= 3) then -- Ring 0-3 or >950 Social Credit required
		return XJB.Util.CommandCreditScore("crying", "re")
	end

	Anger = Anger or false
	if type(Anger) == "string" then
		Anger = toboolean(Anger)
		if not Anger then
			return XJB.Util.CommandError("reee", "Anger must be a boolean.")
		end
	end

	if Length == "-" then Length = 5 end
	Length = math.floor(tonumber(Length) or 5)
	if Length < 3 then Length = 3 end
	if Length > 64 and not (Ring and Ring <= 1) then -- When I find a complete bs bug
		Length = 64
	end
	local Reee = "r"..string.rep("e", Length).."!"
	if Anger then Reee = Reee:upper() end
	return XJB.Util.CommandOutput("😡 Someone's __very__ angry!", "**"..Reee.."**", 16666666)
end

XJB.Commands.measure = {}
XJB.Commands.measure.types = {
	"fun"
}
XJB.Commands.measure.help = function()
	return XJB.Util.CommandHelp("measure [id = you]", [[
Measures a user's length.]])
end
XJB.Commands.measure.func = function(Message, Ring, Score, Rupees, ID)
	if ID then
		if ID == "-" then ID = Message.author.id end
		ID = ID:match("^<@!?(.+)>$") or ID
		if ID and not XJB.Data.GetUser(ID) then
			return XJB.Util.CommandError("ring", "Invalid ID.")
		end
	else
		ID = Message.author.id
	end
	if Score < 700 and not (Ring and Ring <= 4) then -- Ring 0-4 or >700 Social Credit required
		if ID == Message.author.id then
			return XJB.Util.CommandCreditScore("measure", "I'll need a microscope for that...")
		end
		return XJB.Util.CommandCreditScore("measure", "Ha, gay!")
	end
	if ID == XJB.Client.user.id then
		return XJB.Util.CommandError("measure", "ERROR: Embed descriptions support a maximum of 2048 characters.") -- Le ebin kid with sunglasses.png
	end

	Ring = XJB.Util.GetRing(ID, Message.guild_id)
	Score = XJB.Util.GetCredit(ID)

	local Length = 0
	for i = 1, #ID do
		Length = Length + tonumber(ID:sub(i, i))
	end

	Length = Length % 32
	Length = (Score^2 / 10000 - 20) - Length -- Big social credit score = big ???
	if Ring and Ring < 4 then
		Length = Length + (8 - Ring) -- Ring 0-4 get a boost
	end

	if Length < 3 then
		Length = 3
	end

	Length = math.ceil(Length)
	return XJB.Util.CommandOutput("📏 Measurement of __"..XJB.Data.GetUser(ID).username.."__.", "B"..string.rep("=", Length).."D", nil, Length + 2)
end

local Australia = {
	"G'day, mate!",
	"Oi, cunt!",
	"You want a golden gaytime?",
	"I'd fuckin' kill for a bit of Vegemite!",
	"Look out, it's an Emu!",
	"Piss off, wanker.",
	"Fuckin' internet's gone out...",
	"Doorbell's fucked, yell \"Oi, cunt!\" really loud."
}

XJB.Commands.outback = {}
XJB.Commands.outback.types = {
	"fun", "language"
}
XJB.Commands.outback.help = function()
	return XJB.Util.CommandHelp("outback [text]", [[
Flips your text upside down!
Can be applied on upside down text to make it normal.
Default text is a random Australian greeting.
__**Do not use on unicode strings**__, such as output of this.
If you want to read the output of this command ping an Australian.
**For some reason, b and d are not reversed.**]])
end
XJB.Commands.outback.func = function(Message, Ring, Score, Rupees, ...)
	local Text = table.concat({...}, " ")
	if Text == "" or Text == "-" then Text = XJB.Util.Random(Australia) end
	if Score < 700 and not (Ring and Ring <= 4) then -- Ring 0-4 or >700 Social Credit required
		return XJB.Util.CommandCreditScore("outback", "Fook off, cunt!")
	end

	local Ret = ""
	for _, c in pairs(Text:encode()) do
		Ret = (STDLua.EscapeTypes.Flip[c] or STDLua.EscapeTypes.FlipInverse[c] or c)..Ret -- Quick solution
	end

	return XJB.Util.CommandOutput("*🇦🇺 Meanwhile, in the outback...*", Ret, XJB.Constants.Colours.Blue)
end

XJB.Util.AddCommand("math", {
	types = {
		"script", "utility"
	},
	help = function()
		return XJB.Util.CommandHelp("math <number> <operator> <number>", [[
Evaluates a mathematical expression.
Operator can be +, -, (x or *), /, ^ or %.
It can also be their names.]])
	end,
	func = function(Message, Ring, Score, Rupees, First, Operator, Second)
		if Score < 650 and not (Ring and Ring <= 4) then -- Ring 0-4 or >650 Social Credit required
			return XJB.Util.CommandCreditScore("math", "You won't have a calculator everywhere you go!")
		end
		First = tonumber(First)
		Second = tonumber(Second)
		if not (First and Operator and Second) then
			return XJB.Commands.math.help()
		end

		local Result
		if Operator:compare("^%+$", "^plus$", "^add$") then
			Result = First + Second
		elseif Operator:compare("^%-$", "^minus$", "^subtract$") then
			Result = First - Second
		elseif Operator:compare("^%*$", "^x$", "^multiply$") then
			Result = First * Second
		elseif Operator:compare("^/$", "^÷$", "^divide$") then
			if First == 0 then
				return XJB.Util.CommandError("math", "You cannot divide 0 by a number!")
			end
			if Second == 0 then
				return XJB.Util.CommandError("math", "You cannot divide a number by 0!")
			end
			Result = First / Second
		elseif Operator:compare("^%^$", "^exponent$", "^power$") then
			if First ~= 0 then -- 0 ^ 9999 is still 0 :p
				if math.abs(Second) > 128 then
					return XJB.Util.CommandError("math", "You cannot raise or a lower a number to >128, my CPU has feelings too.")
				end
				if First == 1 then
					Result = 1
				end
			elseif Second ~= 1 then
				Result = 0
			end
			if Second == 0 then
				Result = 1
			else
				Result = Result or First ^ Second
			end
		elseif Operator:compare("^%%$", "^modulo$", "^modulous$", "^mod$", "^remainder$") then
			-- https://css-tricks.com/tales-of-a-non-unicorn-a-story-about-the-trouble-with-job-titles-and-descriptions/
			-- "I have never used the or heard of PHP’s modulous operator before or since I failed the fizzbuzz test." - Ben Racicot
			-- big kek
			if First == 0 then
				return XJB.Util.CommandError("math", "You cannot divide 0 by a number!")
			end
			if Second == 0 then
				return XJB.Util.CommandError("math", "You cannot divide a number by 0!")
			end
			Result = First % Second
		else
			return XJB.Util.CommandError("math", "Invalid operator, must be +, -, *, /, ^ or %.")
		end

		local Output = XJB.Util.ToString(Result):gsub("inf", "∞")
		if Output:match("nan") then
			return XJB.Util.CommandError("math", "Result is not a number."), "Result of expression is nan, should be impossible?"
		end

		return XJB.Util.CommandOutput("🗒 "..Message.author.username.."'s expression", "Result: "..Output, 16711935, Result)
	end,
	aliases = {
		"maths",
		"calc",
		"calculator"
	}
})

XJB.Util.AddCommand("issue", {
	types = {
		"issues"
	},
	func = function()
		return XJB.Util.CommandHelp("issue <text>", [[
Notifies Xi Jinping of an issue with or suggestion for **Xi JinBot**.
Please note that it is about the bot, not your server.
**__For server-specific issues, contact your server's owner.__**]])
	end,
	func = function(Message, Ring, Score, Rupees, ...)
		if Score < 750 and not (Ring and Ring <= 3) then -- Ring 0-3 or >750 Social Credit required
			return XJB.Util.CommandCreditScore("issue", "No")
		end
		if Message.LoopTimes then
			return XJB.Util.CommandError("issue", "Issue must be ran on its own.")
		end
		local ID, Error
		if not XJB.User.ShadowBans.Issue[Message.author.id] then
			local Text = table.concat({...}, " ")
			if Text == "" then
				return XJB.Commands.issue.help()
			end
			ID, Error = XJB.Util.CreateIssue{
				m = Message.id,
				c = Message.channel_id,
				u = Message.author.id
			}
			if not ID then
				return XJB.Util.CommandError("issue", "Failed to create issue: "..Error)
			end
			for UID, Ring in pairs(XJB.User.Rings.Global) do
				if Ring <= 0 then
					local User, Error = XJB.Data.GetUser(UID)
					if User then
						XJB.Util.SendMessage(nil, (XJB.Util.CommandOutput("🖊 Issue #"..tostring(ID), "<@"..Message.author.id..">: "..Text)), User)
					else
						print("Commands.issue: Failed to get user "..UID..": "..Error)
					end
				end
			end
		end
		return XJB.Util.CommandOutput(nil, "**🖊 Sent issue to Xi Jinping, thanks!**")
	end,
	aliases = {
		"feedback"
	},
	destructive = true
})

XJB.Util.AddCommand("reply", {
	requirements = {
		ring = 0
	},
	types = {
		"admin", "issues"
	},
	help = function()
		return XJB.Util.CommandHelp("reply <id = latest> <text>", [[
Replies to an issue.
You can replace id with - for latest issue.
If the issue command message was deleted it will PM the user instead.]])
	end,
	func = function(Message, Ring, _, Rupees, ID, ...)
		if not XJB.Util.CanRunCommand("reply", Message.guild_id, Ring) then
			return XJB.Util.CommandError("reply", "You are not **Xi Jinping**!")
		end
		local Text = table.concat({...}, " ")
		if Text == "" or ID == nil then
			return XJB.Commands.reply.help()
		end
		if ID == "-" then ID = table.count(Issues) end
		ID = tonumber(ID)
		if Issues[ID] == nil then
			return XJB.Util.CommandError("reply", "Failed to find issue "..tostring(ID or "<invalid>")..".")
		end
		Text = XJB.Util.CommandOutput("✏ Reply received for issue #"..tostring(ID).."!", "<@"..Issues[ID].u..">: "..Text)
		local Ret, Error = XJB.Util.ReplyIssue(ID, Text)
		if Ret ~= ID then
			return XJB.Util.CommandError("reply", "Failed to send reply: "..Error)
		end
		return XJB.Util.CommandOutput("✏ Reply sent to "..XJB.Data.GetUser(Error.u).username.."!")
	end,
	destructive = true
})

XJB.Commands.block = {}
XJB.Commands.block.types = {
	"admin", "issues"
}
XJB.Commands.block.help = function()
	return XJB.Util.CommandHelp("block <id>", [[
Stops a user from sending issues.
ID can be their user ID or issue id.
Requires you to be Xi Jinping.]])
end
XJB.Commands.block.func = function(Message, Ring, _, Rupees, ID)
	if not (Ring and Ring <= 0) then
		return XJB.Util.CommandError("block", "You are not **Xi Jinping**.")
	end
	if not ID then
		return XJB.Commands.block.help()
	end
	if ID == "-" then ID = table.count(Issues) end
	if Issues[tonumber(ID)] then
		ID = Issues[tonumber(ID)].u
	end
	ID = ID:match("^<@!?(.+)>$") or ID
	if not ID then
		return XJB.Commands.block.help()
	end
	local User = XJB.Data.GetUser(ID)
	if not User then
		return XJB.Util.CommandError("block", "Invalid ID.")
	end
	local Ret, Error = XJB.Util.SetBan("Issue", ID, not XJB.Util.GetBan("Issue", ID))
	if Ret ~= ID then
		return XJB.Util.CommandError("block", "Failed to toggle shadowban: "..Error)
	end
	if Error then
		XJB.Commands.credit.func(Message, 0, 1300, ID, "-100")
	else
		XJB.Commands.credit.func(Message, 0, 1300, ID, "+100")
	end
	return XJB.Util.CommandOutput("🛡 "..User.username.."#"..User.discriminator.." is "..((Error and "now") or "no longer").." blocked.")
end
XJB.Commands.block.destructive = true

XJB.Commands.closeissues = {}
XJB.Commands.closeissues.types = {
	"admin", "issues"
}
XJB.Commands.closeissues.help = function()
	return XJB.Util.CommandHelp("closeissues", [[
Closes all issues and resets counter to 0.
Requires you to be Xi Jinping.]])
end
XJB.Commands.closeissues.func = function(Message, Ring)
	if not (Ring and Ring <= 0) then
		return XJB.Util.CommandError("closeissues", "You are not **Xi Jinping**.")
	end

	if table.count(XJB.User.Issues) == 0 then
		return XJB.Util.CommandError("closeissues", "😊 No issues are open. **Good job!** *(?)*")
	end
	XJB.User.Issues = {}
	local Success, Error = XJB.Util.WriteData("issues")
	if not Success then
		return XJB.Util.CommandError("closeissues", "Failed to delete issues!"), Error
	end
	return XJB.Util.CommandOutput(nil, "**⚔ Closed __all__ issues!**")
end
XJB.Commands.closeissues.destructive = true

XJB.Commands.role = {}
XJB.Commands.role.types = {
	"admin", "server"
}
XJB.Commands.role.help = function()
	return XJB.Util.CommandHelp("role [credit range] [name]", [[
Sets a role to be set for a user in a social credit range.
Default range parameters are 600,1300 and can be replaced with -.
If no role is specified, the range will be removed.
If no range is specified then it will list all roles that use social credit score.
Listing roles requires no execution level.
Example: role -,900 Bad Boys]])
end
XJB.Commands.role.func = function(Message, Ring, Score, Rupees, Range, ...)
	if Score < 900 and not (Ring and Ring <= 4) then -- Ring 0-4 or >900 Social Credit required
		return XJB.Util.CommandCreditScore("role")
	end
	local Server = Message.parent.parent
	if Range then
		if not (Ring and Ring <= 1) then
			return XJB.Util.CommandError("role", "Setting a role requires a Ring-__0-1__ execution level.")
		end
		local Name = table.concat({...}, " ")
		local Role
		if Name ~= "" then
			Role =XJB.Data.GetRoleByName(Server, Name:escape(Patterns), true)
			if not Role then
				return XJB.Util.CommandError("role", "No role found called "..Name..".")
			end
		end
		local Min, Max = Range:match("(.+),(.+)")
		if Min == "-" then Min = 600 end
		if Max == "-" then Max = 1300 end
		Min = tonumber(Min)
		Max = tonumber(Max)
		if not (Min and Max) then
			return XJB.Util.CommandError("role", "Invalid range.")
		end
		if Min < 600 then Min = 600 end
		if Min > 1300 then Min = 1300 end
		if Max < 600 then Max = 600 end
		if Max > 1300 then Max = 1300 end
		if Max < Min then
			return XJB.Util.CommandError("role", "Invalid range.")
		end
		XJB.Data.SetScoreRole(Server, Min..","..Max, Role.id)
		if Name == "" then
			return XJB.Util.CommandOutput("📜 Social Credit Role", "Reset Scores "..Min.." -> "..Max..".")
		end
		return XJB.Util.CommandOutput("📜 Social Credit Role", "Set Scores "..Min.." -> "..Max.." to be role "..Name)
	end

	local Roles = XJB.User.Roles

	if table.count(Roles[Server.id] or {}) == 0 then
		return XJB.Util.CommandOutput("📜 Role list", "There are no credit score roles for this server.")
	end
	local Ret = "**Score Range** - **Role**"
	for Range, ID in pairs(Roles[Server.id]) do
		local Name = (XJB.Data.GetRoleByID(Server, ID) or {}).name or "Invalid Role"
		Ret = Ret.."\n"..Range.." - "..Name
	end
	return XJB.Util.CommandOutput("📜 Role list", Ret)
end

local Nicks = {}
XJB.Util.AddCommand("nick", {
	requirements = {
		ring = 1
	},
	types = {
		"admin",
		"server"
	},
	help = function()
		return XJB.Util.CommandHelp("nick [nickname]", [[
Sets my nickname.
If no nickname is present it will be reset back to **Xi JinBot**.]])
	end,
	func = function(Message, Ring, _, Rupees, ...)
		print("nick start")
		if not (XJB.Util.HasPermission(Message.author.id, "MANAGE_NICKNAMES", Message.guild_id, Message.parent) or Message.parent.type ~= 0 or XJB.Util.CanRunCommand("nick", Message.guild_id, Ring)) then
			return XJB.Util.CommandError("nick", "Access denied.")
		end

		if Nicks[Message.guild_id] then
			return XJB.Util.CommandError("nick", "If I let you do that I would freeze. *Please wait for me to restart.*")
		end
		Nicks[Message.guild_id] = true

		local Name = table.concat({...}, " ")
		if Name == "" then Name = "Xi JinBot" end
		if #Name > 32 then
			return XJB.Util.CommandError("nick", "Nickname must be 32 characters or shorter.")
		end

		local Channel = Message.parent
		if Channel.type == 1 then
			return XJB.Util.CommandError("nick", "Nickname cannot be set in DMs.")
		end

		local Server = Channel.parent
		local Success, Error = XJB.Data.SetNickname(Server, Name)
		if not Success then
			if Error == 403 then
				return XJB.Util.CommandError("nick", "I need the **Change Nickname** permission!")
			end
			return XJB.Util.CommandError("nick", "Failed to set nickname!"), nil, Error
		end

		XJB.Util.SendMessage(Message, (XJB.Util.CommandOutput("📝 Nickname", "Set to **"..Name.."**!"))) -- Crashes it idk why
		print("end")
		return nil, Name
	end,
	aliases = {
		"setnick",
		"nickname",
		"setnickname"
	},
	destructive = true -- not really but it freezes $test
})

local Info = {
	version = function()
		return "**Version** - Xi JinBot v"..XJB.Constants.Version
	end,
	ping = function()
		return "**Ping** - todo :)"
	end,
	commands = function()
		return "**Commands** - "..(table.count(XJB.Commands) - table.count(XJB.Aliases))
	end,
	prefix = function(Message)
		return "**Prefix** - "..XJB.Util.GetPrefix(Message.parent.parent)
	end,
	translation = function()
		return "**Translation API** - Yandex Translate"
	end
}

XJB.Util.AddCommand("info", {
	types = {
		"info",
		"utility"
	},
	help = function()
		return XJB.Util.CommandHelp("info [include]", [[
Returns information about **Xi JinBot**.
You can specify a list of things to only include.
Replaces spaces with underscores (_).
__**Example**__: info version]].."\n"..Info.version())
	end,
	func = function(Message, Ring, Score, Rupees, ...)
		if Score < 750 and not (Ring and Ring <= 5) then -- Ring 0-5 or >750 Social Credit required
			return XJB.Util.CommandCreditScore("role")
		end

		local Include = {...}
		if #Include == 0 then
			for Name, _ in opairs(Info) do
				table.insert(Include, Name)
			end
		end

		local Ret = ""

		for _, Thing in ipairs(Include) do
			if Info[Thing:lower()] then
				Ret = Ret.."\n"..Info[Thing:lower()](Message, Ring, Score)
			end
		end
		return XJB.Util.CommandOutput("🔧 Information about Xi JinBot", Ret)
	end,
	aliases = {
		"information",
		"botinfo"
	}
})

XJB.Commands.exec = {}
XJB.Commands.exec.types = {
	"script"
}
XJB.Commands.exec.help = function()
	return XJB.Util.CommandHelp("exec <command>", "Does nothing, use for loops and stuff.")
end
XJB.Commands.exec.func = function() end

XJB.Util.AddCommand("ebin", {
	types = {
		"fun"
	},
	help = function()
		math.randomseed(#("Makes your message more epic"))
		local Output = table.pack(XJB.Util.CommandHelp("ebin <text>", "Makes your message more epic"..XJB.Misc.EbinFace()))
		math.randomseed(os.time())
		return table.unpack(Output)
	end,
	func = function(Message, Ring, Score, Rupees, ...)
		if Score < 750 and not (Ring and Ring <= 5) then -- Ring 0-5 or >750 Social Credit required
			return XJB.Util.CommandCreditScore("ebin", "You are not epic enough for this command.") -- dont worry its transformed too
		end
		local Epic = table.concat({...}, " ")
		if Epic == "" then
			return XJB.Commands.ebin.help()
		end
		math.randomseed(#Epic)
		local Output = table.pack(XJB.Util.CommandOutput("<:spurdo:608826580269137925> diz iz fuggign ebin! :DDD", XJB.Misc.Ebin(Epic)..XJB.Misc.EbinFace(), XJB.Constants.Colours.Brown))
		math.randomseed(os.time())
		return table.unpack(Output)
	end,
	aliases = {
		"eben",
		"spurdo"
	}
})

Interjection = "I'd just like to interject for a moment. What you’re referring to as Linux, is in fact, GNU/Linux, or as I’ve recently taken to calling it, GNU plus Linux. Linux is not an operating system unto itself, but rather another free component of a fully functioning GNU system made useful by the GNU corelibs, shell utilities and vital system components comprising a full OS as defined by POSIX. Many computer users run a modified version of the GNU system every day, without realizing it. Through a peculiar turn of events, the version of GNU which is widely used today is often called “Linux”, and many of its users are not aware that it is basically the GNU system, developed by the GNU Project. There really is a Linux, and these people are using it, but it is just a part of the system they use. Linux is the kernel: the program in the system that allocates the machine’s resources to the other programs that you run. The kernel is an essential part of an operating system, but useless by itself; it can only function in the context of a complete operating system. Linux is normally used in combination with the GNU operating system: the whole system is basically GNU with Linux added, or GNU/Linux. All the so-called “Linux” distributions are really distributions of GNU/Linux!"

XJB.Commands.interject = {}
XJB.Commands.interject.types = {
	"fun",
	"rupees"
}
XJB.Commands.interject.help = function()
	return XJB.Util.CommandHelp("interject", [[
Brings forth the GNU/Linux Interjection.
**Beware that it __will__ drain your social credit!**]])
end
XJB.Commands.interject.func = function(Message, Ring, Score, Rupees)
	if Score < 950 and not (Ring and Ring <= 3) then -- Ring 0-3 or >950 Social Credit required
		return XJB.Util.CommandCreditScore("interject", "It's GANOOO/linocks...")
	end

	if (Message.LoopTimes or 1) > 1 then
		return XJB.Util.CommandError("interject", "I can't do it again... I'm not strong enough.")
	end

	local Paying = Score < 1200 and not (Ring and Ring <= 2) -- 1200 social credit AND YOUR KIDS INTERJECT FREE!!!
	if Rupees < 10 and Paying then -- 10 SCS per interjection
		return XJB.Util.CommandNeedful("interject", 10)
	end
	if Paying and not Message.Testing then
		XJB.Util.SetRupees(Message.author, Rupees - 10)
		XJB.Util.SendMessage(Message, (XJB.Util.CommandOutput("📙 Thank you, come again when needful!", "10 Rupees have been taken from your account.", XJB.Constants.Colours.Orange)))
	end
	return XJB.Util.CommandOutput("📙 Install __THIS__!", Interjection, XJB.Constants.Colours.Orange)
end

XJB.Util.AddCommand("botinvite", {
	types = {
		"admin",
		"server"
	},
	help = function()
		return XJB.Util.CommandHelp("botinvite", "Returns an invite link if you want to add me to your server.")
	end,
	func = function(Message, Ring, Score)
		if Score < 800 and not (Ring and Ring <= 5) then -- Ring 0-5 or >800 Social Credit required
			return XJB.Util.CommandCreditScore("botinvite", "www.google.com")
		end
		return {
			embed = {
				url = "https://discordapp.com/oauth2/authorize?client_id=606919446077898763&scope=bot",
				title = "💌 Xi JinBot invite link.",
				description = "**"..XJB.Util.Random(XJB.Misc.InviteMessages):gsub("$USERNAME", Message.author.username).."**",
				color = XJB.Constants.Colours.Delta
			}
		}, "https://discordapp.com/oauth2/authorize?client_id=606919446077898763&scope=bot"
	end,
	aliases = {
		"invite",
		"invitebot"
	}
})

XJB.Commands.afk = {}
XJB.Commands.afk.types = {
	"fun", "utility"
}
XJB.Commands.afk.help = function()
	return XJB.Util.CommandHelp("afk [message]", "Tells everyone that you are afk (**A**way **F**rom **K**eyboard).")
end
XJB.Commands.afk.func = function(Message, Ring, Score, Rupees, ...)
	if Score < 900 and not (Ring and Ring <= 4) then -- Ring 0-4 or >900 Social Credit required
		return XJB.Util.CommandCreditScore("afk", "Don't forget, you're here forever.")
	end
	local Text = table.concat({...}, " ")
	if #Text > 0 then
		if #Text > 64 and not (Ring and Ring <= 2) then
			return XJB.Util.CommandError("afk", "AFK message must be no longer than 64 characters.")
		end
		Text = ": **"..Text.."**"
	else
		Text = "."
	end
	return XJB.Util.CommandOutput("💤 Away From Keyboard", "**"..Message.author.username.."** is now AFK"..Text)
end

XJB.Commands.crop = {}
XJB.Commands.crop.types = {
	"script", "utility"
}
XJB.Commands.crop.help = function()
	return XJB.Util.CommandHelp("crop <length> <text>", [[
Crops text to a certain size.
If you specify a range like **4,10** it will crop from a different start point.
__You can use negative numbers.__
Example: crop 3,-3 abcdefgh
Returns: cdef]])
end
XJB.Commands.crop.func = function(Message, Ring, Score, Rupees, Length, ...)
	if Score < 700 and not (Ring and Ring <= 5) then -- Ring 0-5 or >700 Social Credit required
		return XJB.Util.CommandCreditScore("crop")
	end
	if not Length then
		return XJB.Commands.crop.help()
	end
	if Length:match("^.+,.+$") then
		local Start = tonumber(Length:match("^(.+),"))
		local End = tonumber(Length:match(",(.+)$"))
		if not (Start and End) then
			return XJB.Commands.crop.help()
		end
		Length = {Start, End}
	elseif tonumber(Length) then
		Length = {1, tonumber(Length)}
	end
	local Text = table.concat({...}, " ")
	if #Text == 0 then
		return XJB.Commands.crop.help()
	end
	local Limit = 512 + ((Score - 1000) / 100) * 64
	if #Text > Limit and not (Ring and Ring <= 3) then
		return XJB.Util.CommandError("crop", "Text must be at most "..tonumber(Limit).." characters long.")
	end
	return XJB.Util.CommandOutput("✂ Cropped Text", Text:sub(Length[1], Length[2]))
end

XJB.Commands.cat = {}
XJB.Commands.cat.types = {
	"fun", "script"
}
XJB.Commands.cat.help = function()
	return XJB.Util.CommandHelp("cat <text>", [[
Con**cat**enates the text without any whitespace.
You can escape whitespace by putting **\** before it.]])
end
XJB.Commands.cat.func = function(_, Ring, Score, Rupees, ...)
	if Score < 700 and not (Ring and Ring <= 5) then -- Ring 0-5 or >700 Social Credit required
		return XJB.Util.CommandCreditScore("cat", "Meow!")
	end
	local Text = table.concat({...}, " ")
	Text = Text:gsub(
		"([^\\])%s", "%1"):gsub( -- Remove not escaped spaces
		"\\(%s)", "%1") -- Remove \ from space
	return XJB.Util.CommandOutput("🐱 Concatenation", Text)
end

XJB.Util.AddCommand("xjb_prefix", {
	requirements = {
		ring = 1
	},
	types = {
		"admin",
		"server"
	},
	help = function()
		return XJB.Util.CommandHelp("xjb_/prefix [prefix = "..XJB.Constants.DefaultPrefix.."]", [[
Sets this servers prefix.
If no prefix is specified then it defaults to **]]..XJB.Constants.DefaultPrefix.."**.")
	end,
	func = function(Message, Ring, _, Rupees, ...)
		if not XJB.Util.CanRunCommand("xjb_prefix", Message.guild_id, Ring) then
			return XJB.Util.CommandError("prefix", "Setting the server's prefix requires a __Ring-**0-1**__ execution level.")
		end
		if Message.parent.type == 1 then
			return XJB.Util.CommandError("prefix", "Prefix cannot be set for DMs.")
		end
		local Prefix = table.concat({...}, " ")
		if Prefix == "" or Prefix == "-" then
			Prefix = XJB.Constants.DefaultPrefix
		end
		if #Prefix > 64 and not (Ring and Ring <= 0) then
			return XJB.Util.CommandError("prefix", "Prefix must be at most 64 characters (For bible verses, duh).")
		end
		return XJB.Util.SetPrefix(Message.parent.parent, Prefix)
	end,
	aliases = {
		"prefix"
	},
	destructive = true
})

local Brainlets = {
	"609545589914337311",
	"607704866055389324",
	"609546548325646347",
	"609546594475573259",
	"609546489475104820",
	"607704898565439489",
	"609545544423178261",
	"609546433372094474",
	"607704932031922176",
	"609547054171160587",
	"607704740624728065" -- Xi Jinping :*
}

local Pepes = {
	"611249079401578534",
	"611249029350817812",
	"611248506933346304",
	"611246890771677189",
	"611248480824066049",
	"611250227059359744",
	"611250204364111872",
	"611733415401750586",
	"612036281014288561",
	--[["470903663376859137",
	"577211414641508353",
	"556890374313082958",
	"360574081453522944"]]
}

local Wojaks = {
	"611257353882304521",
	"611257100764577822",
	"611257100429164554",
	"611257099086725149",
	"611257102765260800",
	"611257098784735252",
	"611257098730340361",
	"611257102400356362",
	"611257352972271636"
}

local function RandomEmoji()
	return XJB.Util.Random(XJB.Misc.Emojis)
end

XJB.Commands.emoji = {}
XJB.Commands.emoji.types = {
	"fun", "utility"
}
XJB.Commands.emoji.help = function()
	return XJB.Util.CommandHelp("emoji [id]", [[
Sends an emoji from __any__ server *that Xi JinBot is on*.
Say **\\:emoji_name:** to get the id.
When called without an id it returns a random emoji.
Does not work for animated emojis.
*If you want access to more emojis, buy nitro or get Xi Jinbot on more servers (pls :p).*
Example: emoji 608826580269137925
Returns: <:spurdo:608826580269137925>]])
end
XJB.Commands.emoji.func = function(Message, Ring, Score, Rupees, ID)
	if Score < 700 and not (Ring and Ring <= 4) then -- Ring 0-4 or >700 Social Credit required
		return XJB.Util.CommandCreditScore("emoji", "😡")
	end
	if not (ID) then
		local Emoji = RandomEmoji()
		return XJB.Util.CommandOutput(nil, Emoji.." **Emoji!**", nil, Emoji)
	end
	ID = ID:match("^<:%w+:(%d+)>$") or ID
	if #ID ~= 18 then
		return XJB.Util.CommandError("emoji", "Invalid emoji ID.")
	end
	local URL = "https://cdn.discordapp.com/emojis/"..ID..".png?width=64&height=64"
	local Success = STDLua.Download(URL)
	if not Success then
		return XJB.Util.CommandError("emoji", "Emoji does not exist.")
	end
	return {
		content = RandomEmoji().." **Your emoji, sir.**",
		embed = {
			image = {
				url = URL
			},
			color = XJB.Constants.Colours.Yellow
		}
	}, "<:pleaseaddxijinbot:"..ID..">"
end

XJB.Commands.brainlet = {}
XJB.Commands.brainlet.types = {
	"fun"
}
XJB.Commands.brainlet.help = function()
	return XJB.Util.CommandHelp("brainlet", "Returns a random brainlet emoji.")
end
XJB.Commands.brainlet.func = function(Message, Ring, Score)
	if Score < 900 and not (Ring and Ring <= 4) then -- Ring 0-4 or >900 Social Credit required
		return XJB.Util.CommandOutput("🤪 Brainlet", "⬆") -- Returns his avatar kekekekek
	end
	local Brainlet = XJB.Util.Random(Brainlets)
	local BrainletEmoji = "<:brainlet:"..Brainlet..">"
	Brainlet = "https://cdn.discordapp.com/emojis/"..Brainlet..".png?width=64&height=64"
	return {
		embed = {
			image = {
				url = Brainlet
			},
			color = XJB.Constants.Colours.White,
			author = 1
		}
	}, BrainletEmoji
end

XJB.Commands.pepe = {}
XJB.Commands.pepe.types = {
	"fun"
}
XJB.Commands.pepe.help = function()
return XJB.Util.CommandHelp("pepe", "Returns a random pepe emoji.")
end
XJB.Commands.pepe.func = function(Message, Ring, Score)
	if Score < 900 and not (Ring and Ring <= 4) then -- Ring 0-4 or >900 Social Credit required
		return XJB.Util.CommandCreditScore("pepe", "feel man 🐸")
	end
	local Pepe = XJB.Util.Random(Pepes)
	local PepeEmoji = "<:pepe:"..Pepe..">"
	Pepe = "https://cdn.discordapp.com/emojis/"..Pepe..".png?width=64&height=64"
	return {
		content = "🐸 **Feels bad man...**",
		embed = {
			image = {
				url = Pepe
			},
			color = XJB.Constants.Colours.Pepe,
			author = 1
		}
	}, PepeEmoji
end

XJB.Commands.wojak = {}
XJB.Commands.wojak.types = {
	"fun"
}
XJB.Commands.wojak.help = function()
return XJB.Util.CommandHelp("wojak", "Returns a random non-brainlet wojak emoji.")
end
XJB.Commands.wojak.func = function(Message, Ring, Score)
	if Score < 900 and not (Ring and Ring <= 4) then -- Ring 0-4 or >900 Social Credit required
		return XJB.Util.CommandCreditScore("wojak", "incelbro 👨")
	end
	local Wojak = XJB.Util.Random(Wojaks)
	local WojakEmoji = "<:wojak:"..Wojak..">"
	Wojak = "https://cdn.discordapp.com/emojis/"..Wojak..".png?width=64&height=64"
	return {
		content = "👨 **anger**",
		embed = {
			image = {
				url = Wojak
			},
			color = XJB.Constants.Colours.White,
			author = 1
		}
	}, WojakEmoji
end

XJB.Commands.gerald = {}
XJB.Commands.gerald.types = {
	"admin", "fun"
}
XJB.Commands.gerald.help = function()
	return XJB.Util.CommandHelp("gerald [id] [channelid = current]", [[
Returns something gerald said.
Run with a message id to save it.
Saving a message requires __Ring-**0-2**__ execution level.]])
end
XJB.Commands.gerald.func = function(Message, Ring, Score, Rupees, ID, ChannelID)
	if Message.author.id == "597423895007592451" or (Score < 900 and not (Ring and Ring <= 3)) then -- Ring 0-3 or >900 Social Credit required, You cannot be gerald.
		return XJB.Util.CommandOutput("🤪 Gerald said...", Message.clean) -- Returns his text kekekekekekek
	end

	if ID then
		if not (Ring and Ring <= 2) then
			return XJB.Util.CommandError("gerald", "Saving a message requires __Ring-**0-2**__ execution level.")
		end

		local message = XJB.Data.GetMessage(ChannelID or Message.parent.id, ID)
		if not message then
			return XJB.Util.CommandError("gerald", "Invalid ID.")
		end

		if message.author.id ~= "597423895007592451" then
			return XJB.Util.CommandError("gerald", "Gerald did not say that.")
		end

		return XJB.Util.AddGerald(message.content)
	end
	return XJB.Util.CommandOutput("🤪 Gerald said...", "*"..XJB.Util.Random(XJB.User.Gerald).."*")
end

XJB.Commands.prepend = {}
XJB.Commands.prepend.types = {
	"script"
}
XJB.Commands.prepend.help = function()
	return XJB.Util.CommandHelp("prepend <prepend> <text>", [[
Prepends a string to some text.
\n is replaced with a new line.]])
end
XJB.Commands.prepend.func = function(_, Ring, Score, Rupees, Prepend, ...)
	if Score < 800 and not (Ring and Ring <= 5) then -- Ring 0-5 or >800 Social Credit required
		return XJB.Util.CommandCreditScore("prepend", "prependtext")
	end
	if not Prepend then
		return XJB.Commands.prepend.help()
	end
	Prepend = Prepend

	local Text = table.concat({...}, " ")
	if Text == "" then
		return XJB.Commands.prepend.help()
	end

	return XJB.Util.CommandOutput("🔌 Prepend", Prepend..Text)
end

XJB.Commands.append = {}
XJB.Commands.append.types = {
	"script"
}
XJB.Commands.append.help = function()
	return XJB.Util.CommandHelp("append <append> <text>", [[
Appends a string to some text.
\n is replaced with a new line.]])
end
XJB.Commands.append.func = function(_, Ring, Score, Rupees, Append, ...)
	if Score < 800 and not (Ring and Ring <= 5) then -- Ring 0-5 or >800 Social Credit required
		return XJB.Util.CommandCreditScore("append", "textappend")
	end
	if not Append then
		return XJB.Commands.append.help()
	end
	Append = Append

	local Text = table.concat({...}, " ")
	if Text == "" then
		return XJB.Commands.append.help()
	end

	return XJB.Util.CommandOutput("Append 🔌", Text..Append)
end

XJB.Util.AddCommand("feed", { -- IP Leak vulnerability by making it request a honeypot. keep in mind or use VPS.
	requirements = {
		ring = 1
	},
	types = {
		"admin",
		"feeds",
		"server"
	},
	help = function()
		return XJB.Util.CommandHelp("feed <source id / url> <channel = current / new id> [newchannel = current]", [[
Sets this server's feed channel for a certain source.
A channel of "**null**" will disable it.
Run with no arguments to list all available sources.
You can supply your own Atom/RSS feed by using a link to it and a new id.
A channel of "**delete**" will delete the custom feed.
*newchannel* is only used when creating a feed.
Listing requires no execution level.]])
	end,
	func = function(Message, Ring, _, Rupees, Source, ChannelID, NewChannel)
		if not Source then
			local Fields = table.new{{
				name = "**Feed display name**",
				value = "Source ID - Channel *(if any)*"
			}}

			local Description
			local Added = {}
			for _, Feed in opairs(XJB.Constants.FeedLinks) do -- Builtin feeds
				if #Fields < 25 then
					Fields:insert{
						name = "**"..Feed.Name.."**",
						value = Feed.Short
					}
					Added[Feed.Name] = #Fields
				else
					Description = true
				end
			end

			local ServerFeedList = XJB.User.Feeds[Message.guild_id]
			if ServerFeedList then
				if #Fields < 25 then
					for Index, Feed in opairs(ServerFeedList) do
						if #Fields < 25 then
							local Value = Feed.Short or Index
							if Feed.Channel then
								local Channel = XJB.Data.GetChannel(Feed.Channel or "")
								if Channel then
									Value = Value.." - <#"..Channel.id..">"
								else
									Feed.Channel = nil
								end
							end

							local Name = Added[Feed.Name] or XJB.Constants.FeedLinks[Index:lower()].Name
							if Added[Name] then
								Fields[Added[Name]].value = Value
							else
								Fields:insert{
									name = "**"..Name.."**",
									value = Value
								}
							end
						else
							Description = true
						end
					end
				else
					Description = true
				end
			end

			return {
				content = "📧 **Available feeds for __"..XJB.Data.GetServer(Message.guild_id).name.."__**",
				embed = {
					fields = Fields,
					color = XJB.Constants.Colours.Delta,
					description = Description and "***Only showing 25***"
				}
			}
		end

		if not XJB.Util.CanRunCommand("feed", Message.guild_id, Ring) then
			return XJB.Util.CommandError("feed", "Setting a feed channel requires a __Ring-**0-1**__ execution level.")
		end

		local FeedLinks = XJB.Constants.FeedLinks
		local Feeds = XJB.User.Feeds

		local Feed
		local GlobalFeed = FeedLinks[Source:lower()]
		local ServerFeed = (Feeds[Message.guild_id] or {})[Source:lower()]
		Feed = GlobalFeed or ServerFeed

		if not Feed then
			if not ChannelID then
				return XJB.Commands.feed.help()
			end

			if GlobalFeed then
				return XJB.Util.CommandError("feed", "Feed called **"..ChannelID:lower().."** already exists.")
			end

			if not (Source:match("https?://www%..-%.%w-/") or Source:match("https?://.-%.%w-/")) then -- www.site, site
				return XJB.Util.CommandError("feed", "Invalid source.")
			end

			local Data, Error = STDLua.Download(Source)
			if not Data then
				return XJB.Util.CommandError("feed", "Failed to retrieve feed from "..Source.."."), Error
			end

			local Major, Minor = Data:match('^<%?xml version%s*=%s*"1%.0" encoding%s*=%s*"[uU][tT][fF]%-8"%?>%s*<rss version%s*=%s*"(%d)%.(%d)".->.+</rss>%s*$')
			local Type = "RSS"
			if not (Major and Minor) then
				Major, Minor = Data:match('^<%?xml version%s*=%s*"1%.0" encoding%s*=%s*"[uU][tT][fF]%-8"%?>%s*<feed.-%s*xmlns="http://www%.w3%.org/(2005)/(Atom)">.-</feed>%s*$')
				if not (Major and Minor) then
					return XJB.Util.CommandError("feed", "Supplied feed is not RSS or Atom.")
				end
				Type = "Atom"
			end

			if (Type == "RSS" and not (Major == "2" and Minor == "0")) or (Type == "Atom" and not (Major == "2005" and Minor == "Atom")) then
				return XJB.Util.CommandError("feed", "Unsupported version.")
			end

			local Name = Data:match("^.-<channel><title>(.-)</title>") or Data:match("<title>(.-)</title>")
			if ChannelID == "-" then ChannelID = Name end

			Feed = {
				URL = Source,
				Name = Name,
				Short = ChannelID,
				Prefix = XJB.Constants[Type].Prefix
			}
			(Feeds[Message.guild_id] or {})[Feed.Short:lower()] = Feed;

			local Success, Error = XJB.Util.WriteData("feeds", Feeds)
			if Success then
				XJB.Util.SendMessage(Message, (XJB.Util.CommandOutput(Feed.Prefix.." Feed", "Set feed for **"..Feed.Name.." to "..Feed.URL.."**!")))
			else
				return XJB.Util.CommandError("feed", "__**Failed to save feeds! Sorry!!!**__"), Error
			end
			Source = ChannelID
			ChannelID = NewChannel
		end
		Source = Source:lower()

		local Prefix = Feed.Prefix or ""
		local Name = Feed.Name
		local Short = Feed.Short

		local Channel
		if ChannelID and ChannelID ~= "-" then
			if ChannelID == "null" then
				if not (Feeds[Message.guild_id] or {})[Short] then
					return XJB.Util.CommandError("feed", "There is no feed channel for "..Name.." set.")
				end
				local Success, Message = XJB.Util.SetFeed(Short, Message.guild_id)
				if not Success then
					return XJB.Util.CommandError("feed", "Failed to remove feed channel!"), nil, Message
				end
				return XJB.Util.CommandOutput(Prefix..Short, "Removed feed channel.")
			elseif ChannelID == "delete" then
				if not (Feeds[Message.guild_id] or {})[Short] then
					return XJB.Util.CommandError("feed", "There is no feed called **"..Name.."**.")
				end

				if XJB.Constants.FeedLinks[Short] then
					return XJB.Util.CommandError("feed", "You cannot delete a built-in feed!")
				end

				Feeds[Message.guild_id][Short] = nil
				if table.count(Feeds) == 0 then
					XJB.User.Feeds[Message.guild_id] = nil -- Save data and fix empty custom feeds stuff
				end

				local Success, Message = XJB.Util.WriteData("feeds")
				if not Success then
					return XJB.Util.CommandError("feed", "Failed to delete feed!"), nil, Message
				end
				return XJB.Util.CommandOutput(Prefix..Short, "Deleted feed **"..Short.."**.")
			else
				ChannelID = ChannelID:match("^<#(.+)>$") or ChannelID
				Channel = XJB.Data.GetChannel(ChannelID)
				if #ChannelID ~= 18 or not Channel then
					return XJB.Util.CommandError("feed", "Invalid channel ID.")
				end
			end
		else
			ChannelID = Message.parent.id
			Channel = Message.parent
		end

		if (Feeds[Message.guild_id] or {})[Short] == ChannelID then
			return XJB.Util.CommandError("feed", "Feed channel is already set to **#"..Channel.name.."**.")
		end

		local Success, Message = XJB.Util.SetFeed(Short, Message.guild_id, ChannelID)
		if not Success then
			return XJB.Util.CommandError("feed", "Internal error occured while setting feed channel!"), nil, Message
		end
		XJB.News.CheckFeeds()
		return XJB.Util.CommandOutput(Prefix..Short, "Set feed channel for "..Name.." to **#"..Channel.name.."**.", nil, ChannelID)
	end,
	destructive = true
})
local FakeNews = {
	"**BREAKING**: mr drampf is BAD",
	"someone died lol",
	"xD",
	"sample text",
	"we learned to code... and heres what happenened next.",
	"bot died", -- true story
	"Please pay **$1000** per year to access our headlines (not even fake *(lmao)*)" -- (lmao)
}

XJB.Util.AddCommand("checkfeeds", {
	types = {
		"feeds",
		"utility"
	},
	help = function(_, Prefix)
		return XJB.Util.CommandHelp("checkfeeds", ([[
Check for new articles in Xi JinBot's RSS feeds.
~~This should be done automatically every 5 minutes.~~
Use `%sfeed` to list feed sources.]]):format(Prefix))
	end,
	func = function(Message, Ring, Score)
		if Score < 950 and not (Ring and Ring <= 4) then -- Ring 0-4 or >950 Social Credit required
			return XJB.Util.CommandCreditScore("checkfeeds", XJB.Util.Random(FakeNews))
		end
		XJB.News.CheckFeeds()
		return XJB.Util.CommandOutput(nil, "📰 **Checked Feeds!**")
	end,
	destructive = true
})

local Dentists = {
	{ -- Drake
		[true] = "612006723443753020",
		[false] = "612006738253709412"
	}
}

XJB.Commands.dentist = {}
XJB.Commands.dentist.types = {
	"fun"
}
XJB.Commands.dentist.help = function()
	return XJB.Util.CommandHelp("dentist [number of dentists = 10]", [[
*n*-1 out of *n* dentists recommend Xi JinBot!
But do you?
***For this command to work, N must be 2 or more.***
]])
end
XJB.Commands.dentist.func = function(Message, Ring, Score, Rupees, N)
	if Score < 850 and not (Ring and Ring <= 4) then -- Ring 0-4 or >850 Social Credit required
		return XJB.Util.CommandCreditScore("dentist", "Get that london look")
	end

	if N and not tonumber(N) then
		return XJB.Commands.dentist.help()
	end
	if N == "-" then N = 10 end
	N = math.floor(tonumber(N or 10))
	if N < 2 then
		return XJB.Commands.dentist.help()
	end
	if N > 9000000000 then
		return XJB.Util.CommandError("dentist", "There can't possible be that many dentists!")
	end

	local Selection = Dentists[math.random(1, #Dentists)]

	local ID = math.random(1, N) == 1
	local Text = "**This one does!**"
	if ID then
		Text = "*This one doesn't, and as a result, has lost 50 social credit score.*"
	end
	ID = Selection[ID]
	local Dentist = "https://cdn.discordapp.com/emojis/"..ID..".png?width=64&height=64"
	local DentistEmoji = "<:dentist:"..ID..">"

	return {
		content = "***"..(N - 1).."*/__"..N.."__ dentists recommend __Xi JinBot__!**",
		embed = {
			description = Text,
			image = {
				url = Dentist
			},
			color = XJB.Constants.Colours.Delta
		}
	}, DentistEmoji
end

XJB.Commands.mcskin = {}
XJB.Commands.mcskin.types = {
	"gamers", "utility"
}
XJB.Commands.mcskin.help = function()
	return XJB.Util.CommandHelp("mcskin <uuid or current username>", [[
Returns a Minecraft players skin.
You can use their UUID or username.
Example: mcskin notch]])
end
XJB.Commands.mcskin.func = function(Message, Ring, Score, Rupees, UUID)
	if Score < 900 and not (Ring and Ring <= 3) then -- Ring 0-3 or >900 Social Credit required
		return XJB.Util.CommandCreditScore("mcskin", "No.")
	end
	if not UUID then
		return XJB.Commands.mcskin.help()
	end
	if not MCAPI.IsUUIDValid(UUID) then
		if MCAPI.IsNameValid(UUID) then
			UUID, Error = MCAPI.GetUUID(UUID)
			if not UUID then
				return XJB.Util.CommandError("mcskin", "Invalid username."), nil, Error
			end
		else
			return XJB.Commands.mcskin.help()
		end
	end

	local Username, Error = MCAPI.GetName(UUID)
	if not Username then
		return XJB.Util.CommandError("mcskin", "Failed to get username!"), nil, Error
	end

	return {
		content = "⛏ **"..Username.."'s skin**",
		embed = {
			image = {
				url = "https://crafatar.daporkchop.net/renders/body/"..UUID
			},
			color = XJB.Constants.Colours.Green
		}
	}
end

XJB.Util.AddCommand("status", {
	requirements = {
		ring = 0
	},
	types = {
		"admin"
	},
	help = function()
		return XJB.Util.CommandHelp("status [status text]", [[
Sets my **Playing *...*** status line.
Omit status text to reset it.
*Because status is global it will __**not**__ change the game status.*
***If discord lets bot makers set a server-specific status I'll do it.]])
	end,
	func = function(Message, Ring, _, Rupees, ...)
		if not XJB.Util.CanRunCommand("status", Message.guild_id, Ring) then
			return XJB.Util.CommandError("status", "Permission: DENIED.")
		end
		local Status = table.concat({...}, " ")
		if Status == "" or Status == "-" then
			Status = nil
		end
		if Status and #Status > 32 then
			return XJB.Util.CommandError("status", "Status must be 32 characters or less.")
		end
		XJB.Data.UpdateStatus(Status)
		return XJB.Util.CommandOutput("💬 Status", "Set status to "..((Status and "**Playing "..Status.."**") or "default")..".", nil, Status)
	end
})

local UserInfo = {
--[[
	name = function(Message, User, CallerRing, CallerScore)
		return Name, Value
	end
]]
	username = function(_, User)
		return "**Username**", User.username
	end,
	discriminator = function(_, User)
		return "**Discriminator**", User.discriminator
	end,
	social_credit_score = function(_, User)
		return "**Social Credit Score**", tostring(XJB.Util.GetCredit(User.id))
	end,
	execution_level = function(Message, User)
		local Ring = XJB.Util.GetRing(User, Message.guild_id)
		if not Ring then
			Ring = "*None*"
		elseif Ring < 0 then
			Ring = "**Xi Jinping**"
		else
			Ring = "__Ring-**"..Ring.."**__"
		end
		return "**Execution Level**", Ring
	end,
	avatar = function(_, User)
		return nil, {
			url = XJB.Util.GetAuthor(User).icon_url.."?width=64&height=64" -- 64x64 version of targets pfp
		}, "image"
	end,
	created = function(_, User)
		local Timestamp = User.id >> 22
		Timestamp = Timestamp + 1420070400000 -- Discord Epoch is 1/1/2015
		Timestamp = math.floor(Timestamp / 1000)

		local Time = os.date("At %d/%m/%Y - %X UTC", Timestamp)
		return "**Created**", Time
	end,
	joined = function(_, User)
		local User, Member = XJB.Data.GetUser(User.id)
		if not (User and Member) then
			return "**Joined**", "*Not on this server*"
		end

		if Member.joined_at then
			local Year, Month, Day, Hour, Minute, Second = Member.joined_at:match("(%d+)%-(%d%d)%-(%d%d)T(%d%d):(%d%d):(%d%d)%.")
			local Time = string.format("At %s/%s/%s - %s:%s:%s UTC", Day, Month, Year, Hour, Minute, Second)
			return "**Joined**", Time
		else
			return "**Joined**", "*Unknown*"
		end
	end
}

XJB.Commands.user = {}
XJB.Commands.user.types = {
	"info", "utility"
}
XJB.Commands.user.help = function(_, Prefix)
	return XJB.Util.CommandHelp("user <id = you> [filter]", ([[
Returns lots of information on a user.
Filter uses same syntax as `%sinfo`.]]):format(Prefix))
end

XJB.Commands.user.func = function(Message, Ring, Score, Rupees, ID, ...)
	if Score < 900 and not (Ring and Ring <= 3) then -- Ring 0-3 or >900 Social Credit required
		return XJB.Util.CommandCreditScore("user", "dont kno cant find the user sorry lol :)")
	end

	if ID == nil or ID == "-" then
		ID = Message.author.id
	end
	ID = ID:match("^<@!?(.+)>$") or ID
	local User = XJB.Data.GetUser(ID)
	if not User then
		return XJB.Util.CommandError("user", "Invalid ID.")
	end

	local Include = {...}
	if #Include == 0 then
		for Name, _ in opairs(UserInfo) do
			table.insert(Include, Name)
		end
	end

	local Fields = table.new()
	local Ret = table.new{
		content = "🔨 **User info for User #"..ID.."**",
		embed = {
			color = XJB.Constants.Colours.Delta
		}
	}

	for _, Thing in ipairs(Include) do
		local Func = UserInfo[Thing:lower()]
		if Func then
			local Name, Value, Index = Func(Message, User, Ring, Score)
			if Index then -- Put in embed
				Ret.embed[Index] = Value
			elseif Name and Value then -- Put in embed.fields{}
				Fields:insert{name = Name, value = Value}
			end
		else
			return XJB.Util.CommandError("user", "Invalid filter.")
		end
	end

	if #Fields == 0 then
		return XJB.Util.CommandError("user", "Failed to get info on user.")
	elseif #Fields > 25 then
		return XJB.Util.CommandError("user", "Too much info; please specify a filter!")
	end

	local ScriptOutput = ""
	for _, Field in pairs(Fields) do
		ScriptOutput = ScriptOutput..Field.value..","
	end
	if ScriptOutput:match("(.+),$") then
		ScriptOutput = ScriptOutput:match("(.+),$")
	end

	Ret.embed.fields = Fields
	return Ret, ScriptOutput
end

XJB.Util.AddCommand("invites", {
	types = {
		"info", "server"
	},
	help = function(Message)
		local Server = XJB.Data.GetServer(Message.guild_id)
		return XJB.Util.CommandHelp("invites [id or \"server\" = you]", ([[
	Tells you how many people someone has invited\* to $SERVER.
	Replace id with "-" for your id.
	If id is "**server**" then it will list top inviters on $SERVER.
	\**Deleted invite links do not count.*]]):gsub("%$SERVER", (Message.parent.name and "__"..Message.parent.name.."__") or "a server"))
	end,
	func = function(Message, Ring, Score, Rupees, ID)
		if Score < 860 and not (Ring and Ring <= 4) then -- Ring 0-4 or >860 Social Credit required
			return XJB.Util.CommandCreditScore("invites", "invites? **ZERO!**")
		end

		if Message.parent.type ~= 0 then
			return XJB.Util.CommandError("invites", "`"..XJB.Constants.DefaultPrefix.."invites` can only be used in a server.")
		end

		local Server
		if ID == "server" then
			Server = Message.guild_id
			ID = nil
		end

		if not ID or ID == "-" then
			ID = Message.author.id
		end
		ID = ID:match("^<@!?(.+)>$") or ID

		local User = XJB.Data.GetUser(ID)
		if not User then
			return XJB.Util.CommandError("invites", "Invalid ID.")
		end

		local Success = XJB.Util.UpdateInvites(XJB.Data.GetServer(Message.guild_id))
		if not Success then
			return XJB.Util.CommandError("invites", "I need the **Manage Server** permission to view server invites!")
		end

		if Server then
			local InviteList = XJB.User.Invites[Server]
			if not InviteList then
				return XJB.Util.CommandOutput(nil, "📭 *There are no used invites for this server...*")
			end

			local RawFields = table.new()
			local FieldIndices = table.new()
			for ID, Count in pairs(InviteList) do
				local Name = "Deleted user ("..ID..")"
				local User = XJB.Data.GetUser(ID)

				if User then
					Name = User.username.."#"..User.discriminator
				end

				RawFields[Name] = {
					name = Name,
					value = tostring(Count)
				}
				FieldIndices:insert{
					index = Name,
					count = Count
				}
			end
			table.sort(FieldIndices, function(a, b)
				return a.count > b.count
			end)

			local Top = 10
			if Top > #FieldIndices then Top = #FieldIndices end

			local Fields = {}
			for i = 1, Top do
				Fields[i] = RawFields[FieldIndices[i].index]
			end

			return {
				content = "📬 **Top __"..Top.."__ inviters for "..XJB.Data.GetServer(Server).name.."**:",
				embed = {
					description = "**User**\nInvites",
					fields = Fields,
					color = XJB.Constants.Colours.DeltaNedas
				}
			}, Fields[1]
		end

		local Invited = (XJB.User.Invites[Message.guild_id] or {})[ID]
		local Server = XJB.Data.GetServer(Message.guild_id) or {name = "this server"}
		if not Invited then
			return XJB.Util.CommandOutput(nil, "📭 **__"..User.username.."__ has not invited anybody to __"..Server.name.."__.**", nil, 0)
		end

		return XJB.Util.CommandOutput(nil, "📬 **__"..User.username.."__ has invited *"..Invited.."* people to __"..Server.name.."__.**", nil, Invited)
	end,
	destructive = true
})

XJB.Util.AddCommand("changes", {
	types = {
		"info",
		"utility"
	},
	help = function(Message)
		local Prefix = XJB.Util.GetPrefix(Message.guild_id)
		return XJB.Util.CommandHelp("changes [version]", ([[
Returns all changes since a version.
If version is "**-**" then it gets this version's changes.
If you leave out a segment it will get all history since that version.
**Example**: `$PREFIXchanges 0.2`
**Returns**: *<changes since 0.2.0.0>*]]):gsub("$PREFIX", Prefix))
	end,
	func = function(Message, Ring, Score, Rupees, Major, Minor, Build, Patch)
		if Score < 950 and not (Ring and Ring <= 3) then -- Ring 0-3 or >950 Social Credit required
			return XJB.Util.CommandCreditScore("changes", "Changes since v"..XJB.Constants.Version..": you lost social credit.") -- :devilish_pepe:
		end

		if not Major or Major == "-" then
			return XJB.Util.CommandOutput("📔 Latest changes for __Xi JinBot v*"..XJB.Constants.Version.."*__:", XJB.Constants.LatestChanges)
		end

		local Check = {}
		local String = ""

		if Major then -- Use 0 2 0 0 or 0.2.0.0
			local OldVersion = Major
			local Cry = false
			Major = tonumber(OldVersion:match("^%d+"))
			if Major and XJB.Constants.Changelog[Major] then
				Check = XJB.Constants.Changelog[Major]
				String = Major
				Minor = tonumber(Minor or OldVersion:match("^%d+%.(%d+)"))
				if Minor then
					if Check[Minor] then
						Check = Check[Minor]
						String = Major.."."..Minor
						Build = tonumber(Build or OldVersion:match("^%d+%.%d+%.(%d+)"))
						if Build then
							if Check[Build] then
								Check = Check[Build]
								String = String.."."..Build
								Patch = tonumber(Patch or OldVersion:match("^%d+%.%d+%.%d+%.(%d+)$"))
								if Patch then
									if Check[Patch] then
										Check = Check[Patch]
										String = String.."."..Patch
									else
										Cry = true
									end
								end
							else
								Cry = true
							end
						end
					else
						Cry = true
					end
				end
			else
				Cry = true
			end
			if Cry then
				return XJB.Util.CommandError("changes", "Invalid version.")
			end
		end

		local Output = XJB.Misc.GetChanges(String, Check)
		local Prefix = XJB.Util.GetPrefix(Message.guild_id)
		print("Output: ", String, Output)
		return XJB.Util.CommandOutput("📔 Changes since __Xi JinBot *v"..String.."*__:", Output:match("^.-:\n(.+)"):gsub("%$PREFIX", Prefix), nil, Output)
	end
})

XJB.Util.AddCommand("commit", {
	requirements = {
		ring = 0
	},
	types = {
		"admin",
		"utility"
	},
	help = function()
		return XJB.Util.CommandHelp("commit [message]", [[
	Commits any pending changes to the master branch.
	If message is ommitted or "**-**" it will be the current timestamp.
	You can use $TIME in commit message to use the current timestamp.]])
	end,
	func = function(Message, Ring, _, Rupees, ...)
		if not XJB.Util.CanRunCommand("commit", Message.guild_id, Ring) then
			return XJB.Util.CommandError("commit", "Committing changes requires a Ring-__$RING__ execution level.")
		end

		local Text = table.concat({...}, " ")
		local now = STDLua.Timestamp()

		if Text == "" or Text == "-" then
			Text = tostring(now)
		end
		Text = Text:gsub("$TIME", now)
		STDLua.Execute("git add -A")
		STDLua.Execute("git commit -am \"%s\"", Text:format("Bash"))
		STDLua.Execute("git push origin")
		return XJB.Util.CommandOutput("🛠 Commit", "Changes probably committed, check logs.")
	end,
	destructive = true
})

XJB.Util.AddCommand("time", {
	types = {
		"info",
		"script",
		"utility"
	},
	help = function()
		return XJB.Util.CommandHelp("time <command>", [[
Runs a command and returns how long it took.
The format is **seconds**.**microseconds**.]])
	end,
	func = function(Message, Ring, Score, Rupees, ...)
		if Score < 900 and not (Ring and Ring <= 4) then -- Ring 0-4 or >900 Social Credit required
			return XJB.Util.CommandCreditScore("time", "It's ten o' clock!") -- i mean somewhere it always is :)
		end

		local Args = {...}
		if #Args == 0 then
			return XJB.Commands.time.help()
		end

		local Time = XJB.Util.TimeFunction(function()
			Message.content = XJB.Util.GetPrefix(Message.guild_id)..table.concat(Args, " ")
			XJB.Util.ParseMessage(Message, true)
		end)
		return XJB.Util.CommandOutput("⏱ Time", ("Running that command took ** %.6f ** seconds."):format(Time), nil, Time)
	end,
	aliases = {
		"timer",
		"stopwatch"
	}
})

XJB.Util.AddCommand("test", {
	requirements = {
		ring = 0
	},
	types = {
		"admin"
	},
	help = "Runs all commands (besides this) with sample input.\nRequires an execution level of __Ring-**0**__.",
	func = function(Message, Ring)
		if not XJB.Util.CanRunCommand("test", Message.guild_id, Ring) then
			return XJB.Util.CommandError("test", "Testing commands requires an execution level of __Ring-**0**__.")
		end

		local CommandsTested = 0
		local TimesTested = 0 -- Commands * 36
		local ErrorCount = 0
		Message.Testing = true
		local FoundErrors = {}

		local Aliases = XJB.Aliases
		local TestTime = XJB.Util.TimeFunction(function()
			XJB.Util.SendMessage(Message, (XJB.Util.CommandOutput("☢ Command tests", "Doing Ring + Social credit tests... this may take a while!")))
			for Name, Command in pairs(XJB.Commands) do
				if not (Aliases[Name] or Command.destructive) then
					print("Commands.test: Testing command "..Name..".")
					CommandsTested = CommandsTested + 1
					for ring = -1, 5 do
						if ring == -1 then ring = nil end
						for credit = 700, 1300, 200 do
							local Success, Error = pcall(function() Command.func(Message, ring, credit) end)
							if not Success then
								ErrorCount = ErrorCount + 1
								if not FoundErrors[Error] then -- Only use 1 error not 36 of them
									FoundErrors[Error] = true
									printf("Commands.test: \27[31mError occured while running command %s: %s.\27[0m\n", Name, Error)
								end
							end
							Lanes.sleep(0.1)
							TimesTested = TimesTested + 1
						end
						if not ring then ring = -1 end -- Loop will cry otherwise
					end
				end
			end

			XJB.Util.SendMessage(nil, (XJB.Util.CommandOutput(nil, "Doing unique tests...")), Message.parent)
			for Name, Inputs in opairs(XJB.Misc.UniqueTests) do
				if type(Inputs) ~= "table" then Inputs = {Inputs} end
				for _, Input in ipairs(Inputs) do
					local Success, Error = pcall(function()
						XJB.Commands[Name].func(Message, 0, 1300, table.unpack(Input))
					end)
					if not Success then
						ErrorCount = ErrorCount + 1
						printf("Commands.test: \27[31mError occured while running command %s: %s.\27[0m\n", Name, Error)
					end
					Lanes.sleep(0.1)
					TimesTested = TimesTested + 1
				end
			end

			local FakeID = Message.author.id << 22
			local Timestamp = os.time() * 1000 + 1420070400000
			FakeID = FakeID | Timestamp -- Replace Account Created timestamp with now
			print("Tested a fake newbie add.")
			XJB.Events.guildMemberAdd({
				user = {
					id = FakeID,
					username = "Test",
					discriminator = "User"
				},
				parent = {
					id = Message.guild_id
				}
			}, true)
			Message.Testing = false
		end)

		local Colour = XJB.Constants.Colours.Green
		if ErrorCount > 0 then
			Colour = XJB.Constants.Colours.Delta
			local Ratio = ErrorCount / TimesTested
			if Ratio > 0.10 then
				Colour = XJB.Constants.Colours.Yellow
				if Ratio > 0.25 then
					Colour = XJB.Constants.Colours.Orange
					if Ratio > 0.5 then
						Colours = XJB.Constants.Colours.Red
						if Ratio > 0.85 then
							Colours = XJB.Constants.Colours.Black
						end
					end
				end
			end
		end

		XJB.Util.SendMessage(nil, { -- Returning doesnt work idk why
			content = "**Test results for all commands.**",
			embed = {
				description = ("Tests took %.6f seconds."):format(TestTime),
				fields = {
					{
						name = "Commands tested",
						value = "*"..tostring(CommandsTested).."*"
					},
					{
						name = "Total times tested",
						value = "*"..tostring(TimesTested).."*"
					},
					{
						name = "Errors occured",
						value = "***"..tostring(ErrorCount).."***"
					}
				},
				color = Colour
			}
		}, Message.parent)
		return nil, ErrorCount
	end,
	aliases = {
		"testcommands"
	},
	destructive = true
})


XJB.Util.AddCommand("requirements", {
	requirements = {
		ring = 1
	},
	types = {
		"admin",
		"server"
	},
	help = function(Message)
		if Message.parent.type == 0 then
			local Server = XJB.Data.GetServer(Message.guild_id)
			if Server then
				Server = "**"..Server.name.."**"
			else
				Server = "this server"
			end
			return XJB.Util.CommandHelp("requirements [command] [ring]", ([[Sets a command's execution level requirement.
If no command is specified it lists all custom requirements for $SERVER.
If no ring is specified it will reset the requirement to its default.
If ring is "**null**" then anybody can use the command.]]):gsub("%$SERVER", Server))
		end
		return XJB.Util.CommandHelp("requirements", "**This command can only be ran on a server.**")
	end,
	func = function(Message, Ring, _, Rupees, Command, Required)
		if not XJB.Util.CanRunCommand("requirements", Message.guild_id, Ring) then
			return XJB.Util.CommandError("requirements", "I'm sorry, Dave. I can't let you do that")
		end

		if Message.parent.type ~= 0 then
			return XJB.Util.CommandError("requirements", "This command can only be ran on a server.")
		end

		if not Command then -- List server specific requirements
			local Requirements = XJB.User.Requirements[Message.guild_id]
			if not Requirements then
				return XJB.Util.CommandOutput("Requirements", "This server has no custom command requirements.")
			end

			local Ret = table.new{
				"**Command** - Execution level required"
			}
			for Command, Required in pairs(Requirements) do
				Required = Required.ring
				if Required == "null" then Required = "None" end
				Ret:insert("**"..Command.."** - "..Required)
			end
			return XJB.Util.CommandOutput("Requirements for "..((XJB.Data.GetServer(Message.guild_id) or {}).name or "this server"), Ret:concat("\n"))
		end

		if not XJB.Commands[Command] then
			return XJB.Util.CommandError("requirements", "Command does not exist.")
		end

		local Requirements = XJB.Util.GetRequirements(Command, Message.guild_id)
		if not Requirements then
			return XJB.Util.CommandError("requirements", "Failed to get base requirements for "..Command..".")
		end

		if not (XJB.Util.CanRunCommand(Command, Message.guild_id, Ring) or Ring == 0) then
			return XJB.Util.CommandError("requirements", "You cannot set requirements for that command.")
		end

		if Required == "-" then
			Required = "null"
		end

		if Required and not (tonumber(Required) or Required == "null") then
			return CommandError("requirements", "Invalid execution level")
		end

		local Success, Error = XJB.Util.SetRequirements(Command, Message.guild_id, Required and {ring = tonumber(Required) or Required})
		if not Success then
			return XJB.Util.CommandError("requirements", "Failed to set command requirements!"), Error
		end

		if Required then
			if Required == "null" then
				Required = "removed"
			else
				if Ring and tonumber(Required) < Ring then
					return XJB.Util.CommandError("requirements", "That would lock you out of it, your execution level is too low!")
				end
				Required = "set to __Ring-**"..Required.."**__"
			end

			return XJB.Util.CommandOutput("Requirements", "**"..Command.."'s execution level requirement was "..Required..".**")
		end
		return XJB.Util.CommandOutput("Requirements", "**"..Command.."'s requirements were reset.**")
	end,
	aliases = {
		"requirement"
	},
	destructive = true
})

local SMessage = require("litcord.structures.Message")
XJB.Util.AddCommand("delete", {
	requirements = {
		ring = 1
	},
	types = {
		"admin",
		"server"
	},
	help = function()
		return XJB.Util.CommandHelp("delete <message> [channel = current]", [[
Deletes one of my old messages.
If message is a link then channel will be ignored.
If channel is not specified or it is "-", it defaults to this channel.]])
	end,
	func = function(Message, Ring, _, Rupees, message, channel)
		if not message then return XJB.Commands.delete.help() end

		-- Look for server then PMs
		local FoundChannel, FoundMessage = message:match("https://discordapp%.com/channels/"..Message.guild_id.."/(%d-)/(%d+)") or message:match("https://discordapp%.com/channels/@me/(%d-)/(%d+)")
		if FoundChannel and FoundMessage then
			channel = FoundChannel
			message = FoundMessage
		end

		if Message.parent.type == 0 then
			if channel == "-" or not channel then
				channel = Message.parent.id
			end
			if not (XJB.Util.CanRunCommand("delete", Message.guild_id, Ring) or XJB.Util.HasPermission(Message.author.id, "MANAGE_MESSAGES", Message.guild_id, channel)) then
				Message:delete() -- delete THEIR message topkek
				return
			end

			local Requested = XJB.Data.GetMessage(channel, message)
			if not Requested then
				return XJB.Util.CommandError("delete", "Message not found.")
			end

			if Requested.author.id ~= XJB.Client.user.id then
				return XJB.Util.CommandError("delete", "That's not my message!")
			end

			Message:delete()
			if Requested.parent.guild_id ~= Message.guild_id then
				-- Silently fail and dont say if the server has xi jinbot for Muh Privacy(TM)
				return
			end

			Requested.parent = XJB.Data.GetChannel(channel)
			Requested.parent.parent = {parent = XJB.Client}
			SMessage.delete(Requested)
			return
		end
		channel = Message.parent.id

		local Requested = XJB.Data.GetMessage(channel, message)
		if not Requested then
			return XJB.Util.CommandError("delete", "Invalid message!")
		end

		if Requested.author.id ~= XJB.Client.user.id then
			return XJB.Util.CommandError("delete", "That's not my message!")
		end

		Requested.parent = XJB.Data.GetChannel(channel)
		Requested.parent.parent = {parent = XJB.Client}
		SMessage.delete(Requested)
		Message:delete() -- Spooky, deletes both messages; I could :react("🗑") but then id need $unreact.
		return nil, Requested.id
	end
})

local function Retry(Message)
	if not Message.content:match("^"..XJB.Util.GetPrefix(Message.guild_id):escape("Patterns")) then
		return XJB.Util.CommandError("retry", "That is not a command.")
	end

	local Success, Error = xpcall(function() XJB.Util.ParseMessage(Message, false, false) end, debug.traceback)
	if not Success then
		return XJB.Util.CommandError("retry", "Retried command errored!"), nil, Error
	end
	return XJB.Util.CommandOutput("♻ Retry", "Command tried again.")
end

XJB.Util.AddCommand("retry", {
	requirements = {
		ring = 0
	},
	types = {
		"admin"
	},
	help = function()
		return XJB.Util.CommandHelp("retry <message> [channel = current]", [[
Retries a command.
If message is a link then channel will be ignored.
If channel is not specified or it is "**-**", it defaults to this channel.]])
	end,
	func = function(Message, Ring, _, Rupees, message, channel)
		if not message then return XJB.Commands.retry.help() end

		-- Look for server then PMs
		local FoundChannel, FoundMessage = message:match("https://discordapp%.com/channels/"..Message.guild_id.."/(%d-)/(%d+)") or message:match("https://discordapp%.com/channels/@me/(%d-)/(%d+)")
		if FoundChannel and FoundMessage then
			channel = FoundChannel
			message = FoundMessage
		end

		if Message.parent.type == 0 then
			if channel == "-" or not channel then
				channel = Message.parent.id
			end
			channel = channelID:match("^<#(.+)>$") or channel

			local Requested = XJB.Data.GetMessage(channel, message)
			if not Requested then
				return XJB.Util.CommandError("retry", "Message not found.")
			end

			if Requested.id == Message.id then
				return "what the actual fuck go win the lottery"
			end

			return Retry(Requested)
		end
		channel = Message.parent.id

		local Requested = XJB.Data.GetMessage(channel, message)
		if not Requested then
			return XJB.Util.CommandError("retry", "Message not found.")
		end

		if Requested.id == Message.id then
			return "what thhe fuck?!"
		end

		return Retry(Requested)
	end
})

XJB.Util.AddCommand("newbies", {
	requirements = {
		ring = 2
	},
	types = {
		"admin",
		"server"
	},
	help = function()
		return XJB.Util.CommandHelp("newbies/warnings [time] [channel]", [[
Sets a warning channel for a time.
If a user joins and they have not been on discord for more than time a warning will be created.
Time is a time string and is made up of numbers proceeded by:
- c or C, Century
- D, Decade,
- y or Y, Year
- M, Month **(Average)**
- w or W, Week
- d or *nothing*, Day
- h or H, Hour
- m, Minute,
- s or S, Second
If time is "**-**" it will use **7 days**.
If time is not present this server's warning channels and times will be listed.
If channel is "**-**" or not present it will use the current channel.
If channel is "**null**" then the channel will no longer receive warnings.
**Example**: newbies 7 #mod-log
**Returns *(on newbie join)***: **<User>**#**<Tag>** *(**ID**)* has joined while being on discord for less than **7** days!]])
	end,
	func = function(Message, Ring, _, Rupees, ...)
		if not XJB.Util.CanRunCommand("newbies", Message.guild_id, Ring) then
			return XJB.Util.CommandError("newbies", "You are the noob here. 😎") -- haha sick burn, anon
		end

		local Input = {...}

		if #Input == 0 then
			local Newbies = XJB.User.Newbies[Message.guild_id]
			if not Newbies then
				return XJB.Util.CommandOutput("😎 Newbies", "There are no warning channels set.")
			end

			local Fields = table.new{"**Channel** - Days"}
			for Newbie, Channel in opairs(Newbies) do
				local Output = "*(#"..Channel..")*"
				local Downloaded = XJB.Data.GetChannel(Channel)
				if Downloaded then Output = "#"..Downloaded.name end

				Fields:insert("**"..Output.."** - "..math.floor(tonumber(Newbie) / XJB.Constants.Seconds.Day))
			end

			return {
				content = "**😎 Newbies**",
				embed = {
					color = XJB.Constants.Colours.Yellow,
					description = table.concat(Fields, "\n")
				}
			}
		end

		local Channel = table.remove(Input, #Input) -- Simulate "_, ..., Channel"
		local Time = table.concat(Input, "")
		if Time == "" then
			Time = Channel
			Channel = nil
		end

		if Time == "-" then Time = "7d" end
		Time = XJB.Util.ParseTime(Time)

		if Time < XJB.Constants.Seconds.Day then
			return XJB.Util.CommandError("newbies", "Time must be at least 1 day.")
		end

		if Time > XJB.Constants.Seconds.Decade then
			return XJB.Util.CommandError("newbies", "Yea no.")
		end

		if Channel == "null" then
			Channel = nil
		else
			if Channel == "-" or Channel == nil then
				Channel = Message.parent.id
			end

			Channel = Channel:match("^<#(.+)>$") or Channel
			local Exists = XJB.Data.GetChannel(Channel)
			if not Exists then
				return XJB.Util.CommandError("newbies", "Channel does not exist.")
			end

			if Exists.guild_id ~= Message.guild_id then
				return XJB.Util.CommandError("newbies", "That channel is not in this server!")
			end
		end

		if not XJB.Util.SetNewbie(Message.guild_id, Channel, Time) then
			return XJB.Util.CommandError("newbies", "Failed to set newbie warning!")
		end

		local Days = math.floor(Time / XJB.Constants.Seconds.Day)
		if Channel then
			return XJB.Util.CommandOutput("😎 Newbies", "Newbie warning channel for under **"..Days.."** "..XJB.Misc.s("day", Days).." set to **<#"..Channel..">**.", XJB.Constants.Colours.Yellow, Days)
		end
		return XJB.Util.CommandOutput("😎 Newbies", "Newbie warning channel for under **"..Days.."** "..XJB.Misc.s("day", Days).." removed.", XJB.Constants.Colours.Yellow, Days)
	end,
	aliases = {
		"warnings"
	},
	dangerous = true
})

local RedditUserInfo = {
--[[
	name = function(Old, New)
		return Name, Value
	end
]]
	username = function(Data)
		return "**Username**", Data:match('<div class="titlebox"><h1>(.-)</h1><span class="karma">')
	end,

	karma = function(Data)
		local Posts = Data:match('<span class="karma">(.-)</span>')
		local Comments = Data:match('<span class="karma comment%-karma">(.-)</span>')

		return "**Karma**", "Posts - **"..Posts.."**\nComments - **"..Comments.."**\n__Total - **"..XJB.Util.ToString(XJB.Util.ToNumber(Posts) + XJB.Util.ToNumber(Comments)).."**__"
	end,

	cake_day = function(_, Data)
		local Day = Data:match('<span id="profile.-%-cakeday" class=".-">(.-)</span>')
		Day = Day:gsub("^(.-) (.-), (.-)", "%2 %1 %3")

		return "**Cake Day**", Day
	end,

	premium = function(_, Data)
		local Status = Data:match('<svg.-<title>Reddit Premium</title>')
		if Status then
			return "**Premium**", "This user has reddit premium."
		end
	end,

	avatar = function(_, Data)
		local Avatar = Data:match('https://styles%.redditmedia%.com/.-/styles/profileIcon_.-%.png')
		if Avatar then
			return nil, {
				url = Avatar,
				width = 64,
				height = 64
			}, "image"
		end
	end
}

-- TODO: Subreddit CSS for Members and Online
local SubredditInfo = {
	subreddit = function(Data)
		return "**Subreddit**", Data:match('<a href="https://old%.reddit%.com/r/(%w_-)/" class="hover" >.-</a>')
	end,

	members = function(Data)
		local Members = Data:match('<span class="subscribers"><span class="number">(.-)</span>')
		local Online = Data:match('<p class="users%-online" title=".-"><span class="number">(.-)<')
		return "**Members**", "Online - **"..Online.."**\nTotal - **"..Members.."**"
	end,

	restricted = function(Data)
		local Restricted = Data:match('<a target="_top" >Submissions restricted</a>')
		if Restricted then
			return "**Restricted**", "Only approved users may post in this community."
		end
	end,

	icon = function(_, Data)
		local Icon = Data:match('<img.-alt="Subreddit icon".-src="(.-)"')
		if Icon then
			return nil, {
				url = Icon,
				width = 64,
				height = 64
			}, "image"
		end
	end
}

XJB.Util.AddCommand("reddit", {
	types = {
		"script",
		"utility"
	},
	help = function(_, Prefix)
		return XJB.Util.CommandHelp("reddit <username/subreddit = u/you> [filter]", "Similar to `"..Prefix..[[info` but for reddit users and subreddits.
Use "**u/username**" for users and "**r/subreddit**" for subreddits.
If name is absent or it is "**-**" then it will use **u/your username**.]])
	end,
	func = function(Message, Ring, Score, Rupees, Name, ...)
		if Score < 900 and not (Ring and Ring <= 3) then -- Ring 0-3 or >900 Social Credit required
			return XJB.Util.CommandCreditScore("reddit", "hehe REDDDDddIT lol")
		end

		if Name == nil or Name == "-" then
			Name = "u/"..Message.author.username
		end

		local Access = RedditUserInfo
		if Name:match("^[Uu]/.+") or Name:match("^[Uu]ser/.+") then
			Name = "u/"..Name:match("^.-/(.+)$")
			if #Name > 22 then
				return XJB.Util.CommandError("reddit", "Usernames are limited to 20 characters.")
			end
		elseif Name:match("^[Rr]/.+") then
			Name = "r/"..Name:match("^.-/(.+)$")
			if #Name > 23 then
				return XJB.Util.CommandError("reddit", "Subreddit names are limited to 21 characters.")
			end

			Access = SubredditInfo
		else
			return XJB.Util.CommandError("reddit", "Specify a user with \"**u/username**\" or a subreddit with \"**r/subreddit**\".")
		end

		if not Name:match("^[ur]/[%w-_]+$") then
			return XJB.Util.CommandError("reddit", "You can only use numbers, letters, **-** or **_**.")
		end

		if Name == "u/me" then -- Only works for signed in users
			return XJB.Util.CommandOutput("REDDIT", "**Karma** - infinite")
		end

		local Include = {...}
		if #Include == 0 then
			for Name, _ in opairs(Access) do
				table.insert(Include, Name)
			end
		end

		local Fields = table.new()
		local Ret = table.new{
			content = "📱 Reddit info for **"..Name.."**",
			embed = {
				color = XJB.Constants.Colours.Orange
			}
		}

		local Old = STDLua.Download("https://old.reddit.com/"..Name)
		if Old == nil or Old:match("<p>.-the page you requested does not exist.-</p>") then
			return {
				embed = {
					title = "page not found", -- Funny reddit man XDDDDd
					description = "the page you requested does not exist",
					image = {
						url = string.format("https://www.redditstatic.com/reddit404%x.png", math.random(10, 14)),
						height = 96
					},
					color = XJB.Constants.Colours.Orange
				}
			}
		end

		if Old:match("<h3>This community has been banned</h3>") then
			return {
				embed = {
					title = "This community has been banned", -- Funny reddit hammer XDDDDd
					description = "Banned "..Old:match('<div class="note">Banned&#32;<time .->(.-)</time>%.</div>')..".",
					image = {
						url = "https://www.redditstatic.com/interstitial-image-banned.png",
						width = 64,
						height = 64
					},
					color = XJB.Constants.Colours.Orange
				}
			}
		end

		local New = STDLua.Download("https://www.reddit.com/"..Name)

		for _, Thing in ipairs(Include) do
			local Func = Access[Thing:lower()]
			if Func then
				local Name, Value, Index = Func(Old, New)
				if Index then -- Put in embed
					Ret.embed[Index] = Value
				elseif Name and Value then -- Put in embed.fields{}
					Fields:insert{name = Name, value = Value}
				end
			else
				return XJB.Util.CommandError("reddit", "Invalid filter.")
			end
		end

		if #Fields == 0 then
			return XJB.Util.CommandError("reddit", "Failed to get info for "..Name..".")
		elseif #Fields > 25 then
			return XJB.Util.CommandError("reddit", "Too much info; please specify a filter!")
		end

		local ScriptOutput = ""
		for _, Field in pairs(Fields) do
			ScriptOutput = ScriptOutput..Field.value..","
		end
		if ScriptOutput:match("(.+),$") then
			ScriptOutput = ScriptOutput:match("(.+),$")
		end

		Ret.embed.fields = Fields
		return Ret, ScriptOutput
	end
})

XJB.Util.AddCommand("freeze", {
	requirements = {
		ring = 0
	},
	types = {
		"admin"
	},
	help = "Freezes time until a key is pressed in the XJB console.",
	func = function(Message, Ring)
		if not XJB.Util.CanRunCommand("freeze", Message.guild_id, Ring) then
			return XJB.Util.CommandError("freeze", "They don't call me *Mr. Freeze.*")
		end
		XJB.Util.SendMessage(Message, (XJB.Util.CommandOutput("🆓🇿🇪 Frozen", "`press any key to continue_`")))
		io.read("*l")
		return XJB.Util.CommandOutput("🌡 Thawed", "I have awoken from my cold slumber.")
	end
})

XJB.Util.AddCommand("purge", {
	requirements = {
		ring = 2
	},
	types = {
		"admin",
		"utility"
	},
	help = function()
		return CommandHelp("purge <start message> [end message = this] [delete pinned = false] [channel = this]", [[
Deletes all messages from **start** to **end**, inclusive.
By default it will not delete any pinned messages.]])
	end,
	func = function(Message, Ring, _, Rupees, Start, End, Force, Channel)
		return "WIP"
	end
})

local RupeeSubCommands = {
	balance = function(Message, _, _, Rupees, NeedfulSir)
		NeedfulSir = NeedfulSir or Message.author.id
		NeedfulSir = NeedfulSir:match("^<@!?(.+)>$") or NeedfulSir
		if NeedfulSir == Message.author.id then
			return XJB.Util.CommandOutput("🚽 Rupees - balance", "mr sir "..Message.author.username.." your rupees is **"..XJB.Util.ToString(Rupees).."**.", XJB.Constants.Colours.Brown, Rupees)
		end

		NeedfulSir = XJB.Data.GetUser(NeedfulSir)
		if not NeedfulSir then
			return XJB.Util.CommandError("rupees - balance", "the sir is not vry needful....")
		end

		Rupees = XJB.Util.GetRupees(NeedfulSir)
		if NeedfulSir == XJB.Client.user.id then
			return XJB.Util.CommandOutput("🚽 Rupees - balance", "mr jinbot rupee are "..XJB.Util.ToString(Rupees)..".", XJB.Constants.Colours.Brown, Rupees)
		end
		return XJB.Util.CommandOutput("🚽 Rupees - balance", "mr kindly sir "..NeedfulSir.username.." the rupees is **"..XJB.Util.ToString(Rupees).."**.", XJB.Constants.Colours.Brown, Rupees)
	end,
	pay = function(Message, _, _, Rupees, NeedfulSir, Rupee)
		Rupee = tonumber(Rupee)
		if not Rupee then
			return XJB.Commands.rupees.help()
		end

		Rupee = math.round(Rupee, 2)
		if Rupee <= 0 then
			return XJB.Util.CommandError("rupees - pay", "CORE JAVA is smarter for you...")
		end


		if Rupee > Rupees then
			return XJB.Util.CommandError("rupees - pay", "pls sir come to durgasoft online train for job in CORE JAVA")
		end

		NeedfulSir = NeedfulSir:match("^<@!?(.+)>$") or NeedfulSir
		if NeedfulSir == Message.author.id then
			return XJB.Util.CommandError("rupees - pay", "mr sir you are not very needful.")
		end

		NeedfulSir = XJB.Data.GetUser(NeedfulSir)
		if not NeedfulSir then
			return XJB.Util.CommandError("rupees - pay", "the sir is not vry needful....")
		end

		if not (XJB.Util.SetRupees(NeedfulSir.id, XJB.Util.GetRupees(NeedfulSir.id) + Rupee) and
			XJB.Util.SetRupees(Message.author.id, Rupees - Rupee)) then
			return XJB.Util.CommandError("rupees - pay", "sorry mr patient sir but mr nagoor babu sir must fix problem tnk u for patients")
		end

		printf("Commands.rupees.pay: %s (%s) paid %s rupees to %s (%s).\n",
			Message.author.username, Message.author.id,
			XJB.Util.ToString(Rupee),
			NeedfulSir.username, NeedfulSir.id)

		if NeedfulSir.id == XJB.Client.user.id then
			return XJB.Util.CommandOutput("🚽 Rupees - pay", "mmm thank yu kind bobs")
		end
		return XJB.Util.CommandOutput("🚽 Rupees - pay", "deposit **"..XJB.Util.ToString(Rupee).."** rupee into "..NeedfulSir.username.." account. thank DURGA Software Solution mr sir", XJB.Constants.Colours.Brown)
	end,
	buy = function(Message, _, Score, _, Amount)
		Amount = tonumber(Amount)
		if not Amount then
			return XJB.Commands.rupees.help()
		end

		if Amount <= 0 then
			return XJB.Util.CommandError("rupees - buy", "CORE JAVA is smarter for you...")
		end

		Amount = math.floor(Amount)
		if Amount > Score - 1000 then
			return XJB.Util.CommandError("rupees - buy", "pls sir come to durgasoft online train for job SHILL CCP")
		end

		XJB.Util.SetCredit(Message.author.id, Score - Amount)
		if not XJB.Util.SetRupees(Message.author.id, XJB.Util.GetRupees(Message.author.id) + Amount) then
			return XJB.Util.CommandError("rupees - buy", "sorry mr patient sir but mr nagoor babu sir must fix problem tank u for patients")
		end

		return XJB.Util.CommandOutput("🚽 Rupees - buy", "buy **"..Amount.."** rupee into account. thank DURGA Software Solution mr sir", XJB.Constants.Colours.Brown)
	end,
	shop = function(Message, _, _, Rupees, ...)
		local Input = {...}
		local Amount, Needful
		if #Input > 0 then
			if #Input == 1 then
				Amount = 1
				Needful = table.concat(Input, " ")
			else
				Amount = table.remove(Input, #Input) -- Simulate "Rupees, ..., Amount"
				Needful = table.concat(Input, " ")
				if not XJB.Util.ToNumber(Amount) then
					Needful = Needful.." "..Amount
					Amount = 1
				end
			end
		end

		Amount = math.round(Amount or 1)
		if Amount < 1 then
			return XJB.Util.CommandError("rupees - buy", "CORE JAVA is smarter for you...")
		end

		local Shop = XJB.User.Inventories.Shop
		Shop.__orderedIndex = nil

		if Needful and Needful ~= "" then
			for Item, Price in opairs(Shop) do
				if Item:lower() == Needful:lower() then
					if Rupees < Price * Amount then
						return XJB.Util.CommandNeedful("rupees - shop", Price * Amount)
					end

					if not (XJB.Util.BuyItem(Message.author, Item, Amount) and XJB.Util.SetRupees(Message.author.id, Rupees - Price * Amount)) then
						return XJB.Util.CommandError("rupees - shop", "sorry mr patient sir but mr nagoor babu sir must fix problem tank u for patients")
					end

					if Item == "iFone 6s" then
						if not XJB.User.Inventories.iGotItFirst then
							XJB.User.Inventories.iGotItFirst = true
							if XJB.Util.WriteData("inventories") then
								print("Commands.rupees.shop: #iGotItFirst - %s (%s)\n", Message.author.username, Message.author.id)
								XJB.Util.SetAchievement(Message.author, "#iGotItFirst!\n- iPhone 6 India")
							end
						end
					end

					return XJB.Util.CommandOutput("🚽 Rupees - shop", "purchase **"..XJB.Util.ToString(Amount).."**x **"..Item.."** for ***"..XJB.Util.ToString(Price * Amount).."*** rupee.", XJB.Constants.Colours.Brown)
				end
			end
			return XJB.Util.CommandError("rupees - shop", "sorry kindly sir item is not sell in DURGA")
		end
		local List = table.new{
			"*needful rupee* - __**needful item**__\n"
		}

		for Item, Price in opairs(Shop) do
			List:insert("*"..XJB.Util.ToString(Price).." rupees* - `"..Item.."`")
		end

		return {
			content = "🚽 Rupees - shop list",
			embed = {
				description = table.concat(List, "\n"),
				color = XJB.Constants.Colours.Brown
			}
		}
	end,
	list = function(Message, Ring, Score, _, NeedfulSir)
		NeedfulSir = NeedfulSir or Message.author.id
		NeedfulSir = NeedfulSir:match("^<@!?(.+)>$") or NeedfulSir

		NeedfulSir = XJB.Data.GetUser(NeedfulSir)
		if not NeedfulSir then
			return XJB.Util.CommandError("rupees - list", "the sir is not vry needful....")
		end

		local List = table.new{
			"*item kindly sir amount* - __**item kindly sir have**__"
		}
		local Inventory = XJB.Util.GetInventory(NeedfulSir)
		if Inventory then
			for Item, Count in opairs(Inventory) do
				if Item ~= "__orderedIndex" then
					List:insert("*"..XJB.Util.ToString(Count).."x* `"..Item.."`")
				end
			end
		else
			return XJB.Util.CommandOutput("🚽 Rupees - list", "needful sir has few item...", XJB.Constants.Colours.Brown)
		end

		return {
			content = "🚽 Rupees - item list of mr **"..NeedfulSir.username.."**",
			embed = {
				description = table.concat(List, "\n"),
				color = XJB.Constants.Colours.Brown
			}
		}
	end,
	set = function(Message, Ring, _, _, NeedfulSir, Rupee)
		if not XJB.Util.CanRunCommand("rupees", Message.guild_id, Ring) then
			return XJB.Util.CommandError("rupees - set", "YOU ARE NOT THE NEEDFUL")
		end

		Rupee = tonumber(Rupee)
		if not Rupee then
			return XJB.Commands.rupees.help()
		end

		Rupee = math.round(Rupee, 2)

		if NeedfulSir == "-" then NeedfulSir = Message.author.id end
		NeedfulSir = NeedfulSir:match("^<@!?(.+)>$") or NeedfulSir

		NeedfulSir = XJB.Data.GetUser(NeedfulSir)
		if not NeedfulSir then
			return XJB.Util.CommandError("rupees - set", "the sir is not vry needful....")
		end

		if not XJB.Util.SetRupees(NeedfulSir.id, Rupee) then
			return XJB.Util.CommandError("rupees - set", "sorry mr patient sir but mr nagoor babu sir must fix problem tank u for patients")
		end

		if NeedfulSir.id == XJB.Client.user.id then
			return XJB.Util.CommandOutput("🚽 Rupees - set", "mmm thank yu kindly mr bobs", XJB.Constants.Colours.Brown)
		end
		return XJB.Util.CommandOutput("🚽 Rupees - set", "set **"..XJB.Util.ToString(Rupee).."** rupee for "..NeedfulSir.username.." account. thank DURGA Software Solution mr kindly sir...", XJB.Constants.Colours.Brown)
	end,
	send = function(Message, Ring, _, Rupees, NeedfulSir, ...)
		if not NeedfulSir then
			return XJB.Commands.rupees.help()
		end

		local Input = {...}
		local Amount, Needful
		if #Input > 0 then
			if #Input == 1 then
				Amount = 1
				Needful = table.concat(Input, " ")
			else
				Amount = table.remove(Input, #Input) -- Simulate "NeedfulSir, ..., Amount"
				Needful = table.concat(Input, " ")
				if not XJB.Util.ToNumber(Amount) then
					Needful = Needful.." "..Amount
					Amount = 1
				end
			end
		end

		Amount = math.round(Amount or 1)
		if Amount < 1 then
			return XJB.Util.CommandError("rupees - send", "CORE JAVA is smarter for you...")
		end

		NeedfulSir = NeedfulSir:match("^<@!?(.+)>$") or NeedfulSir

		NeedfulSir = XJB.Data.GetUser(NeedfulSir)
		if not NeedfulSir then
			return XJB.Util.CommandError("rupees - send", "the sir is not vry needful....")
		end
		if NeedfulSir.id == Message.author.id then
			return XJB.Util.CommandError("rupees - send", "u r not vry needful mr sir r.!")
		end

		local YourInventory = XJB.Util.GetInventory(Message.author)
		if not YourInventory then
			return XJB.Util.CommandError("rupees - send", "mr sir you have few item...")
		end

		local FoundNeedful = 0
		for Item, Price in opairs(YourInventory) do
			if Item:lower() == Needful:lower() then
				FoundNeedful = Price
				Needful = Item
				break
			end
		end

		if FoundNeedful < Amount then
			return XJB.Util.CommandError("rupees - send", "mr sir you do not have needful items!")
		end

		if not (XJB.Util.BuyItem(Message.author, Needful, -Amount) and XJB.Util.BuyItem(NeedfulSir, Needful, Amount)) then
			return XJB.Util.CommandError("rupees - send", "sorry mr patient sir but mr Druga must fix problem tank u for patients")
		end

		if NeedfulSir.id == XJB.Client.user.id then
			return XJB.Util.CommandOutput("🚽 Rupees - send", "mmm thank yu very kindly mr bobs", XJB.Constants.Colours.Brown)
		end
		return XJB.Util.CommandOutput("🚽 Rupees - send", "send **"..XJB.Util.ToString(Amount).."**x **"..Needful.."** for "..NeedfulSir.username.." items. thank mr nagoor babu sir, mr kindly sir...", XJB.Constants.Colours.Brown)
	end
}

XJB.Util.AddCommand("rupees", {
	types = {
		"fun",
		"rupees"
	},
	help = function()
		return XJB.Util.CommandHelp("rupees [subcommand] [subcommand option]", [[
Home of INDIA.
needful subcommands:
- `balance`: *[mr sir = u]*
   return the needful sir rupees amount
- `pay` **<mr sir> <rupee>**
   give som mony to kindly sir
- `buy` **<amount>**
   sell unuse social credit for CORE RUPEES
- `shop` *[item]* *[amount = 1]*
   list ifones for sales or buy the needful products
- `list` *[mr sir = u]*
   list kindly sirs ifpones
- `set` **<needful sir>** *[rupee]*
   kindly deposit the rupee
- `send` **<needful sir>** **<not needful item>** *[amount = 1]*
   give kindly sir no longer needful items tank u sir..
***For technical support of __CERTIFIED ORACLE REAL TIME PROFESSIONAL__ please do of the needful at __durgasoftonlinetraining@gmail.com__ M. Tech!***
tank u kind sirs]])
	end,
	func = function(Message, Ring, Score, Rupees, SubCommandName, ...)
		if Score < 700 and not (Ring and Ring < 3) then
			return XJB.Util.CommandNeedful("rupees") -- Link to durgasoft
		end

		local SubCommand = RupeeSubCommands[SubCommandName]
		if not (SubCommand) then
			return XJB.Commands.rupees.help()
		end

		return SubCommand(Message, Ring, Score, Rupees, ...)
	end,
	aliases = {
		"dotheneedful",
		"durga",
		"durgasir",
		"durgasoft",
		"india",
		"loo",
		"poo",
		"needful",
		"rajesh",
		"rakesh",
		"toilet",
		"vbucks"
	}
})

XJB.Util.AddCommand("achievements", {
	requirements = {
		ring = 0
	},
	types = {
		"utility"
	},
	help = function()
		return XJB.Util.CommandHelp("achievements <user = you> [add]", [[
List achievements of user.
If **add** is present it will give a user an achievement.
Listing does not require an execution level.]])
	end,
	func = function(Message, Ring, Score, _, User, ...)
		if Score < 900 and not (Ring and Ring < 3) then
			return XJB.Util.CommandCreditScore("achievements", "no achievement found :(")
		end

		if not User then User = Message.author.id end
		User = User:match("^<@!?(.+)>$") or User
		if not User then
			return XJB.Util.CommandError("achievements", "Invalid user.")
		end

		User = XJB.Data.GetUser(User)
		if not User then
			return XJB.Util.CommandError("achievements", "User not found.")
		end

		local Add = table.concat({...}, " ")
		if Add ~= "" then
			if not XJB.Util.CanRunCommand("achievements", Message.guild_id, Ring) then
				return XJB.Util.CommandError("achievements", "how about no")
			end

			if not XJB.Util.SetAchievement(User, Add, User.id == Message.author.id) then
				return XJB.Util.CommandError("achievements", "Failed to write achievements data! sorry!!!")
			end

			return XJB.Util.CommandOutput("🎀 Achievements", "**"..User.username.."** achieved ***"..Add.."***!", XJB.Constants.Colours.Yellow, Add)
		end
		local Achievements = XJB.Util.GetAchievements(User)
		if not Achievements then
			return XJB.Util.CommandOutput("🎀 Achievements", "**"..User.username.."** has achieved nothing in my eyes.", XJB.Constants.Colours.Yellow, 0)
		end

		local Fields = table.new()
		local Description = false

		for Achievement, Timestamp in opairs(Achievements) do
			if #Fields == 25 then
				Description = true
				break
			end
			Fields:insert{
				name = Achievement,
				value = os.date("*Got at %d/%m/%y - %X UTC*", Timestamp)
			}
		end

		return {
			content = "🎀 Achievements for **"..User.username.."**:",
			embed = {
				fields = Fields,
				description = (Description and "***Only showing 25 achievements.***") or nil,
				color = XJB.Constants.Colours.Yellow
			}
		}
	end
})

XJB.Util.AddCommand("roulette", {
	types = {
		"fun",
		"rupees"
	},
	help = function()
		return XJB.Util.CommandHelp("roulette <rupees> <bet>", [[
Bet rupees on a game of roulette.
Rupees must be between **10** and **10,000**.
Bet can be:
- 1 to 34,
- 0 or 00,
- **r**ed or **b**lack,
- **e**ven or **o**dd,
- **l**ow or **h**igh,
- **d**ozen**1-3**,
- **s**ixth**1-6**, *(half of a **d**ozen)*
- **c**olumn**1-3**
Your winnings are calculated by **your bet** \* (**no. of numbers betted on** / **38**)
Example: `roulette 100 d2` (bet **100** rupees on the **second** **d**ozen, win 300\* if it lands there.)
*\* please note there is a 1% processing fee per bet.*
__Your national ID is used to verify that you are over 18 years old.__]])
	end,
	func = function(Message, Ring, Score, Rupees, Amount, Bet)
		if Score < 900 and not (Ring and Ring < 3) then
			return XJB.Util.CommandCreditScore("roulette", "Sorry kid, 18+ command.")
		end

		Amount = XJB.Util.ToNumber(Amount)
		if not (Bet and Amount) then
			return XJB.Commands.roulette.help()
		end

		local Colour = "Green"
		local Result = math.random(0, 37)
		local Won = false

		if Rupees < Amount or Amount < 10 then
			return XJB.Util.CommandNeedful("roulette", Amount)
		end

		if Result == 0 then
			Result = "0"
		elseif Result == 37 then
			Result = "00"
		elseif XJB.Misc.Roulette.Red[Result % 18] then
			Colour = "Red"
		else
			Colour = "Black"
		end

		local BettedCount = 18
		if XJB.Util.ToNumber(Bet) then -- Single number bet
			Won = Bet == Result
			BettedCount = 1
		elseif Bet:compare("^r$", "^red$") then
			Won = Colour == "Red"
		elseif Bet:compare("^b$", "^black$") then
			Won = Colour == "Black"
		elseif Bet:compare("^e$", "^even$") then
			Won = (tonumber(Result) ~= 0) and ((Result & 1) == 0)
		elseif Bet:compare("^o$", "^odd$") then
			Won = (tonumber(Result) ~= 0) and ((Result & 1) == 1)
		elseif Bet:compare("^l$", "^low$") then
			Won = (tonumber(Result) > 0) and (tonumber(Result) < 17)
		elseif Bet:compare("^h$", "^high$") then
			Won = tonumber(Result) > 18
		elseif Bet:compare("^d%d$", "^dozen%d$") then
			local Number = tonumber(Bet:match("^d(%d)$") or Bet:match("^dozen(%d)$"))
			local ResultN = tonumber(Result)
			if Number <= 3 and Number >= 1 then
				Won = ((Number - 1) * 12) <= ResultN and ResultN <= (Number * 12)
			else
				return XJB.Util.CommandError("roulette", "There are only **3** dozens.")
			end
			BettedCount = 12
		elseif Bet:compare("^s%d$", "^sixth%d$") then
			local Number = tonumber(Bet:match("^s(%d)$") or Bet:match("^sixth(%d)$"))
			local ResultN = tonumber(Result)
			if Number <= 6 and Number >= 1 then
				Won = ((Number - 1) * 6) <= ResultN and ResultN <= (Number * 6)
			else
				return XJB.Util.CommandError("roulette", "There are only **6** sixths.")
			end
			BettedCount = 6
		elseif Bet:compare("^c%d$", "^column%d$") then
			local Number = tonumber(Bet:match("^c(%d)$") or Bet:match("^column(%d)$"))
			if Number <= 3 and Number >= 1 then
				local ResultN = tonumber(Result)
				local Column = ResultN % 3
				if Column == 0 then Column = 3 end
				Won = Column == Number
			else
				return XJB.Util.CommandError("roulette", "There are only **3** columns.")
			end
			BettedCount = 12
		else
			return XJB.Util.CommandError("roulette", "Invalid bet placement.")
		end

		local Winnings = math.round(Amount * (1 + BettedCount / 38) * 0.99, 2)

		Bet = XJB.Util.ToNumber(Bet)
		if Won then
			if not XJB.Util.SetRupees(Message.author.id, Rupees + (Winnings - Amount)) then
				return XJB.Util.CommandError("roulette", "Sorry, your rupees were not saved!!!")
			end

			return XJB.Util.CommandOutput("🎲 Roulette", "||`You won `**`"..XJB.Util.ToString(Winnings).."`**` rupees!`\nIt landed on a **"..Colour.."** ***"..Result.."***!||", XJB.Constants.Colours[Colour], Result)
		end
			if not XJB.Util.SetRupees(Message.author.id, Rupees - Amount) then
				return XJB.Util.CommandError("roulette", "Lucky you, your losses were not saved!!!")
			end

			-- Make it so you can't tell if you won or lost without clicking the spoiler.
			return XJB.Util.CommandOutput("🎲 Roulette", "||`You lost"..string.rep(".", #XJB.Util.ToString(Winnings) + 8).."`\nIt landed on a **"..Colour.."** ***"..Result.."***.||", XJB.Constants.Colours[Colour], Result)
	end,
	aliases = {
		"poolette",
		"poulette"
	}
})

XJB.Util.AddCommand("8ball", {
	types = {
		"fun"
	},
	help = function()
		return XJB.Util.CommandHelp("8ball [type]", [[
Returns a random 8ball message.
Type is **8ball** by default, you can use **list** to list all available types.
If type is invalid then it defaults to 8ball, you can use it for a message.]])
	end,
	func = function(Message, Ring, Score, Rupees, Type)
		if Score < 900 and not (Ring and Ring < 3) then
			return XJB.Util.CommandCreditScore("8ball", "no")
		end
		Type = (Type or "8ball"):lower()
		local Set = XJB.Misc.BallSets[Type]
		if not Set then
			Type = "8ball"
			Set = XJB.Misc.BallSets[Type]
		end
		local Output = XJB.Util.Random(Set)
		return XJB.Util.CommandOutput("🎱 Magic 8-Ball", "*"..Output.."*", XJB.Constants.Colours.Black, Output)
	end,
	aliases = {
		"fortune"
	}
})

XJB.Util.AddCommand("alias", {
	types = {
		"utility"
	},
	help = function(Prefix)
		return XJB.Util.CommandHelp("alias [alias] [command]", ([[
Adds a per-user alias for a command.
Alias name cannot be an existing command, alias or user alias.
Command does not include the prefix.
Run with no arguments to list your user aliases.
Replace $PREFIX( with $PREFIX\( to use subcommands at runtime.
Example: `alias hello say Hello, my old chum.
`$hello`
Output: *Hello, my old chum.*]]):gsub("%$PREFIX", Prefix))
	end,
	func = function(Message, Ring, Score, Rupees, Alias, ...)
		if Score < 950 and not (Ring and Ring < 4) then
			return XJB.Util.CommandCreditScore("alias", "computer say no no :(")
		end

		local Aliases = XJB.User.Aliases[Message.author.id]
		if Alias == "-" or not Alias then
			if not Aliases then
				return XJB.Util.CommandOutput("🕵 Alias", "You have no aliases set.")
			end

			local Output = ""
			local Count = 0
			for Alias, Command in pairs(Aliases) do
				Count = Count + 1
				Output = Output.."**"..Alias.."** - `"..Command.."`\n"
			end

			return XJB.Util.CommandOutput("🕵 Aliases - __"..Count.."__", Output)
		end

		if XJB.Commands[Alias] or XJB.Aliases[Alias] then
			return XJB.Util.CommandError("alias", "You cannot work on built-in commands or aliases.")
		end

		local Set = {...}
		if #Set == 0 then
			Aliases = Aliases or {}
			if not Aliases[Alias] then
				return XJB.Util.CommandError("alias", "No alias called *"..Alias.."* exists.")
			end

			if not XJB.Util.SetAlias(Message.author, Alias, nil) then
				return XJB.Util.CommandError("alias", "Failed to write user aliases! *Sorry!!!*")
			end

			return XJB.Util.CommandOutput("🕵 Alias", "Removed alias *"..Alias.."*.")
		end

		local Command = table.concat(Set, " "):gsub("%$\\%(", "$(")

		if not XJB.Util.SetAlias(Message.author, Alias, Command) then
			return XJB.Util.CommandError("alias", "Failed to write user aliases! *Sorry!!!*")
		end

		return XJB.Util.CommandOutput("🕵 Alias", "Set alias for `"..Command.."` to *"..Alias.."*.")
	end,
	aliases = {
		"aliases" -- <:O wow
	}
})

XJB.Util.AddCommand("blind", {
	requirements = {
		ring = 2
	},
	types = {
		"server",
		"utility"
	},
	help = function()
		return XJB.Util.CommandHelp("blind [channel = current]", [[
Blindness is where I can't read commands in a channel.
Toggles "blindness" for a certain channel.]])
	end,
	func = function(Message, Ring, _, _, ChannelID)
		if not XJB.Util.CanRunCommand("blind", Message.guild_id, Ring) then
			return XJB.Util.CommandError("blind", "Permission... **`DENIED`**")
		end

		if ChannelID == "-" or not ChannelID then
			ChannelID = Message.channel_id
		end

		local Channel = XJB.Data.GetChannel(ChannelID)
		if not Channel then
			return XJB.Util.CommandError("blind", "Invalid channel ID.")
		end

		if (Channel.guild_id ~= Message.guild_id) and (Ring and Ring < 0) then
			return XJB.Util.CommandError("blind", "You can only blind a channel on this server.")
		end
		if Channel.type ~= 0 then
			return XJB.Util.CommandError("blind", "I'm sorry, Dave, but you can only do that in a server text channel.")
		end

		ChannelID = Channel.id

		local Now = not XJB.Util.IsBlind(ChannelID)

		if not XJB.Util.SetBlind(ChannelID, Now) then
			return XJB.Util.CommandError("blind", "Failed to write blind channels! *Sorry!!!*")
		end

		return XJB.Util.CommandOutput("👁 Blind", "**<#"..ChannelID..">** is "..((Now and "now") or "no longer").." blind.")
	end
})

XJB.Util.AddCommand("mirror", {
	requirements = {
		ring = 2
	},
	types = {
		"server",
		"utility"
	},
	help = function()
		return XJB.Util.CommandHelp("mirror <source = current> [mirror]", [[
Copies all **non-bot** messages from a source channel to its mirrors.
If a mirror already exists there for that source it will be removed.
Mirrored channel cannot be source and must be from the same server.
If no mirror channel is specified then it will list all mirrors of a source channel.]])
	end,
	func = function(Message, Ring, _, _, SourceID, MirrorID)
		if not XJB.Util.CanRunCommand("mirror", Message.guild_id, Ring) then
			return XJB.Util.CommandError("mirror", "Permission... **`DENIED`**")
		end

		if SourceID == "-" or not SourceID then
			SourceID = Message.channel_id
		end

		local Source = XJB.Data.GetChannel(SourceID)
		if not Source then
			return XJB.Util.CommandError("mirror", "Invalid Source ID")
		end
		SourceID = Source.id

		if (Source.guild_id ~= Message.guild_id) and Ring ~= 0 then
			return XJB.Util.CommandError("mirror", "You can only mirror a channel on this server.")
		end

		if MirrorID then
			if MirrorID == "-" then MirrorID = Message.channel_id end
			local Mirror = XJB.Data.GetChannel(MirrorID)
			if not Mirror then
				return XJB.Util.CommandError("mirror", "Invalid Mirror ID")
			end
			MirrorID = Mirror.id

			if (Mirror.guild_id ~= Message.guild_id) and (Ring and Ring < 0) then
				return XJB.Util.CommandError("mirror", "You can only mirror to a channel on this server.")
			end

			if MirrorID == SourceID then
				return XJB.Util.CommandError("mirror", "no")
			end

			local Mirrorring = true
			local i = 1
			for i, Mirror in ipairs(XJB.Util.GetMirrors(SourceID) or {}) do
				if Mirror == MirrorID then
					Mirrorring = false
					break
				end
			end

			if Mirrorring then
				if not XJB.Util.AddMirror(Source, Mirror) then
					return XJB.Util.CommandError("mirror", "Failed to write mirrorred channels! *Sorry!!!*")
				end
			else
				table.remove(XJB.Util.GetMirrors(SourceID), i)
				if not XJB.Util.WriteData("mirrors") then
					return XJB.Util.CommandError("mirror", "Failed to write mirrorred channels! *Sorry!!!*")
				end
			end

			return XJB.Util.CommandOutput("🗨💬 Mirror", ((Mirrorring and "M") or "No longer m").."irrorring messages from **<#"..SourceID..">** to **<#"..MirrorID..">**.")
		end

		local Mirrors = XJB.Util.GetMirrors(SourceID)
		if not Mirrors or #Mirrors == 0 then
			return XJB.Util.CommandOutput("🗨💬 Mirror", "This channel has no mirrors.")
		end

		local Output = ""
		for i, Mirror in ipairs(Mirrors) do
			Output = Output.."-> **<#"..Mirror..">**\n"
		end

		return XJB.Util.CommandOutput("🗨💬 Mirrors of __<#"..SourceID..">__", Output)
	end
})