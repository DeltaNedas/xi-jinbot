XJB.Misc.EbinFaces = {
	":D",
	":DD",
	":DDD",
	":-D",
	":d",
	":dd",
	":ddd",
	":-d",
	"XD",
	"XDD",
	"XXD",
	"XXDD",
	"xD",
	"xDD",
	"xxD",
	"xxDD"
}

XJB.Misc.EbinFace = function()
	return " "..XJB.Util.Random(XJB.Misc.EbinFaces)
end

XJB.Misc.Ebin = function(Epic)
	local Ebin = (Epic or ""):lower():escape("Ebin", false, false, true)
	return Ebin:gsub("[%.,]", XJB.Misc.EbinFace)
end

XJB.Misc.Translate = {
	Chinese = "https://translate.yandex.com/?lang=zh-en",
	English = "https://translate.yandex.com/?lang=en-zh",
	Detect = "https://translate.yandex.net/api/v1.5/tr.json/detect?hint=%s&key=%s&text=%s",
	Translate = "https://translate.yandex.net/api/v1.5/tr.json/translate?lang=%s-%s&key=%s&text=%s"
}

XJB.Misc.Languages = {
	af = "Afrikaans", am = "Amharic", ar = "Arabic", az = "Azerbaijan", ba = "Bashkir",
	be = "Belarusian", bg = "Bulgarian", bn = "Bengali", bs = "Bosnian", ca = "Catalan",
	ceb = "Cebuano", cs = "Czech", cy = "Welsh", da = "Danish", de = "German",
	el = "Greek", en = "English", eo = "Esperanto", es = "Spanish", et = "Estonian",
	eu = "Basque", fa = "Persian", fi = "Finnish", fr = "French", ga = "Irish",
	gd = "Scottish", gl = "Galician", gu = "Gujarati", he = "Hebrew", hi = "Hindi",
	hr = "Croatian", ht = "Haitian", hu = "Hungarian", hy = "Armenian", id = "Indonesian",
	is = "Icelandic", it = "Italian", ja = "Japanese", jv = "Javanese", ka = "Georgian",
	kk = "Kazakh", km = "Khmer", kn = "Kannada", ko = "Korean", ky = "Kyrgyz",
	la = "Latin", lb = "Luxembourgish", lo = "Laotian", lt = "Lithuanian", lv = "Latvian",
	mg = "Malagasy", mhr = "Mari", mi = "Maori", mk = "Macedonian", ml = "Malayalam",
	mn = "Mongolian", mr = "Marathi", mrj = "HillMari", ms = "Malay", mt = "Maltese",
	my = "Burmese", ne = "Nepali", nl = "Dutch", no = "Norwegian", pa = "Punjabi",
	pap = "Papiamento", pl = "Polish", pt = "Portuguese", ro = "Romanian", ru = "Russian",
	si = "Sinhala", sk = "Slovakian", sl = "Slovenian", sq = "Albanian", sr = "Serbian",
	su = "Sundanese", sv = "Swedish", sw = "Swahili", ta = "Tamil", te = "Telugu",
	tg = "Tajik", th = "Thai", tl = "Tagalog", tr = "Turkish", tt = "Tatar",
	udm = "Udmurt", uk = "Ukrainian", ur = "Urdu", uz = "Uzbek", vi = "Vietnamese",
	xh = "Xhosa", yi = "Yiddish", zh = "Chinese"
}

XJB.Misc.InviteMessages = {
	"Join the CCP. Do it now.",
	"Enter the botnet.",
	"Embrace, extend, extinguish.",
	"Install Gentoo!",
	"Dew it.",
	"Hey, $USERNAME. You're finally awake.",
	"Lets-a go!"
}

XJB.Misc.Emojis = {
	"<:spurdo:608826580269137925>",
	"😀", "😁", "😂", "🤣", "😃", "😄", "😅", "😆", "😉", "😊",
	"😋", "😎", "😍", "😘", "🥰", "😗", "😙", "😚", "☺️", "🙂",
	"🤗", "🤩", "🤔", "🤨", "😐", "😑", "😶", "🙄", "😏", "😣",
	"😥", "😮", "🤐", "😯", "😪", "😫", "😴", "😌", "😛", "😜",
	"😝", "🤤", "😒", "😓", "😔", "😕", "🙃", "🤑", "😲", "☹️",
	"🙁", "😖", "😞", "😟", "😤", "😢", "😭", "😦", "😧", "😨",
	"😩", "🤯", "😬", "😰", "😱", "🥵", "🥶", "😳", "🤪", "😵",
	"😡", "😠", "🤬", "😷", "🤒", "🤕", "🤢", "🤮", "🤧", "😇",
	"🤠", "🤡", "🥳", "🥴", "🥺", "🤥", "🤫", "🤭", "🧐", "🤓",
	"😈", "👿", "👹", "👺", "💀", "👻", "👽", "🤖", "💩", "😺",
	"😸", "😹", "😻", "😼", "😽", "🙀", "😿", "😾" -- What the actual fuck
}

XJB.Misc.FindBrackets = function(s, prefix, i) -- stolen from stack overflow
	local res = {}
	res.par = res
	local lev = 0
	for loc = 1, #s do
		if s:sub(loc, loc + #prefix) == prefix..'(' then
			lev = lev + 1 + #prefix
			local t = {
				par = res,
				start = loc,
				lev = lev
			}
			res[#res + 1] = t
			res = t
		elseif s:sub(loc, loc) == ')' then
			lev = lev - 1
			if lev < 0 then return end
			res.stop = loc
			res = res.par
		end
	end
	return res
end

XJB.Misc.GetChanges = function(Name, Version)
	local Ret = "N/A"
	if type(Version) == "table" then
		Ret = ""
		for Number = 0, #Version do
			local Version = Version[Number]
			if type(Version) == "table" then
				Ret = Ret..XJB.Misc.GetChanges(Name.."."..Number, Version):gsub("\n", "\n  ").."\n"
			else
				Ret = Ret.."\n  **Changes since *v"..Name.."."..Number.."***:\n"..Version
			end
		end
		if Ret:match("\n$") then
			Ret = Ret:sub(1, -2)
		end
	elseif type(Version) == "string" then
		Ret = Version
	end
	return "**Changes since *v"..Name.."***:\n"..Ret
end

XJB.Misc.UniqueTests = {
	lua = {
		string.split('print("yup, lua works")', " ")
	},
	say = {
		string.split("yep, say works.", " ")
	},
	math = {
		string.split("2 + 2", " ")
	},
	user = {
		"542742543478292480"
	},
	measure = {
		"542742543478292480"
	},
	chinese = {
		"test"
	},
	english = {
		"hola mi amigo"
	},
	invites = {
		"server"
	},
	checkfeeds = {""}
}

-- day, 1 -> day
-- day, 4 -> days
XJB.Misc.s = function(Text, Test)
	if Test == 1 then
		return Text
	end
	return Text.."s"
end

XJB.Misc.Pajeets = {
	"Mr nagoor babu sir M.tech certified oracle real time expert",
	"only classrom with 1000 student",
	"to poo, or to loo. that is question",
	"**15 rupee are deposit to account sir**",
	"india superpower 2020"
}

XJB.Misc.Roulette = {
	Red = {
		[0] = 1, -- 18
		[1] = 1,
		[3] = 1,
		[5] = 1,
		[7] = 1,
		[9] = 1,
		[12] = 1,
		[14] = 1,
		[16] = 1
	}
}

XJB.Misc.BallSets = {
	["8ball"] = {
		"It is certain.",
		"It is decidedly so.",
		"Without a doubt.",
		"Yes - definitely.",
		"You may rely on it.",
		"As I see it, yes.",
		"Most likely.",
		"Outlook good.",
		"Yes.",
		"Signs point to yes.",
		"Reply hazy, try again.",
		"Ask again later.",
		"Better not tell you now.",
		"Cannot predict now.",
		"Concentrate and ask again.",
		"Don't count on it.",
		"My reply is no.",
		"My sources say no.",
		"Outlook not so good.",
		"Very doubtful."
	},
	["durga"] = {
		"it is needful.",
		"it is needful, sir.",
		"mr sir.",
		"based mr durga sir",
		"mr. Nagoor Babu M. Tech",
		"real-time certified JAVA expert",
		"loo is clogged",
		"street is clean",
		"street is dirty",
		"yes kind sir",
		"you send me... ipfon",
		"kindly do needful m sir",
		"please mr sir",
		"please sir need learn CORE JAVA from durga sir...",
		"You need CORE JAVA sir...",
		"do the needful sir",
		"show the bob n vagene sirs",
		"you send me... offended",
		"pls bob"
	}
}