local STDLua = require("stdlua")
local JSON = require("lunajson")
require("litcord")
local SUser = require("litcord.structures.User")
local SChannel = require("litcord.structures.Channel")

XJB.Util.Quit = function(Code, Error)
	if Code and Code ~= 0 then
		print("\n\27[31mXi JinBot crashed! Error: "..tostring(Error).."\27[0m")
	else
		print("\nXi JinBot shutting down.")
	end
	XJB.News.Stop()
	print("Quit(): Saving creators...")
	XJB.Util.SaveCreators()
	print("Quit(): Saving feeds...")
	XJB.Util.SaveFeeds()
	print("Quit(): Saving users...")
	XJB.Util.WriteData("users")
	print("Xi Jinbot is offline.")
	os.exit(Code or 0)
end

XJB.Util.ReadData = function(Name, Default)
	Default = Default or {}
	if Name then
		if STDLua.Exists(Name..".json") then
			local Data, Error = STDLua.Read(Name..".json")
			if not Data then
				print("ReadData(): "..Error)
			end
			local Ret
			local Success, Message = pcall(function()
				local json = JSON.decode(Data)
				Ret = table.merge(Default, json) or Default -- Never override, only replace nils
			end)
			if not Success then print("ReadData(): Failed to parse JSON: "..Message) end
			Ret.__orderedIndex = nil
			return Ret, Error
		end
		return Default
	end
end

XJB.Util.WriteData = function(Name, Data)
	if Name then
		Data = XJB.User[Name:sub(1,1):upper()..Name:sub(2)] or Data
		Name = Name..".json"
		if Data then
			Data.__orderedIndex = nil
			local Success, Error = STDLua.Write(Name, JSON.encode(Data))
			if not Success then
				printf("WriteData(): %s: %s\n", Name, Error)
			end
			return Success, Error
		else
			return os.remove(Name)
		end
	end
end

XJB.Util.ToString = function(n) -- Remove trailing zeroes from a number and add commas
	if type(n) == "number" then
		local Ret = tostring(math.round(n, 2))
		local Left, Point, Right = Ret:match("(%d*)(%.?)(%d*)")
		return ((Left or ""):reverse():gsub("(%d%d%d)", "%1,"):reverse()..Point..(Right or ""):gsub("^%d*(0+)$", "")):gsub("%.$", ""):gsub("^,", "")
	end
	return tostring(n)
end

XJB.Util.ToNumber = function(s)
	return tonumber((s:gsub(",", "")))
end

XJB.Util.IsNumberValid = function(Number)
	return Number < 2^32 and Number > -2^32
end

XJB.Util.Welcome = function(Server)
	printf("Welcome(): Welcoming server %s ", Server.id)
	printf("Welcome(): Took %.6f seconds to welcome.\n", (XJB.Util.TimeFunction(function()
		local Downloaded = XJB.Data.GetServer(Server.id, false)
		Server.name = Downloaded.name
		Server.owner_id = Downloaded.owner_id
		print("("..Server.name..")")
		if XJB.User.Rings[Server.id] == nil or XJB.User.Rings[Server.id][Server.owner_id] == nil then
			XJB.Util.SetRing(Server.owner_id, 1, Server.id)
			local User = XJB.Data.GetUser(Server.owner_id)
			if User then
				XJB.Util.SendMessage(nil, (XJB.Util.CommandOutput(nil, "**🎉 Hi "..User.username.."! I'm Xi JinBot. Use "..XJB.Util.GetPrefix(Server.id).."help if you need anything! 🎉**", 65450)), User)
				print("Welcome(): Sent welcome message to "..User.username.."#"..User.discriminator..".")
			end
		end
	end)))
end

XJB.Util.GetAuthor = function(User)
	local Ret = {
		name = User.username
	}
	if User.avatar then
		Ret.icon_url = "https://cdn.discordapp.com/avatars/"..User.id.."/"..User.avatar..".png" -- nitro cucks BTFO!!!
	else
		Ret.icon_url = "https://cdn.discordapp.com/embed/avatars/"..(tonumber(User.discriminator or 1) % 5)..".png"
	end
	return Ret
end

XJB.Util.Random = function(Table)
	return Table[math.random(1, #Table)]
end

XJB.Util.TimeFunction = function(Function, ...) -- Microsecond timing of how long a function takes to execute
	local start = STDLua.Timestamp(6)

	local Ret = table.pack(Function(...))

	local stop = STDLua.Timestamp(6)
	return (stop - start) / 1000000, table.unpack(Ret)
end

XJB.Util.CharToHex = function(c)
	return string.format("%%%02X", string.byte(c))
end

XJB.Util.HexToChar = function(hex)
	return string.char(tonumber(hex, 16))
end

XJB.Util.EncodeURL = function(url)
	if type(url) == "string" then
		return (url:gsub("\n", "\r\n"):gsub(
		"([^%w _%%%-%.~])", XJB.Util.CharToHex):gsub(
		" ", "+"))
	end
end

XJB.Util.DecodeURL = function(encoded)
	if type(encoded) == "string" then
		return (encoded:gsub("+", " "):gsub(
		"%%(%x%x)", XJB.Util.HexToChar):gsub(
		"\r\n", "\n"))
	end
end

XJB.Util.TranslateText = function(Text, Language)
	if Text == "" then
		return {
			content = "**📚 Translate to "..Language..".**",
			embed = {
				title = "Yandex Translate",
				url = XJB.Misc.Translate[Language],
				color = XJB.Constants.Colours.Delta
			}
		}, XJB.Misc.Translate[Language]
	end

	if #Text:encode() > 256 then -- Number of UTF-8 Characters, may not necessarily be bytes
		return XJB.Util.CommandError(Language:lower(), "Text must be 256 characters or less.")
	end

	local FromCode = "zh"
	local ToCode = "en"
	if Language == "Chinese" then
		FromCode = "en"
		ToCode = "zh"
	end

	local DetectData, Error = STDLua.Download(XJB.Misc.Translate.Detect:format(FromCode, XJB.User.Tokens.translate, XJB.Util.EncodeURL(Text)))
	if not DetectData then
		return XJB.Util.CommandError(Language:lower(), "Failed to download data for language detection from Yandex Translate!")
	end
	DetectData = JSON.decode(DetectData)

	local Code = DetectData.code
	if Code ~= 200 then
		if Code == 401 then
			print("TranslateText(): Set tokens.json:Translate to your Yandex Translate API key.")
			return XJB.Util.CommandError(Language:lower(), "Invalid Yandex Translate API key.")
		elseif Code == 402 then
			print("TranslateText(): Set tokens.json:Translate to a non-blocked Yandex Translate API key.")
			return XJB.Util.CommandError(Language:lower(), "Blocked Yandex Translate API key.\nTell Xi Jinping to update the key!")
		elseif Code == 404 then
			print("TranslateText(): Daily limit of 1M characters was reached.")
			return XJB.Util.CommandError(Language:lower(), "Limit was reached for daily translation. Shame on you.")
		else
			print("TranslateText(): Error code "..tostring(Code))
			return XJB.Util.CommandError(Language:lower(), "Unknown error code: "..tostring(Code))
		end
	end
	FromCode = DetectData.lang

	local Data, Error = STDLua.Download(XJB.Misc.Translate.Translate:format(FromCode, ToCode, XJB.User.Tokens.translate, XJB.Util.EncodeURL(Text)))
	if not Data then
		return XJB.Util.CommandError(Language:lower(), "Failed to download data from Yandex Translate!")
	end
	Data = JSON.decode(Data)

	local Code = Data.code
	if Code ~= 200 then
		if Code == 401 then
			print("TranslateText(): Set tokens.json:Translate to your Yandex Translate API key.")
			return XJB.Util.CommandError(Language:lower(), "Invalid Yandex Translate API key.")
		elseif Code == 402 then
			print("TranslateText(): Set tokens.json:Translate to a non-blocked Yandex Translate API key.")
			return XJB.Util.CommandError(Language:lower(), "Blocked Yandex Translate API key.\nTell Xi Jinping to update the key!")
		elseif Code == 404 then
			print("TranslateText(): Daily limit of 1M characters was reached.")
			return XJB.Util.CommandError(Language:lower(), "Limit was reached for daily translation. Shame on you.")
		end
	end

	local Translated = (Data.text or {})[1]
	if not Translated then
		return XJB.Util.CommandError(Language:lower(), "Could not get translation!")
	end
	local Detected = XJB.Misc.Languages[FromCode]
	return {
		content = "**📚 Translated Text**",
		embed = {
			title = "*Powered by Yandex.Translate*",
			url = XJB.Misc.Translate[Language],
			fields = {
				{
					name = "From "..Detected..":",
					value = Text
				},
				{
					name = "To "..Language..":",
					value = Translated
				}
			},
			color = XJB.Constants.Colours.Delta
		}
	}, Translated
end

XJB.Util.AddCommand = function(Name, Table)
	Name = Name:lower()

	if XJB.Commands[Name] then
		print("AddCommand(): Command "..Name.." already exists.")
		return false, "Command already exists"
	end

	if type(Types) ~= "table" then Types = {Types} end

	local Help = Table.help
	if type(Help) ~= "function" then
		local HelpString = Table.help
		Help = function()
			return XJB.Util.CommandHelp(Name, HelpString) -- No parameter command
		end
	end

	if type(Table.func) ~= "function" then
		print("AddCommand(): Func of "..Name.." is not a function.")
		return false, "Not a function"
	end

	local Command = {
		requirements = Table.requirements or {ring = "null"},
		types = Table.types,
		help = Help,
		func = Table.func,
		destructive = Table.destructive
	}
	XJB.Commands[Name] = Command

	local SetAliases = Table.aliases or {}
	for _, Alias in pairs(SetAliases) do
		Alias = Alias:lower()
		if XJB.Aliases[Alias] then
			print("AddCommand(): Attempted to set alias "..Alias.." to existing alias.")
		elseif XJB.Commands[Alias] then
			print("AddCommand(): Attempted to set alias "..Alias.." to existing command.")
		else
			XJB.Commands[Alias] = Command
			XJB.Aliases[Alias] = true
		end
	end

	return true, #SetAliases
end

XJB.Util.PrintHelp = function(Message, Category, Override)
	local Prefix = XJB.Util.GetPrefix(Message.guild_id)
	local Categories = {}
	for Name, Command in opairs(XJB.Commands) do
		if Name and Name ~= "__orderedIndex" then
			if not XJB.Aliases[Name] then
				if Command.types then
					for _, Type in pairs(Command.types) do
						Categories[Type] = (Categories[Type] or 0) + 1
					end
				else
					printf("PrintHelp(): Command %s has no types.\n", Name)
				end
				Categories.all = (Categories.all or 0) + 1
			end
		end
	end

	if Category then
		Category = Category:match("^%((.+)%)$")
		if not Categories[Category] then
			return XJB.Util.CommandError("help", "Invalid category.")
		end
		local CatCommands = {}
		for Name, Command in opairs(XJB.Commands) do
			if Name and Name ~= "__orderedIndex" then
				if not XJB.Aliases[Name] then
					local Insert = false
					if Category == "all" then
						Insert = true
					else
						for _, Type in ipairs(Command.types) do
							if Type == Category then
								Insert = true
								break
							end
						end
					end
					if Insert then
						local help = Command.help(Message, Prefix)
						print(help.content or help)
						table.insert(CatCommands, ((help.content or help):match(
							"^.-: (.+)$"):gsub(
							"(%[.-%])", "*%1*"):gsub( -- Italic []
							"(<.->)", "__%1__"):gsub( -- Underlined <>
							"^("..Name:escape("Patterns")..")", "**"..Name.."**"):gsub( -- Bold name
							"^(.-)/(%w+)", "**%1**/**%2**"))) -- Bold split name
					end
				end
			end
		end
		return {
			content = (Category == "all" and "📜 Available commands:") or "📜 Available commands in "..Category..":",
			embed = {
				title = "*Use help **command** for more information.*",
				description = table.concat(CatCommands, "\n").."\n\n**Xi JinBot v"..XJB.Constants.Version.."**",
				color = XJB.Constants.Colours.Green
			}
		}, Override or table.count(CatCommands)
	end
	local Description = ""
	for Name, Count in opairs(Categories) do
		Description = Description.."**"..Name.."**: __"..Count.."__ Command"
		if Count == 1 then
			Description = Description..".\n"
		else
			Description = Description.."s.\n"
		end
	end

	return {
		content = "📜 **Command categories:**",
		embed = {
			title = "***Use help __(category)__ to list its commands.***\n\n", -- make it REALLY obvious :D
			description = Description.."\nSome useful commands: **checkfeeds**, **role**, **chinese**, **english**, **issue**, **feed**\n***Use help __(category)__ to list its commands.***\n**Xi JinBot v"..XJB.Constants.Version.."**",
			color = XJB.Constants.Colours.Green
		}
	}, Override or table.count(Categories)
end

XJB.Util.CommandError = function(Command, Error, Override)
	local Ret = {
		content = "**🚨 Error occured while running "..Command.."!**",
		embed = {
			description = Error or "Unknown error!",
			color = XJB.Constants.Colours.Red
		}
	}
	if Command == "ebin" then
		math.randomseed(#Ret.content)
		Error = XJB.Misc.Ebin(Error)
		Ret.content = XJB.Misc.Ebin(Ret.content)
		Ret.embed.description = Ebin(Ret.embed.description)
		math.randomseed(os.time())
	end
	return Ret, Override or Error
end

XJB.Util.CommandCreditScore = function(Command, Message, Override)
	local Ret = {
		content = "**⛔ Your credit score is too low for "..Command.."!**",
		embed = {
			description = Message or "<insert snarky comment>",
			color = XJB.Constants.Colours.Red
		}
	}
	if Command == "ebin" then
		math.randomseed(#Ret.content)
		Message = XJB.Misc.Ebin(Message)
		Ret.content = XJB.Misc.Ebin(Ret.content)
		Ret.embed.description = XJB.Misc.Ebin(Ret.embed.description)
		math.randomseed(os.time())
	end
	return Ret, Override or Message
end

XJB.Util.CommandHelp = function(Usage, Description, Override)
	local Ret = {
		content = "**Usage**: "..Usage,
		embed = {
			description = Description,
			color = XJB.Constants.Colours.Delta
		}
	}
	if Usage:match("^ebin") then
		math.randomseed(#Ret.content)
		Ret.content = XJB.Misc.Ebin(Ret.content)
		Description = XJB.Misc.Ebin(Description)
		Ret.embed.description = Description
		math.randomseed(os.time())
	end
	return Ret, Override or Description
end

XJB.Util.CommandOutput = function(Title, Description, Colour, Override)
	return {
		content = Title and "**"..Title.."**",
		embed = {
			description = Description,
			color = Colour or XJB.Constants.Colours.Delta
		}
	}, Override or Description or Title
end

XJB.Util.CommandNeedful = function(Command, Rupees, Override)
	local Ret = {
		content = "**💰 Sorry mr sir you are not needful for "..Command..".**",
		embed = {
			description = (Rupees and "U need the "..XJB.Util.ToString(Rupees).." rupees...") or "pls sir do needful today **__durgasoft.com**__!",
			color = XJB.Constants.Colours.Brown
		}
	}
	if Command == "ebin" then
		math.randomseed(#Ret.content)
		Ret.content = XJB.Misc.Ebin(Ret.content)
		Ret.embed.description = XJB.Misc.Ebin(Ret.embed.description)
		math.randomseed(os.time())
	end
	return Ret, Override or Rupees
end

XJB.Util.CreditRange = function(Range, Score)
	local Min, Max = Range:match("(.+),(.+)")
	Min = tonumber(Min)
	Max = tonumber(Max)
	return Score >= Min and Score <= Max
end

XJB.Util.SetScoreRole = function(Server, Range, ID)
	local Roles = XJB.User.Roles
	Roles[Server.id] = Roles[Server] or {}
	Roles[Server.id][Range] = ID
	XJB.Util.CheckRoles(Server)
	return XJB.Util.WriteData("roles")
end

XJB.Util.GetScoreRoles = function(Server, Score)
	if type(Server) ~= "table" then Server = {id = Server} end
	local Roles = XJB.User.Roles

	local Ret = {}
	for Range, Role in pairs(Roles[Server.id] or {}) do
		Ret[Role] = XJB.Util.CreditRange(Range, Score)
	end
	if table.count(Ret) > 0 then
		return Ret
	end
end

XJB.Util.RoleWasSetByMe = function(Server, User, Role)
	if type(Role) ~= "table" then Role = {id = Role} end
	local RSBM = XJB.User.RSBM
	if RSBM[Server.id] and RSBM[Server.id][User.id] and RSBM[Server.id][User.id][Role.id] then
		return true
	end
	return false
end

XJB.Util.UpdateInvites = function(Server)
	Server = table.merge(XJB.Data.GetServer(Server.id), XJB.Data.GetServer(Server.id, false)) -- Load local objects and "name", etc.
	local ServerInvites, Error = Server:getInvites()
	if ServerInvites then
		for _, Invite in pairs(ServerInvites) do
			if type(Invite) == "table" then
				if Invite.uses > 0 then
					XJB.Util.SetInvites(Server, Invite.inviter.id, XJB.Util.GetInvites(Server, Invite.inviter.id) + Invite.uses)
				end
			end
		end
	else
		return false, Error
	end
	return true
end

XJB.Util.AddGerald = function(Text)
	if type(Text) == "string" then
		table.insert(XJB.User.Gerald, Text)
		local Success, Error = XJB.Util.WriteData("gerald")
		if not Success then
			return XJB.Util.CommandError("gerald", "Failed to write gerald quotes! Sorry!!!"), nil, Error
		end
		return XJB.Util.CommandOutput(nil, "**🤪 GeraldGChang quote saved.**")
	end
end

XJB.Util.SetFeed = function(Feed, Server, Channel)
	if type(Server) ~= "table" then Server = {id = Server} end
	if type(Channel) ~= "table" then Channel = {id = Channel} end

	local Feeds = XJB.User.Feeds
	Feeds[Server.id] = Feeds[Server.id] or {}
	Feeds[Server.id][Feed] = Feeds[Server.id][Feed] or {}
	Feeds[Server.id][Feed].Channel = Channel.id
	local Success, Error = XJB.Util.WriteData("feeds", Feeds)
	return Success, Error
end

XJB.Util.GetFeed = function(Feed, Server) -- Returns a channel
	if type(Server) ~= "table" then Server = {id = Server} end
	return XJB.Data.GetChannel((XJB.User.Feeds[Server.id] or {})[Feed].Channel)
end

XJB.Util.SetInvites = function(Server, User, Number)
	if type(Server) ~= "table" then Server = {id = Server} end
	if type(User) ~= "table" then User = {id = User} end

	local Invites = XJB.User.Invites
	Invites[Server.id] = Invites[Server.id] or {}
	Invites[Server.id][User.id] = Number
	return true
end

XJB.Util.GetInvites = function(Server, User)
	if type(Server) ~= "table" then Server = {id = Server} end
	if type(User) ~= "table" then User = {id = User} end

	return (XJB.User.Invites[Server.id] or {})[User.id] or 0
end

XJB.Util.LoadArchive = function(Server)
	local Archive = XJB.User.Archives[Server.id]
	if Archive then
		local ID = Server.id
		printf("LoadArchive(): Loading archive of %s (%s).\n", Server.name, ID)

		print("LoadArchive(): Loading message creators.")
		XJB.Util.SaveCreators();
		if Archive.Creators then
			table.merge(XJB.User.Creators, Archive.Creators)
		end
		XJB.Util.LoadCreators();

		if Archive.Prefix then
			XJB.User.Prefixes[ID] = Archive.Prefix
			XJB.Util.WriteData("prefixes")
		end

		if Archive.Roles then
			print("LoadArchive(): Loading Social Credit Roles.")
			XJB.User.Roles[ID] = Archive.Roles[ID]
			XJB.Util.WriteData("roles")
		end

		if Archive.RSBM then
			print("LoadArchive(): Loading list of Roles set by me.")
			XJB.User.RSBM[ID] = Archive.RSBM[ID]
			XJB.Util.WriteData("rsbm")
		end

		if Archive.Feeds then
			print("LoadArchive(): Loading list of Feed channels.")
			for Feed, Channel in pairs(Archive.Feeds) do
				XJB.User.Feeds[ID][Feed] = Channel
			end
			XJB.Util.WriteData("feeds")
		end

		printf("LoadArchive(): Archive of %s (%s) loaded.\n", Server.name. ID)
		XJB.User.Archives[Server.id] = nil
		XJB.Util.WriteData("archives")
	end
end

XJB.Util.SaveArchive = function(Server, Yes, I, Am, Absolutely, Sure, I, Want, To, DELETE_EVERYTHING) -- This moves data from old servers into archived.json which helps with performance
	-- To actually delete server data set DELETE_EVERYTHING to "YES_DELETE_EVERYTHING_PLEASE_NO_GOING_BACK"
	local Archive = {}
	local ID = Server.id
	printf("SaveArchive(): Saving archive of %s (%s).\n", Server.name, Server.id)

	print("SaveArchive(): Archiving message creators.")
	XJB.Util.SaveCreators(); -- Keep no upvalues, make sure everything can save properly
	for _, Channel in pairs(Guild.channels) do
		if type(Channel) == "table" then
			local Creators = XJB.User.Creators[Channel.id]
			if Creators then
				Archive.Creators[Channel.id] = Creators
				XJB.User.Creators[Channel.id] = nil
			end
		end
	end
	XJB.Util.LoadCreators();

	if XJB.User.Prefixes[ID] then -- Don't store default prefix, save space
		print("SaveArchive(): Archiving Prefix.")
		Archive.Prefix = XJB.User.Prefixes[ID]
		XJB.User.Prefixes[ID] = nil
		XJB.Util.WriteData("prefixes")
	end

	if XJB.User.Roles[ID] then
		print("SaveArchive(): Archiving Roles.")
		Archive.Roles = XJB.User.Roles[ID]
		XJB.User.Roles[ID] = nil
		local Success, Error = XJB.Util.WriteData("roles", XJB.User.Roles)
		if not Success then
			print("SaveArchive(): "..Error)
		end
	end

	local RSBM = XJB.User.RSBM
	if RSBM[ID] then
		print("SaveArchive(): Archiving list of Roles set by me.")
		Archive.RSBM = RSBM[ID]
		RSBM[ID] = nil
		local Success, Error = XJB.Util.WriteData("rsbm", RSBM)
		if not Success then
			print("SaveArchive(): "..Error)
		end
	end

	XJB.Util.SaveFeeds()
	if XJB.User.Feeds[ID] then
		print("SaveArchive(): Archiving Feeds.")
		Archive.Feeds = XJB.User.Feeds[ID]
	end
	XJB.Util.LoadFeeds()

	if DELETE_EVERYTHING == "YES_DELETE_EVERYTHING_PLEASE_NO_GOING_BACK" then
		printf("SaveArchive(): %s (%s) PERMANENTLY DELETED.\n", Server.name, Server.id)
		return true
	else
		printf("SaveArchive(): %s (%s) archived.\n", Server.name, Server.id)
		XJB.User.Archives[Server.id] = Archive
		XJB.Util.WriteData("archives", Archives)
	end
end

local LastSent
XJB.Util.SendMessage = function(Creator, Text, channel, Fake)
	local Time, Success, Error = XJB.Util.TimeFunction(function()
		local now = STDLua.Timestamp()
		if LastSent and (now - LastSent < XJB.Constants.SendTimeout) then
			local Required = LastSent + XJB.Constants.SendTimeout - Now
			printf("SendMessage(): Yielding for %3f.\n", Required / 1000)
			STDLua.Execute("sleep %3f", Required / 1000) -- Anti DDOS
		end
		LastSent = Now

		channel = channel or Creator.parent
		if type(channel) ~= "table" then
			channel = XJB.Data.GetChannel(channel)
			if not channel then return false, 400 end
		end
		if type(Text) == "table" then
			if Text.embed then
				if #(JSON.encode(Text)) > 6000 or #(Text.embed.description or "") > 2048 then
					print("SendMessage(): Old message was "..table.rconcat(Text))
					Text = XJB.Util.CommandOutput(nil, "🚨 **Output too long!** 🚨", XJB.Constants.Colours.Red)
				end
			else
				Text = Text.content
			end
		end
		if type(Text) ~= "table" and #tostring(Text) > 2000 then
			print("SendMessage(): Old message was "..Text)
			Text = "**[Message too long]**"
		end
		if Fake == true then
			return print("[Xi JinBot]: "..tostring(Text))
		end

		local Message, Error
		if channel.username or channel.type == 1 then
			channel.parent = XJB.Client
			XJB.Client.parent = XJB.Client
			Message, Error = channel:sendMessage(Text)
			XJB.Client.parent = nil
		else
			if not channel.parent then
				channel.parent, err = XJB.Data.GetServer(channel.guild_id)
				channel.parent.parent = XJB.Client
			end
			if not channel.history then
				channel.history = utils.Cache()
			end
			Message, Error = SChannel.sendMessage(channel, Text)
		end
		if Text ~= "Connection timed out" and not Message then
			if type(Text) == "table" then Text = table.rconcat(Text) end
			print("SendMessage(): Failed to send message "..Text..": "..Error)
			return false, Error
		end
		if type(Text) == "table" then
			print("SendMessage(): Sent embed "..table.rconcat(Text))
		else
			print("SendMessage(): Sent message "..tostring(Text))
		end
		if Creator then
			XJB.User.Creators[channel.id] = XJB.User.Creators[channel.id] or {}
			local OldInfo = XJB.User.Creators[channel.id][Creator.id]
			if OldInfo and OldInfo.Cached then -- Fast startup time
				OldInfo = XJB.Util.LoadCreator(OldInfo)
			end
			local Info = OldInfo or {
				Message = Creator,
				Responses = {}
			}
			table.insert(Info.Responses, Message or {})
			XJB.User.Creators[channel.id][Creator.id] = Info
		end
	end)
	printf("SendMessage(): Took %.6f seconds to send.\n", Time)
	return Error == nil, Error
end

XJB.Util.SetPrefix = function(Server, Prefix)
	if type(Server) == "string" then Server = {id = Server} end
	if Prefix ~= XJB.Constants.DefaultPrefix then
		XJB.User.Prefixes[Server.id] = Prefix
	else
		XJB.User.Prefixes[Server.id] = nil
	end
	local Success, Error = XJB.Util.WriteData("prefixes")
	if not Success then
		return XJB.Util.CommandError("prefix", "An error occured while writing server prefixes, sorry!!!")
	end
	return XJB.Util.CommandOutput("🤖 Server Prefix", "Server prefix set to "..Prefix..".", nil, Prefix)
end

XJB.Util.GetPrefix = function(Server)
	local Prefixes = XJB.User.Prefixes
	if type(Server) == "string" then
		return Prefixes[Server] or XJB.Constants.DefaultPrefix, nil, Prefixes[Server] or XJB.Constants.DefaultPrefix
	elseif type(Server) ~= "table" then
		return XJB.Constants.DefaultPrefix, nil, XJB.Constants.DefaultPrefix
	end
	return Prefixes[Server.id] or XJB.Constants.DefaultPrefix, nil, Prefixes[Server.id] or XJB.Constants.DefaultPrefix
end

XJB.Util.SetCredit = function(User, Score)
	if type(User) == "string" then User = XJB.Data.GetUser(User) end
	if Score > 1300 then Score = 1300 elseif Score < 600 then Score = 600 end
	if Score == 1000 then Score = nil end
	if User.id == XJB.Client.user.id then
		return {
			content = "***No.***",
			embed = {
				image = {
					url = "https://media1.tenor.com/images/27364728e09d58e670154b50a59ca9c8/tenor.gif"
				},
				color = XJB.Constants.Colours.Red
			}
		}, "SetCredit(): An attempt was made to modify glorious Xi JinBot's social credit score!"
	end
	XJB.User.Credit[User.id] = Score
	local Success, Error = XJB.Util.WriteData("credit")
	if not Success then
		return CommandError("credit", "**An error occured while writing social credit scores, sorry!!!**")
	end
	XJB.Util.CheckRoles(User)
	return XJB.Util.CommandOutput("🤖 Social Credit", "**"..User.username.."**'s social credit score was set to **"..XJB.Util.ToString(Score or 1000).."**.", XJB.Constants.Colours.Yellow, Score)
end

XJB.Util.GetCredit = function(User)
	if type(User) ~= "table" then
		return XJB.User.Credit[User] or XJB.Constants.DefaultCredit, XJB.User.Credit[User] or XJB.Constants.DefaultCredit
	end
	return XJB.User.Credit[User.id] or XJB.Constants.DefaultCredit, XJB.User.Credit[User.id] or XJB.Constants.DefaultCredit
end

XJB.Util.SetRing = function(User, Ring, Server)
	if type(User) ~= "table" then User = XJB.Data.GetUser(User) end
	if type(Server) ~= "table" then Server = XJB.Data.GetServer(Server) end
	if Ring and not XJB.Util.IsNumberValid(Ring) then return XJB.Util.CommandError("ring", "Invalid execution level."), nil, "Invalid execution level." end
	if User.id == XJB.Client.user.id then
		return {
			content = "***No.***",
			embed = {
				image = {
					url = "https://media1.tenor.com/images/27364728e09d58e670154b50a59ca9c8/tenor.gif"
				},
				color = XJB.Constants.Colours.Red
			}
		}, "No.", "SetRing(): An attempt was made to modify glorious Xi JinBot's execution level!"
	end
	local Result = "Set to "
	if Ring then
		Result = Result.."__Ring-**"..tostring(Ring).."**__"
	else
		Result =  Result.."*None*"
	end
	if Server then
		if type(Server) == "string" then Server = XJB.Data.GetServer(Server) end
			if type(Server) == "table" and Server.id then
			XJB.User.Rings[Server.id] = XJB.User.Rings[Server.id] or {}
			XJB.User.Rings[Server.id][User.id] = Ring
			if Server.name then
				Result = Result.." for server **"..Server.name.."**"
			end
		else
			print("SetRing(): Server is not a table!")
		end
	else
		XJB.User.Rings.Global[User.id] = Ring
	end
	local Success, Error = XJB.Util.WriteData("rings", Rings)
	if not Success then
		return CommandError("ring", "**An error occured while writing execution levels, sorry!!!**"), Error
	end
	return XJB.Util.CommandOutput("🤖 "..User.username.."'s Execution Level", Result..".", XJB.Constants.Colours.Magenta, Ring)
end

XJB.Util.GetRing = function(User, Server)
	if type(User) == "string" then User = XJB.Data.GetUser(User) end
	if type(Server) == "string" then
		Server = XJB.Data.GetServer(Server)
	end
	local ServerRing
	if Server then
		ServerRing = (XJB.User.Rings[Server.id] or {})[User.id]
	end
	local GlobalRing = XJB.User.Rings.Global[User.id]
	local Ring
	if GlobalRing then
		if ServerRing then
			if GlobalRing < ServerRing then
				Ring = GlobalRing
			else
				Ring = ServerRing
			end
		else
			Ring = GlobalRing
		end
	elseif ServerRing then
		Ring = ServerRing
	end
	return Ring
end

XJB.Util.CreateIssue = function(Issue)
	local ID = table.count(XJB.User.Issues) + 1
	XJB.User.Issues[ID] = Issue
	local Success, Error = XJB.Util.WriteData("issues", XJB.User.Issues)
	if not Success then
		return nil, Error
	end
	return ID, Issue
end

XJB.Util.RemoveIssue = function(ID)
	local Issues = XJB.User.Issues
	local Issue = Issues[ID]
	if Issue then
		Issues[ID] = nil
		local Success, Error = XJB.Util.WriteData("issues", Issues)
		if not Success then
			return nil, Error
		end
		return ID, Issue
	end
	return nil, "Issue not found."
end

XJB.Util.ReplyIssue = function(ID, Reply, Creator)
	local Issues = XJB.User.Issues
	local Issue = Issues[ID]
	if Issue then
		local Message = XJB.Data.GetMessage(Issue.c, Issue.m)
		if Message then
			XJB.Util.SendMessage(Creator, Reply, Message.parent)
		else
			local User = XJB.Data.GetUser(Issue.u)
			if not User then
				return nil, "User not found."
			end
			XJB.Util.SendMessage(Creator, Reply, User)
		end
		return ID, Issue, Reply
	end
	return nil, "Issue not found."
end

XJB.Util.SetBan = function(Type, ID, Status)
	if Status == false then Status = nil end
	ShadowBans[Type][ID] = Status
	local Success, Error = XJB.Util.WriteData("shadowbans", ShadowBans)
	if not Success then
		return nil, Error
	end
	return ID, Status == true
end

XJB.Util.GetBan = function(Type, ID)
	return ShadowBans[Type][ID] == true
end

XJB.Util.CheckRole = function(Server, User)
	if type(User) ~= "table" then User = {id = User} end
	local IDs = XJB.Util.GetScoreRoles(Server, XJB.Util.GetCredit(User.id))
	if IDs then
		for ID, Enable in pairs(IDs) do
			if XJB.Util.RoleWasSetByMe(Server, User, ID) or Enable then
				XJB.Data.SetRole(Server, User, XJB.Data.GetRoleByID(Server, ID), Enable)
			end
		end
	end
end

XJB.Util.CheckRoles = function(Server, Whitelist)
	if Server.username then
		local List = {[Server.id] = true}
		for _, Server in pairs(XJB.Client.servers) do
			if type(Server) == "table" then
				XJB.Util.CheckRoles(Server, List)
			end
		end
	else
		if Whitelist then
			for _,Member in pairs(Server.members) do
				if type(Member) == "table" and Whitelist[Member.id] then
					XJB.Util.CheckRole(Server, Member.user)
				end
			end
		else
			for _, Member in pairs(Server.members) do
				if type(Member) == "table" then
					XJB.Util.CheckRole(Server, Member.user)
				end
			end
		end
	end
end

XJB.Util.SaveCreators = function()
	printf("SaveCreators(): Creators saved in %.6f seconds.\n", (XJB.Util.TimeFunction(function()
		local Ret = {}
		for ChannelID, Channel in pairs(XJB.User.Creators or {}) do
			Ret[ChannelID] = {}
			for message, data in pairs(Channel) do
				local Responses = {}
				for _, Response in pairs(data.Responses or {}) do
					table.insert(Responses, (Response or {}).id)
				end
				Ret[ChannelID][message] = Responses
			end
		end
		XJB.User.Creators = Ret
		XJB.Util.WriteData("creators")
	end)))
end

XJB.Util.LoadCreators = function()
	printf("LoadCreators(): Loaded creators in %.6f seconds.\n", (XJB.Util.TimeFunction(function()
		local Saved = {}
		if STDLua.Exists("creators.json") then
			local Data, Error = XJB.Util.ReadData("creators")
			if not Data then
				print("LoadCreators(): "..Error)
				os.exit(1)
			end
			Saved = Data
		end
		local Ret = {}
		for ChannelID, Channel in pairs(Saved) do
			Ret[ChannelID] = {}
			for message, responses in pairs(Channel) do
				if #responses > 0 then
					Ret[ChannelID][message] = {
						Cached = true,
						m = message,
						r = responses,
						c = ChannelID
					}
				end
			end
		end
		XJB.User.Creators = Ret
	end)))
end

XJB.Util.LoadCreator = function(Cached, Override)
	local m = Cached.m -- Command ID
	local Responses = {}-- Response IDs
	local c = Cached.c -- Channel ID

	m = Override or XJB.Data.GetMessage(c, m)
	for _, id in pairs(Cached.r) do
		table.insert(Responses, (XJB.Data.GetMessage(c, id)))
	end

	if m then
		XJB.User.Creators[c][Cached.m] = {
			Message = m,
			Responses = Responses
		}
		return XJB.User.Creators[c][Cached.m]
	else
		print("LoadCreator(): Old message "..Cached.m.." discarded.")
		if #Responses > 0 then
			for _, Response in pairs(Responses) do
				if not Response.parent then
					Response.parent = XJB.Data.GetChannel(c)
				end
				if Response.parent then
					if not Response.parent.parent then
						Response.parent.parent = XJB.Data.GetServer(Response.guild_id)
					end
					if Response.parent.parent then
						Response.parent.parent.parent = XJB.Client
						Message.delete(Response)
						print("LoadCreator(): Deleted response "..Response.id..".")
					end
				end
			end
		end
	end
end

XJB.Util.SaveFeeds = function()
	printf("SaveFeeds(): It took %.6f seconds to save server feeds.\n", (XJB.Util.TimeFunction(function()
		local Ret = {}
		for ServerID, FeedList in pairs(XJB.User.Feeds) do
			Ret[ServerID] = FeedList
			for Short, Feed in pairs(FeedList) do -- Turn Get* functions into serialisable strings
				local Latest = Feed.GetLatest
				local Embed = Feed.GetEmbed

				if Latest or Embed then
					for short, builtin in pairs(XJB.Constant.FeedLinks) do
						local Quit = 0
						if builtin.GetLatest == Latest then
							Feed.GetLatest = short
							Quit = 0.5
						end
						if builtin.GetEmbed == Embed then
							Feed.GetEmbed = short
							Quit = Quit + 0.5
						end
						if Quit == 1 then
							break
						end
					end
				end
			end
		end
		XJB.Util.WriteData("feeds", Ret)
	end)))
end

XJB.Util.LoadFeeds = function()
	printf("LoadFeeds(): It took %.6f seconds to load server feeds.\n", (XJB.Util.TimeFunction(function()
		local Saved, Error = XJB.Util.ReadData("feeds")
		if not Saved then
			print("LoadFeeds(): "..Error)
			os.exit(1)
		end

		for Server, FeedList in pairs(Saved) do
			for Short, Feed in pairs(FeedList) do
				local Latest = Feed.GetLatest
				local Embed = Feed.GetEmbed

				if Latest or Embed then
					Feed.GetLatest = FeedLinks[Latest].GetLatest
					Feed.GetEmbed = FeedLinks[Embed].GetEmbed
				end
			end
		end
		XJB.User.Feeds = Saved
	end)))
end

XJB.Util.GetRequirements = function(Name, Server)
	Command = XJB.Commands[Name]
	if not Command then
		return false
	end

	if not Server then
		return Command.requirements or {}
	end

	if type(Server) ~= "table" then Server = {id = Server} end
	if not XJB.User.Requirements[Server.id] then
		return Command.requirements or {}
	end

	return XJB.User.Requirements[Server.id][Name] or Command.requirements or {}
end

XJB.Util.SetRequirements = function(Command, Server, Requirements)
	if not (Command and Server) then
		return false
	end
	if type(Server) ~= "table" then Server = {id = Server} end

	local Table = XJB.User.Requirements[Server.id] or {}
	Table[Command] = Requirements
	XJB.User.Requirements[Server.id] = Table

	if table.count(Table) == 0 then
		XJB.User.Requirements[Server.id] = nil -- Save a bit of space
	end

	return XJB.Util.WriteData("requirements")
end

XJB.Util.CanRunCommand = function(Command, Server, Ring)
	local Required = XJB.Util.GetRequirements(Command, Server)

	if not Required then return false end -- If it fails, never allow command to be ran
	Required = Required.ring or "null"

	if Required == "null" then return true end
	if not Ring then return false end

	return Ring <= tonumber(Required)
end

-- TODO: make it use channel permissions
XJB.Util.HasPermission = function(User, Permission, Server, Channel)
	if type(User) ~= "table" then User = {id = User} end
	if type(Server) ~= "table" then Server = XJB.Data.GetServer(Server) end
	if type(Channel) ~= "table" then Channel = {id = Channel} end

	for _, member in pairs(Server.members) do
		if type(member) == "table" then
			if member.id == User.id then
				return member:hasPermission(XJB.Constants.Permissions[Permission]) or member:hasPermission(XJB.Constants.Permissions.ADMINISTRATOR)
			end
		end
	end
end

XJB.Util.SetNewbie = function(Server, Channel, Newbie)
	if type(Server) ~= "table" then Server = {id = Server} end
	if type(Channel) ~= "table" then Channel = {id = Channel} end

	XJB.User.Newbies[Server.id] = XJB.User.Newbies[Server.id] or {}
	XJB.User.Newbies[Server.id][tostring(Newbie)] = Channel.id
	if table.count(XJB.User.Newbies[Server.id]) == 0 then
		XJB.User.Newbies[Server.id] = nil
	end

	return XJB.Util.WriteData("newbies")
end

XJB.Util.GetNewbie = function(Server, User)
	if type(Server) ~= "table" then Server = {id = Server} end
	if type(User) ~= "table" then User = {id = User} end

	XJB.User.Newbies[Server.id] = XJB.User.Newbies[Server.id] or {}
	local Newbies = XJB.User.Newbies[Server.id]
	if not Newbies then return false end

	local Timestamp = User.id >> 22
	Timestamp = Timestamp + 1420070400000 -- Discord Epoch is 1/1/2015
	Timestamp = math.floor(Timestamp / 1000)

	local Now = os.time()

	for Newbie, ChannelID in pairs(Newbies) do
		if (Now - Timestamp) <= tonumber(Newbie) then
			return ChannelID, Newbie
		end
	end
	return false
end

XJB.Util.ParseTime = function(TimeString)
	if type(TimeString) == "number" then
		return math.floor(TimeString / XJB.Constants.Seconds.Day)
	end
	local Ret = 0
	Ret = Ret + tonumber(TimeString:match("(%d+)[cC]") or 0) * XJB.Constants.Seconds.Century
	Ret = Ret + tonumber(TimeString:match("(%d+)D") or 0) * XJB.Constants.Seconds.Decade
	Ret = Ret + tonumber(TimeString:match("(%d+)[yY]") or 0) * XJB.Constants.Seconds.Year
	Ret = Ret + tonumber(TimeString:match("(%d+)M") or 0) * XJB.Constants.Seconds.Month
	Ret = Ret + tonumber(TimeString:match("(%d+)[wW]") or 0) * XJB.Constants.Seconds.Week
	Ret = Ret + (tonumber(TimeString:match("(%d+)d") or 0) + tonumber(TimeString:match("(%d+)$") or 0)) * XJB.Constants.Seconds.Day
	Ret = Ret + tonumber(TimeString:match("(%d+)[hH]") or 0) * XJB.Constants.Seconds.Hour
	Ret = Ret + tonumber(TimeString:match("(%d+)m") or 0) * XJB.Constants.Seconds.Minute
	Ret = Ret + tonumber(TimeString:match("(%d+)[sS]") or 0)
	return Ret
end

XJB.Util.SetRupees = function(User, Amount)
	if type(User) ~= "table" then
		User = {id = User}
	end
	if Amount == XJB.Constants.DefaultRupees then Amount = nil end

	print("SetRupees(): Set rupees of "..User.id.." to "..XJB.Util.ToString(Amount or 0)..".")
	XJB.User.Rupees[User.id] = Amount
	return XJB.Util.WriteData("rupees")
end

XJB.Util.GetRupees = function(User)
	if type(User) ~= "table" then
		User = {id = User}
	end
	return XJB.User.Rupees[User.id] or XJB.Constants.DefaultRupees
end

XJB.Util.SetAchievement = function(User, Achievement, Silent)
	if type(User) ~= "table" then
		User = {id = User}
	end

	if not Silent then
		XJB.Util.SendMessage(nil, (XJB.Util.CommandOutput("🎀 Achievement Unlocked!", "You just unlocked **"..Achievement.."**!")), XJB.Data.GetUser(User.id))
	end

	XJB.User.Achievements[User.id] = XJB.User.Achievements[User.id] or {}
	XJB.User.Achievements[User.id][Achievement] = os.time()
	if table.count(XJB.User.Achievements[User.id]) == 0 then
		XJB.User.Achievements[User.id] = nil
	end
	return XJB.Util.WriteData("achievements")
end

XJB.Util.GetAchievements = function(User)
	if type(User) ~= "table" then
		User = {id = User}
	end
	return XJB.User.Achievements[User.id]
end

XJB.Util.BuyItem = function(User, Item, Amount)
	if type(User) ~= "table" then
		User = {id = User}
	end

	XJB.User.Inventories[User.id] = XJB.User.Inventories[User.id] or {}
	XJB.User.Inventories[User.id][Item] = (XJB.User.Inventories[User.id][Item] or 0) + (Amount or 1)

	return XJB.Util.WriteData("inventories")
end

XJB.Util.GetInventory = function(User)
	if type(User) ~= "table" then
		User = {id = User}
	end
	return XJB.User.Inventories[User.id]
end


XJB.Util.IsBlind = function(Channel)
	if type(Channel) ~= "table" then
		Channel = {id = Channel}
	end
	return XJB.User.Blind[Channel.id]
end

XJB.Util.SetBlind = function(Channel, Blind)
	if type(Channel) ~= "table" then
		Channel = {id = Channel}
	end
	XJB.User.Blind[Channel.id] = (Blind and true) or nil -- Save space and ignore default, non blind
	return XJB.Util.WriteData("blind")
end


XJB.Util.GetMirrors = function(Channel)
	if type(Channel) ~= "table" then
		Channel = {id = Channel}
	end
	return XJB.User.Mirrors[Channel.id]
end

XJB.Util.AddMirror = function(Channel, Mirror)
	if type(Channel) ~= "table" then
		Channel = {id = Channel}
	end
	if type(Mirror) ~= "table" then
		Mirror = {id = Mirror}
	end

	local Mirrors = XJB.User.Mirrors[Channel.id] or {}
	table.insert(Mirrors, Mirror.id)
	XJB.User.Mirrors[Channel.id] = Mirrors
	return XJB.Util.WriteData("mirrors")
end

XJB.Util.RemoveMirror = function(Channel, Remove)
	if type(Channel) ~= "table" then
		Channel = {id = Channel}
	end
	local Write = false

	local Mirrors = XJB.User.Mirrors[Channel.id] or {}
	for i, Mirror in pairs(Mirrors) do
		if Mirror == Remove then
			table.remove(Mirrors, i)
			Write = true
		end
	end

	if Write then
		if #Mirrors == 0 then Mirrors = nil end
		XJB.User.Mirrors[Channel.id] = Mirrors
		return XJB.Util.WriteData("mirrors")
	end
end


XJB.Util.GetAliases = function(User)
	if type(User) ~= "table" then
		User = {id = User}
	end
	return XJB.User.Aliases[User.id]
end

XJB.Util.SetAlias = function(User, Alias, Command)
	if type(User) ~= "table" then
		User = {id = User}
	end

	local Aliases = XJB.User.Aliases[User.id] or {}
	Aliases[Alias] = Command

	if table.count(Aliases) == 0 then
		Aliases = nil
	end
	XJB.User.Aliases[User.id] = Aliases
	return XJB.Util.WriteData("aliases")
end
