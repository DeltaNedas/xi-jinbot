--[[ Notes:
	All functions will return false or nil and an error message
		if something goes wrong.
	You must keep in mind that you can only make 600 requests every 10 minutes (avg 1r/s)
	For profiles you can only request the same player's profile once per minute.
	Please cache profile results to save network usage and avoid errors.
]]

local STDLua = require("stdlua")
local JSON = require("lunajson")
local Base64 = require("base64")

local URLs = {
	PlayerData = "https://api.mojang.com/users/profiles/minecraft/%s",
	PlayerNames = "https://api.mojang.com/user/profiles/%s/names",
	PlayerProfile = "https://sessionserver.mojang.com/session/minecraft/profile/%s",
	PlayerRender = "https://crafatar.daporkchop.net/renders/body/%s"
}

local NameChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_"

local SkinTemplates = "http://assets.mojang.com/SkinTemplates/%s.png"
local Alex = SkinTemplates:format("alex")
local Steve = SkinTemplates:format("steve")

local MCAPI = {}

MCAPI.Version = "1.0.1"

-- Backend

local function todecimal(UUID, Place)
	return tonumber("0x"..UUID:sub(Place, Place))
end

function MCAPI.IsUUIDValid(UUID)
	if type(UUID) ~= "string" then return false, "UUID must be a string." end
	UUID = UUID:gsub("%-", "") or UUID
	if #UUID ~= 32 then return false, "UUID must be 32 characters long." end
	if UUID:match("%X") then return false, "UUID must only consist of hexadecimal numbers." end
	return true
end

function MCAPI.IsPlayerSlim(UUID) -- XOR the LSBs of player UUID
	-- 0: steve, 1: alex
	local Valid, Reason = MCAPI.IsUUIDValid(UUID)
	if not Valid then return nil, Reason end
	return todecimal(UUID, 8) ~ todecimal(UUID, 16) ~ todecimal(UUID, 24) ~ todecimal(UUID, 32) == 1
end

function MCAPI.IsNameValid(Name)
	if type(Name) ~= "string" then return false, "Name must be a string." end
	if #Name > 16 then return false, "Name must be no longer than 16 characters." end
	if Name:match("[^"..NameChars.."]") then return "Name must be alphanumeric with optional underscores." end
	return true
end

-- Frontend

function MCAPI.GetUUID(Name) -- Current name of account, old names are invalid
	local Valid, Reason = MCAPI.IsNameValid(Name)
	if not Valid then return false, Reason end
	local Raw, Error = STDLua.Download(URLs.PlayerData:format(Name))
	if not Raw then return false, Error end
	if Raw == "" then return false, "Player does not exist." end
	local Data = JSON.decode(Raw)
	if not Data then return false, "Failed to parse JSON." end
	if Data.error then return false, Data.errorMessage.."." end
	return Data.id
end

function MCAPI.GetNameHistory(UUID)
	local Valid, Reason = MCAPI.IsUUIDValid(UUID)
	if not Valid then return false, Reason end
	local Raw, Error = STDLua.Download(URLs.PlayerNames:format(UUID))
	if not Raw then return false, Error end
	if Raw == "" then return false, "Player does not exist." end
	local Data = JSON.decode(Raw)
	if not Data then return false, "Failed to parse JSON." end
	if Data.error then return false, Data.errorMessage.."." end
	return Data
end

function MCAPI.GetName(UUID)
	local Names, Error = MCAPI.GetNameHistory(UUID)
	if not Names then return false, Error end
	return Names[#Names].name
end

function MCAPI.GetProfile(UUID)
	local Valid, Reason = MCAPI.IsUUIDValid(UUID)
	if not Valid then return false, Reason end
	local Raw, Error = STDLua.Download(URLs.PlayerProfile:format(UUID))
	if not Raw then return false, Error end
	if Raw == "" then return false, "Player profile does not exist." end
	local Data = JSON.decode(Raw)
	if not Data then return false, "Failed to parse profile JSON." end
	if Data.error then
		if Data.error == "TooManyRequestsException" then
			return false, "Please wait 1 minute before making another profile request."
		end
		return false, Data.errorMessage.."."
	end
	return Data
end

function MCAPI.GetSkins(UUID)
	local Profile, Error = MCAPI.GetProfile(UUID)
	if not Profile then return false, Error end
	local Textures
	for _, Property in pairs(Profile.properties or {}) do -- In case more properties are added to profile
		if Property.name == "textures" then
			Textures = Property.value
			break
		end
	end

	if not Textures then return false, "No player textures found." end
	local Raw = Base64.decode(Textures)
	if not Raw then return false, "Failed to decode textures." end
	local Data = JSON.decode(Raw)
	if not Data then return false, "Failed to parse textures." end
	return Data.textures
end

function MCAPI.GetSkin(UUID) -- Returns skin URL and if skin is slim
	local Skins, Error = MCAPI.GetSkins(UUID)
	if not Skins then return false, Error end
	if not Skins.SKIN then -- Get steve/alex
		local Slim = MCAPI.IsPlayerSlim(UUID)
		local Skin = Steve
		if Slim then Skin = Alex end
		return Skin, Slim
	end
	return Skins.SKIN.url, (Skins.SKIN.metadata or {}).model == "slim"
end

function MCAPI.GetCape(UUID)
	local Skins, Error = MCAPI.GetSkins(UUID)
	if not Skins then return false, Error end
	if not Skins.CAPE then return false, "Player has no cape." end
	return Skins.CAPE.url
end

function MCAPI.GetRender(Name) --ty peter porker
	local UUID, Error = MCAPI.GetUUID(Name)
	if not UUID then return nil, Error end
	return URLs.PlayerRender:format(UUID)
end

return MCAPI
