# Tutorial
When you start the bot for the first time with ./xijinbot.lua it will create a tokens.json file.
Replace "Discord":"TOKEN" with "Discord":"YOUR BOT TOKEN FROM DISCORD APPLICATIONS/BOTS/COPY TOKEN".
You can also replace the Yandex Translate token, if you don't then you won't be able to use $chinese or $english.
After that invite your instance of Xi JinBot to a server.
If you own the server you will automatically have Ring-1 execution level.
Use $help for a list of commands.
Have fun and dont let the jannies bite!

# Server
There's a server for Xi JinBot!
Come on down to **x4eKFbT** and join in on the fun.

# Dependencies
You need Lua 5.3, lua-pthread, LuaSec and Litcord to run this bot.
Use `luarocks install luasec litcord`
You can install luarocks and lua5.3 from your distribution's repository.
You also need to compile https://github.com/mah0x211/lua-pthread.
